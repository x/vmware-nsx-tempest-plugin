# Copyright 2018 VMware Inc
# All Rights Reserved
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

from tempest import config
from tempest.lib.common.utils import data_utils
from tempest.lib import decorators
from tempest.lib import exceptions as lib_exc

from vmware_nsx_tempest_plugin.common import constants
from vmware_nsx_tempest_plugin.lib import feature_manager
from vmware_nsx_tempest_plugin.services import nsxv3_client

CONF = config.CONF
LOG = constants.log.getLogger(__name__)


class VlanPnicTests(feature_manager.FeatureManager):
    """Test Vlan Pnic validation
    """

    @classmethod
    def skip_checks(cls):
        super(VlanPnicTests, cls).skip_checks()

    @classmethod
    def setup_clients(cls):
        super(VlanPnicTests, cls).setup_clients()
        cls.cmgr_adm = cls.get_client_manager('admin')

    @classmethod
    def resource_setup(cls):
        super(VlanPnicTests, cls).resource_setup()
        cls.nsx = nsxv3_client.NSXV3Client(CONF.nsxv3.nsx_manager,
                                           CONF.nsxv3.nsx_user,
                                           CONF.nsxv3.nsx_password)
        out = cls.nsx.get_transport_zones()
        vlan_flag = 0
        cls.vlan_ids = []
        for tz in out:
            if "transport_type" in tz.keys():
                if vlan_flag == 0 and tz['transport_type'] == "VLAN":
                    cls.vlan_ids.append(tz['id'])

    @decorators.attr(type=['nsxv3', 'negative'])
    @decorators.idempotent_id('888683ec-9316-4a70-aeb1-a56842d563b8')
    def test_create_vlan_ls_same_as_transport_vlan(self):
        """Verify that MDProxy, DHCP LS attachment with the same
           vlanid is not allowed as the vlan specified for the
           transport node uplink, using the same pnic
        """
        networks_client = self.cmgr_adm.networks_client
        name = data_utils.rand_name("provider_network_vlan")
        body = {"provider:segmentation_id": CONF.nsxv3.transport_vlan,
                "provider:network_type": constants.VLAN_TYPE,
                "admin_state_up": "True"}
        self.assertRaises(lib_exc.BadRequest, self.create_topology_network,
                          name, networks_client=networks_client,
                          **body)

    @decorators.attr(type=['nsxv3', 'positive'])
    @decorators.idempotent_id('42985d14-0296-4f0f-a349-0fc4180c3d5b')
    def test_create_two_ls_diff_vlan_diff_tz_allowed(self):
        """Verify that two OS Vlan LS with mdproxy attachments
           and different vlans is allowed across different tz,
           using the same pnic.
        """
        networks_client = self.cmgr_adm.networks_client
        for i, vlan_tz in enumerate(self.vlan_ids):
            if i == 0 or i == 2:
                name = data_utils.rand_name("provider_network_vlan")
                vlan = str(int(CONF.nsxv3.transport_vlan) + i + 1)
                body = {"provider:segmentation_id": vlan,
                        "provider:physical_network": vlan_tz,
                        "provider:network_type": constants.VLAN_TYPE,
                        "admin_state_up": "True"}
                network = self.create_topology_network(name,
                    networks_client=networks_client, **body)
                self.assertEqual(network['status'], 'ACTIVE')
                self.assertEqual(network['provider:physical_network'], vlan_tz)
                self.assertEqual(network['provider:segmentation_id'],
                                 int(vlan))

    @decorators.attr(type=['nsxv3', 'negative'])
    @decorators.idempotent_id('4cc87ade-dd06-4c18-a0a0-586f7ff68415')
    def test_update_edge_transport_vlan(self):
        """Verify that modifying the transport VLAN of Edge vtep to same as
           OpenStack VLAN LS is not allowed, using the same pnic.
        """
        networks_client = self.cmgr_adm.networks_client
        vlan = str(int(CONF.nsxv3.transport_vlan) + 1)
        msg = "exist duplicate logical-switch vlan-ids:%s" % vlan
        name = data_utils.rand_name("provider_network_vlan")
        body = {"provider:segmentation_id": vlan,
                "provider:network_type": constants.VLAN_TYPE,
                "admin_state_up": "True"}
        network = self.create_topology_network(name,
            networks_client=networks_client, **body)
        self.assertEqual(network['status'], 'ACTIVE')
        self.assertEqual(network['provider:segmentation_id'], int(vlan))
        #update uplink host switch profile with the above vlan
        profiles = self.nsx.get_host_switch_profiles()
        for profile in profiles:
            if profile["resource_type"] == "UplinkHostSwitchProfile":
                uplink_profile = profile
                break
        resp = self.nsx.update_uplink_profile_vlan(vlan, uplink_profile)
        self.assertIn(msg, resp["error_message"])

    @decorators.attr(type=['nsxv3', 'negative'])
    @decorators.idempotent_id('65dabce2-a800-4cc3-b5a9-c6ce7ef6aec7')
    def test_create_ls_with_existing_vlan_same_pnic(self):
        """Verify that two OS Vlan LS with mdproxy attachments and
           the same vlan is not allowed, using the same pnic.
        """
        networks_client = self.cmgr_adm.networks_client
        vlan = str(int(CONF.nsxv3.transport_vlan) + 3)
        name = data_utils.rand_name("provider_network_vlan")
        body = {"provider:segmentation_id": vlan,
                "provider:network_type": constants.VLAN_TYPE,
                "admin_state_up": "True"}
        network = self.create_topology_network(name,
            networks_client=networks_client, **body)
        self.assertEqual(network['status'], 'ACTIVE')
        self.assertEqual(network['provider:segmentation_id'], int(vlan))
        #Create another vlan network with same vlan id as above
        name = data_utils.rand_name("provider_network_vlan")
        self.assertRaises(lib_exc.Conflict, self.create_topology_network,
                          name, networks_client=networks_client,
                          **body)

    @decorators.attr(type=['nsxv3', 'negative'])
    @decorators.idempotent_id('d5166bd3-8482-4448-9475-3a853a0a00a6')
    def test_update_ls_with_existing_vlan_same_pnic(self):
        """Verify that update on OS Vlan LS with mdproxy attachments
           with same transport vlan is not allowed, using the same pnic.
        """
        networks_client = self.cmgr_adm.networks_client
        vlan = str(int(CONF.nsxv3.transport_vlan) + 4)
        name = data_utils.rand_name("provider_network_vlan")
        body = {"provider:segmentation_id": vlan,
                "provider:network_type": constants.VLAN_TYPE,
                "admin_state_up": "True"}
        network = self.create_topology_network(name,
            networks_client=networks_client, **body)
        self.assertEqual(network['status'], 'ACTIVE')
        self.assertEqual(network['provider:segmentation_id'], int(vlan))
        #update vlan network with same vlan id as transport vlan
        body = {"provider:segmentation_id": CONF.nsxv3.transport_vlan}
        self.assertRaises(lib_exc.BadRequest, self.update_topology_network,
                          network_id=network['id'],
                          networks_client=networks_client,
                          **body)

    @decorators.attr(type=['nsxv3', 'negative'])
    @decorators.idempotent_id('65dabce2-a800-4cc3-b5a9-c6ce7ef6aec7')
    def test_create_2_ls_with_diff_vlan_same_pnic(self):
        """Verify that two OS Vlan LS with mdproxy attachments and
           different vlans is allowed, using the same pnic.
        """
        networks_client = self.cmgr_adm.networks_client
        vlan = str(int(CONF.nsxv3.transport_vlan) + 3)
        name = data_utils.rand_name("provider_network_vlan")
        body = {"provider:segmentation_id": vlan,
                "provider:network_type": constants.VLAN_TYPE,
                "admin_state_up": "True"}
        network = self.create_topology_network(name,
            networks_client=networks_client, **body)
        self.assertEqual(network['status'], 'ACTIVE')
        self.assertEqual(network['provider:segmentation_id'], int(vlan))
        #Create another vlan network with same vlan id as above
        name = data_utils.rand_name("provider_network_vlan")
        self.assertRaises(lib_exc.Conflict, self.create_topology_network,
                          name, networks_client=networks_client,
                          **body)

    @decorators.attr(type=['nsxv3', 'positive'])
    @decorators.idempotent_id('a6eafd88-c9c3-4148-8eb0-9ba8e49dd416')
    def test_create_two_ls_same_vlan_diff_tz_allowed(self):
        """Verify that two OS Vlan LS with mdproxy attachments
           and same vlans across different tz is allowed,
           using the same pnic.
        """
        networks_client = self.cmgr_adm.networks_client
        for i, vlan_tz in enumerate(self.vlan_ids):
            if i == 0 or i == 2:
                name = data_utils.rand_name("provider_network_vlan")
                vlan = str(int(CONF.nsxv3.transport_vlan) + 1)
                body = {"provider:segmentation_id": vlan,
                        "provider:physical_network": vlan_tz,
                        "provider:network_type": constants.VLAN_TYPE,
                        "admin_state_up": "True"}
                network = self.create_topology_network(name,
                    networks_client=networks_client, **body)
                self.assertEqual(network['status'], 'ACTIVE')
                self.assertEqual(network['provider:physical_network'], vlan_tz)
                self.assertEqual(network['provider:segmentation_id'],
                                 int(vlan))

    @decorators.attr(type=['nsxv3', 'positive'])
    @decorators.idempotent_id('b79edeab-b09e-434b-8b6f-efd379ac3625')
    def test_create_two_ls_diff_vlan_same_tz_allowed(self):
        """Verify that two OS Vlan LS with mdproxy attachments
           and diff vlans across same tz is allowed,
           using the same pnic.
        """
        networks_client = self.cmgr_adm.networks_client
        for i in range(2):
            name = data_utils.rand_name("provider_network_vlan")
            vlan = str(int(CONF.nsxv3.transport_vlan) + i + 2)
            body = {"provider:segmentation_id": vlan,
                    "provider:network_type": constants.VLAN_TYPE,
                    "admin_state_up": "True"}
            network = self.create_topology_network(name,
                networks_client=networks_client, **body)
            self.assertEqual(network['status'], 'ACTIVE')
            self.assertEqual(network['provider:segmentation_id'], int(vlan))
