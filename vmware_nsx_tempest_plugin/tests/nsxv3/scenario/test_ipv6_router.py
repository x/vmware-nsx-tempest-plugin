#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
import random
import time

from tempest import config
from tempest.lib.common.utils import data_utils
from tempest.lib import decorators
from tempest.lib import exceptions

from vmware_nsx_tempest_plugin.common import constants
from vmware_nsx_tempest_plugin.lib import feature_manager
CONF = config.CONF


class IPv6RoutersTest(feature_manager.FeatureManager):
    """Test the following operations for ports:
        port create
        port delete
        port list
        port show
        port update
    """

    @classmethod
    def skip_checks(cls):
        super(IPv6RoutersTest, cls).skip_checks()
        if not (CONF.network_feature_enabled.ipv6 and
                CONF.network_feature_enabled.ipv6_subnet_attributes):
            raise cls.skipException('IPv6 or its attributes not supported')
        if not (CONF.network.project_networks_reachable or
                CONF.network.public_network_id):
            msg = ('Either project_networks_reachable must be "true", or '
                   'public_network_id must be defined.')
            raise cls.skipException(msg)

    @classmethod
    def setup_clients(cls):
        super(IPv6RoutersTest, cls).setup_clients()
        cls.cmgr_adm = cls.get_client_manager('admin')

    @classmethod
    def resource_setup(cls):
        super(IPv6RoutersTest, cls).resource_setup()

    def _create_ipv6_subnet(self, network, cidr=None, ipv6_ra_mode=None,
                            ipv6_address_mode=None,
                            router_id=None, slaac=False, enable_dhcp=False):
        subnet_client = self.cmgr_adm.subnets_client
        subnet_name = network['name'] + 'sub'
        cidr = (str(random.randint(1, 1049)) + "::/64")
        if slaac:
            subnet = self.create_topology_subnet(
                subnet_name, network,
                subnets_client=subnet_client,
                ip_version=6, ipv6_ra_mode='slaac',
                ipv6_address_mode='slaac',
                cidr=cidr, router_id=router_id)
        else:
            subnet = self.create_topology_subnet(
                subnet_name, network,
                subnets_client=subnet_client,
                ipv6_address_mode=ipv6_address_mode,
                ipv6_ra_mode=ipv6_ra_mode,
                ip_version=6, enable_dhcp=True,
                cidr=cidr, router_id=router_id)
        return subnet

    def create_topo_single_network(self, namestart, create_instance=True,
                                   set_gateway=True, slaac=True, **kwargs):
        """
        Create Topo where 1 logical switches which is
        connected via tier-1 router.
        """
        rtr_name = data_utils.rand_name(name='tempest-router')
        network_name = data_utils.rand_name(name='tempest-net')
        subnet_name = data_utils.rand_name(name='tempest-subnet')
        router_state = self.create_topology_router(rtr_name,
                                                   set_gateway=set_gateway,
                                                   **kwargs)
        network_state = self.create_topology_network(network_name)
        subnet_state = self.create_topology_subnet(subnet_name, network_state,
                                                   router_id=router_state["id"]
                                                   )
        subnet_v6 = self._create_ipv6_subnet(network_state,
                                             slaac=slaac,
                                             router_id=router_state["id"])
        time.sleep(constants.NSX_NETWORK_REALISE_TIMEOUT)
        if create_instance:
            image_id = self.get_glance_image_id(["cirros", "esx"])
            self.create_topology_instance(
                "state_vm_1", [network_state],
                create_floating_ip=True, image_id=image_id)
            self.create_topology_instance(
                "state_vm_2", [network_state],
                create_floating_ip=True, image_id=image_id)
        topology_dict = dict(router_state=router_state,
                             network_state=network_state,
                             subnet_state=subnet_state,
                             subnet_v6=subnet_v6)
        return topology_dict

    def create_topo_single_dhcpv6_network(
            self, namestart, ipv6_address_mode,
            ipv6_ra_mode, create_instance=True,
            set_gateway=True, slaac=False,
            **kwargs):
        """
        Create Topo where 1 logical switches which is
        connected via tier-1 router.
        """
        rtr_name = data_utils.rand_name(name='tempest-router')
        network_name = data_utils.rand_name(name='tempest-net')
        subnet_name = data_utils.rand_name(name='tempest-subnet')
        router_state = self.create_topology_router(rtr_name,
                                                   set_gateway=set_gateway,
                                                   **kwargs)
        network_state = self.create_topology_network(network_name)
        subnet_state = self.create_topology_subnet(subnet_name, network_state,
                                                   router_id=router_state["id"]
                                                   )
        subnet_v6 = self._create_ipv6_subnet(
            network_state, slaac=slaac,
            ipv6_address_mode=ipv6_address_mode,
            ipv6_ra_mode=ipv6_ra_mode,
            router_id=router_state["id"])
        time.sleep(constants.NSX_NETWORK_REALISE_TIMEOUT)
        if create_instance:
            image_id = self.get_glance_image_id(["cirros", "esx"])
            self.create_topology_instance(
                "state_vm_1", [network_state],
                create_floating_ip=True, image_id=image_id)
            self.create_topology_instance(
                "state_vm_2", [network_state],
                create_floating_ip=True, image_id=image_id)
        topology_dict = dict(router_state=router_state,
                             network_state=network_state,
                             subnet_state=subnet_state,
                             subnet_v6=subnet_v6)
        return topology_dict

    @decorators.attr(type=['nsxv3', 'positive'])
    @decorators.idempotent_id('6526db57-c523-4879-b8ae-f50f0190f960')
    def test_single_ipv6_downlink_interface_rtr(self):
        """Test creating single ipv6 static subnet attached
           to router downlink interface
        """
        name = data_utils.rand_name("ipv6-net")
        networks_client = self.cmgr_adm.networks_client
        network = self.create_topology_network(
            name, networks_client=networks_client)
        subnet = self._create_ipv6_subnet(network,
                                          slaac=True)
        rtr_name = data_utils.rand_name("ipv6-rtr")
        router = self.create_topology_router(
            rtr_name, routers_client=self.cmgr_adm.routers_client)
        interface = self.add_router_interface(
            router_client=self.cmgr_adm.routers_client,
            router_id=router['id'], subnet_id=subnet['id'])
        self.assertEqual(subnet['id'], interface['subnet_id'])

    @decorators.attr(type=['nsxv3', 'positive'])
    @decorators.idempotent_id('6cbd1f61-8d63-4d81-a8ba-f0f0624d4585')
    def test_ipv4_ipv6_downlink_interface_rtr(self):
        """Test creating ipv4 and ipv6 static subnets attached
           to router downlink interface
        """
        name = data_utils.rand_name("ipv6-net")
        networks_client = self.cmgr_adm.networks_client
        network = self.create_topology_network(
            name, networks_client=networks_client)
        subnet_client = self.cmgr_adm.subnets_client
        subnet_name = network['name'] + 'ipv4-sub'
        subnet_ipv4 = self.create_topology_subnet(
            subnet_name, network,
            subnets_client=subnet_client,
            cidr='20.20.0.0/16')
        subnet = self._create_ipv6_subnet(network, slaac=True)
        rtr_name = data_utils.rand_name("ipv6-rtr")
        router = self.create_topology_router(
            rtr_name, routers_client=self.cmgr_adm.routers_client)
        interface = self.add_router_interface(
            router_client=self.cmgr_adm.routers_client,
            router_id=router['id'], subnet_id=subnet_ipv4['id'])
        self.assertEqual(subnet_ipv4['id'], interface['subnet_id'])
        interface = self.add_router_interface(
            router_client=self.cmgr_adm.routers_client,
            router_id=router['id'], subnet_id=subnet['id'])
        self.assertEqual(subnet['id'], interface['subnet_id'])

    @decorators.attr(type=['nsxv3', 'positive'])
    @decorators.idempotent_id('6a228287-25d2-41b3-aa7d-f6e566b9c8a3')
    def test_slaac_single_ipv6_downlink_interface_rtr(self):
        """Test creating single ipv6 static subnet attached
           to router downlink interface
        """
        name = data_utils.rand_name("ipv6-net")
        networks_client = self.cmgr_adm.networks_client
        network = self.create_topology_network(
            name, networks_client=networks_client)
        subnet = self._create_ipv6_subnet(network, slaac=True)
        rtr_name = data_utils.rand_name("ipv6-rtr")
        router = self.create_topology_router(
            rtr_name, routers_client=self.cmgr_adm.routers_client)
        interface = self.add_router_interface(
            router_client=self.cmgr_adm.routers_client,
            router_id=router['id'], subnet_id=subnet['id'])
        self.assertEqual(subnet['id'], interface['subnet_id'])

    @decorators.attr(type=['nsxv3', 'positive'])
    @decorators.idempotent_id('8e7b1e28-e50e-458e-beb4-49ce5663c561')
    def test_slaac_ipv4_ipv6_downlink_interface_rtr(self):
        """Test creating ipv4 and ipv6 slaac subnets attached
           to router downlink interface
        """
        name = data_utils.rand_name("ipv6-net")
        networks_client = self.cmgr_adm.networks_client
        network = self.create_topology_network(
            name, networks_client=networks_client)
        subnet_client = self.cmgr_adm.subnets_client
        subnet_name = network['name'] + 'ipv4-sub'
        subnet_ipv4 = self.create_topology_subnet(
            subnet_name, network, subnets_client=subnet_client,
            cidr='20.20.0.0/16')
        subnet = self._create_ipv6_subnet(network, slaac=True)
        rtr_name = data_utils.rand_name("ipv6-rtr")
        router = self.create_topology_router(
            rtr_name, routers_client=self.cmgr_adm.routers_client)
        interface = self.add_router_interface(
            router_client=self.cmgr_adm.routers_client,
            router_id=router['id'], subnet_id=subnet_ipv4['id'])
        self.assertEqual(subnet_ipv4['id'], interface['subnet_id'])
        interface = self.add_router_interface(
            router_client=self.cmgr_adm.routers_client,
            router_id=router['id'], subnet_id=subnet['id'])
        self.assertEqual(subnet['id'], interface['subnet_id'])

    @decorators.attr(type=['nsxv3', 'positive'])
    @decorators.idempotent_id('85235d0f-89b5-48a0-a3ee-d1bd21be6b94')
    def test_slaac_multiple_ipv6_downlink_interface_rtr(self):
        """Test creating multiple ipv6 slaac subnets attached
           to router downlink interface
        """
        name = data_utils.rand_name("ipv6-net")
        networks_client = self.cmgr_adm.networks_client
        network = self.create_topology_network(
            name, networks_client=networks_client)
        subnet = self._create_ipv6_subnet(network, slaac=True)
        network_1 = self.create_topology_network(
            name, networks_client=networks_client)
        subnet_1 = self._create_ipv6_subnet(network_1,
                                            slaac=True)
        rtr_name = data_utils.rand_name("ipv6-rtr")
        router = self.create_topology_router(
            rtr_name, routers_client=self.cmgr_adm.routers_client)
        interface = self.add_router_interface(
            router_client=self.cmgr_adm.routers_client,
            router_id=router['id'], subnet_id=subnet['id'])
        self.assertEqual(subnet['id'], interface['subnet_id'])
        interface = self.add_router_interface(
            router_client=self.cmgr_adm.routers_client,
            router_id=router['id'], subnet_id=subnet_1['id'])
        self.assertEqual(subnet_1['id'], interface['subnet_id'])

    @decorators.attr(type=['nsxv3', 'positive'])
    @decorators.idempotent_id('3564b971-6033-43cc-a13a-93b467bca50d')
    def test_multiple_ipv6_downlink_interface_rtr(self):
        """Test creating multiple ipv6 static subnets attached
           to router downlink interface
        """
        name = data_utils.rand_name("ipv6-net")
        networks_client = self.cmgr_adm.networks_client
        network = self.create_topology_network(
            name, networks_client=networks_client)
        subnet_client = self.cmgr_adm.subnets_client
        subnet_name = network['name'] + 'ipv4-sub'
        subnet_ipv4 = self.create_topology_subnet(
            subnet_name, network, subnets_client=subnet_client,
            cidr='20.21.0.0/16')
        subnet = self._create_ipv6_subnet(
            network,
            ipv6_ra_mode="dhcpv6-stateful",
            ipv6_address_mode="dhcpv6-stateful",
            slaac=False)
        network_1 = self.create_topology_network(
            name, networks_client=networks_client)
        subnet_ipv4_1 = self.create_topology_subnet(
            subnet_name, network_1, subnets_client=subnet_client,
            cidr='20.22.0.0/16')
        subnet_1 = self._create_ipv6_subnet(
            network_1,
            ipv6_ra_mode="dhcpv6-stateful",
            ipv6_address_mode="dhcpv6-stateful",
            slaac=False)
        rtr_name = data_utils.rand_name("ipv6-rtr")
        router = self.create_topology_router(
            rtr_name, routers_client=self.cmgr_adm.routers_client)
        interface = self.add_router_interface(
            router_client=self.cmgr_adm.routers_client,
            router_id=router['id'], subnet_id=subnet['id'])
        self.assertEqual(subnet['id'], interface['subnet_id'])
        interface = self.add_router_interface(
            router_client=self.cmgr_adm.routers_client,
            router_id=router['id'], subnet_id=subnet_ipv4['id'])
        self.assertEqual(subnet_ipv4['id'], interface['subnet_id'])
        interface = self.add_router_interface(
            router_client=self.cmgr_adm.routers_client,
            router_id=router['id'], subnet_id=subnet_1['id'])
        self.assertEqual(subnet_1['id'], interface['subnet_id'])
        interface = self.add_router_interface(
            router_client=self.cmgr_adm.routers_client,
            router_id=router['id'], subnet_id=subnet_ipv4_1['id'])
        self.assertEqual(subnet_ipv4_1['id'], interface['subnet_id'])

    @decorators.attr(type=['nsxv3', 'positive'])
    @decorators.idempotent_id('151dd309-44a2-46ac-978a-f058492843c8')
    def test_slaac_multiple_ipv4_v6_downlink_interface_rtr(self):
        """Test creating multiple ipv6 slaac subnets attached
           to router downlink interface
        """
        name = data_utils.rand_name("dual-net")
        networks_client = self.cmgr_adm.networks_client
        network = self.create_topology_network(
            name, networks_client=networks_client)
        subnet_client = self.cmgr_adm.subnets_client
        subnet_name = network['name'] + 'ipv4-sub'
        subnet_ipv4 = self.create_topology_subnet(
            subnet_name, network, subnets_client=subnet_client,
            cidr='20.21.0.0/16')
        subnet = self._create_ipv6_subnet(network,
                                          slaac=True)
        network_1 = self.create_topology_network(
            name, networks_client=networks_client)
        subnet_ipv4_1 = self.create_topology_subnet(
            subnet_name, network_1, subnets_client=subnet_client,
            cidr='20.22.0.0/16')
        subnet_1 = self._create_ipv6_subnet(network_1,
                                            slaac=True)
        rtr_name = data_utils.rand_name("ipv6-rtr")
        router = self.create_topology_router(
            rtr_name, routers_client=self.cmgr_adm.routers_client)
        interface = self.add_router_interface(
            router_client=self.cmgr_adm.routers_client,
            router_id=router['id'], subnet_id=subnet['id'])
        self.assertEqual(subnet['id'], interface['subnet_id'])
        interface = self.add_router_interface(
            router_client=self.cmgr_adm.routers_client,
            router_id=router['id'], subnet_id=subnet_ipv4['id'])
        self.assertEqual(subnet_ipv4['id'], interface['subnet_id'])
        interface = self.add_router_interface(
            router_client=self.cmgr_adm.routers_client,
            router_id=router['id'], subnet_id=subnet_1['id'])
        self.assertEqual(subnet_1['id'], interface['subnet_id'])
        interface = self.add_router_interface(
            router_client=self.cmgr_adm.routers_client,
            router_id=router['id'], subnet_id=subnet_ipv4_1['id'])
        self.assertEqual(subnet_ipv4_1['id'], interface['subnet_id'])

    @decorators.attr(type=['nsxv3', 'negative'])
    @decorators.idempotent_id('8cc317db-4bcd-4f56-821a-9040b4a9c740')
    def test_update_router_nsxv3_with_ipv6_static_route_via_0_0(self):
        """
        Check it should not allow to add static route on router with
        ::/0 next hop.
        """
        kwargs = {"admin_state_up": "True"}
        topology_dict = self.create_topo_single_network("rtr_update",
                                                        create_instance=False,
                                                        set_gateway=False,
                                                        **kwargs)
        router_state = topology_dict['router_state']
        routes = [{
                  "destination": "3700::/64",
                  "nexthop": "::/0"
                  }]
        router_id = router_state['id']
        self.assertRaises(exceptions.BadRequest,
                          self.cmgr_adm.routers_client.update_router,
                          router_id, routes=routes)

    @decorators.attr(type=['nsxv3', 'negative'])
    @decorators.idempotent_id('8cc317db-4bcd-4f56-821a-9040b4a9c740')
    def test_update_router_nsxv3_with_ipv6_static_route_dest_0_0(self):
        """
        Check it should allow to add static route on router with
        ::/0 as destination.
        """
        kwargs = {"admin_state_up": "True"}
        topology_dict = self.create_topo_single_network("rtr_update",
                                                        create_instance=False,
                                                        set_gateway=False,
                                                        **kwargs)
        router_state = topology_dict['router_state']
        next_hop = topology_dict['subnet_v6']['allocation_pools'][0]['end']
        dest = "::/0"
        routes = [{
                  "destination": dest,
                  "nexthop": next_hop
                  }]
        router_id = router_state['id']
        self.assertRaises(exceptions.BadRequest,
                          self.cmgr_adm.routers_client.update_router,
                          router_id, routes=routes)

    @decorators.attr(type=['nsxv3', 'positive'])
    @decorators.idempotent_id('8155be15-cdc4-4834-be1a-99dbddbee920')
    def test_update_router_nsxv3_with_ipv6_static_route(self):
        """
        Check it should not allow to add static route on router with
        ::/0 next hop.
        """
        kwargs = {"admin_state_up": "True"}
        topology_dict = self.create_topo_single_network("rtr_update",
                                                        create_instance=False,
                                                        set_gateway=False,
                                                        **kwargs)
        next_hop = topology_dict['subnet_v6']['allocation_pools'][0]['end']
        router_state = topology_dict['router_state']
        dest = "3700::/64"
        routes = [{
                  "destination": dest,
                  "nexthop": next_hop
                  }]
        router_id = router_state['id']
        router = self.cmgr_adm.routers_client.update_router(
            router_id, routes=routes)['router']
        self.assertEqual(router['routes'][0]['nexthop'], next_hop)
        self.assertEqual(router['routes'][0]['destination'], dest)
        self.cmgr_adm.routers_client.update_router(router_id, routes=[])

    @decorators.attr(type=['nsxv3', 'negative'])
    @decorators.idempotent_id('93682a0b-253c-4077-9876-b20c75041010')
    def test_update_router_admin_state_ipv6_subnet(self):
        """
        Verify the router admin state can not to changed to False
        for router attached to IPv6 subnet
        """
        kwargs = {"admin_state_up": "True"}
        topology_dict = self.create_topo_single_network("rtr_update",
                                                        create_instance=False,
                                                        set_gateway=False,
                                                        **kwargs)
        router = topology_dict['router_state']
        self.assertEqual(router['admin_state_up'], True)
        kwargs = {"admin_state_up": "False"}
        self.assertRaises(exceptions.BadRequest,
                          self.cmgr_adm.routers_client.update_router,
                          router['id'], **kwargs)

    @decorators.attr(type=['nsxv3', 'negative'])
    @decorators.idempotent_id('a712ca9a-bfa6-47b0-ac59-e971e3a7eb2d')
    def test_deletion_router_ipv6_slacc_interface_use(self):
        """
        Verify the router interface attached to IPv6 subnet
        can be deleted while port is attached to the subnet
        """
        kwargs = {"admin_state_up": "True"}
        topology_dict = self.create_topo_single_network("rtr_update",
                                                        create_instance=True,
                                                        set_gateway=True,
                                                        **kwargs)
        router = topology_dict['router_state']
        subnet_v6 = topology_dict['subnet_v6']
        self.cmgr_adm.routers_client.remove_router_interface(
            router['id'], subnet_id=subnet_v6['id'])

    @decorators.attr(type=['nsxv3', 'negative'])
    @decorators.idempotent_id('4148f175-a33e-4690-ac1b-26f9876de689')
    def test_deletion_router_ipv6_static_interface_use(self):
        """
        Verify the router interface attached to IPv6 subnet
        can be deleted while port is attached to the subnet
        """
        kwargs = {"admin_state_up": "True"}
        topology_dict = self.create_topo_single_dhcpv6_network(
            "rtr_update", create_instance=True, slaac=False,
            set_gateway=True, ipv6_ra_mode="dhcpv6-stateful",
            ipv6_address_mode="dhcpv6-stateful", **kwargs)
        router = topology_dict['router_state']
        subnet_v6 = topology_dict['subnet_v6']
        self.cmgr_adm.routers_client.remove_router_interface(
            router['id'], subnet_id=subnet_v6['id'])

    @decorators.attr(type=['nsxv3', 'negative'])
    @decorators.idempotent_id('38d5814e-3970-42d5-a4b1-6e8af9e0d8e9')
    def test_deletion_router_ipv6_slacc_use(self):
        """
        Verify the router attached to IPv6 subnet
        can not be deleted while port is attached to the subnet
        """
        kwargs = {"admin_state_up": "True"}
        topology_dict = self.create_topo_single_network("rtr_update",
                                                        create_instance=True,
                                                        set_gateway=True,
                                                        **kwargs)
        router = topology_dict['router_state']
        self.assertRaises(exceptions.Conflict,
                          self.cmgr_adm.routers_client.delete_router,
                          router['id'])

    @decorators.attr(type=['nsxv3', 'negative'])
    @decorators.idempotent_id('fd2098e6-6343-4df9-a3d9-1e1e34216d22')
    def test_deletion_router_ipv6_static_use(self):
        """
        Verify the router attached to IPv6 subnet
        can not be deleted while port is attached to the subnet
        """
        kwargs = {"admin_state_up": "True"}
        topology_dict = self.create_topo_single_dhcpv6_network(
            "rtr_update", create_instance=True, slaac=False,
            set_gateway=True, ipv6_ra_mode="dhcpv6-stateful",
            ipv6_address_mode="dhcpv6-stateful", **kwargs)
        router = topology_dict['router_state']
        self.assertRaises(exceptions.Conflict,
                          self.cmgr_adm.routers_client.delete_router,
                          router['id'])

    @decorators.attr(type=['nsxv3', 'positive'])
    @decorators.idempotent_id('85235d0f-89b5-48a0-a3ed-d1bd21be6b94')
    def test_multiple_dhcpv6_stateful_downlink_interface_rtr(self):
        """Test creating multiple dhcpv6 subnets attached
           to router downlink interface
        """
        name = data_utils.rand_name("ipv6-net")
        networks_client = self.cmgr_adm.networks_client
        network = self.create_topology_network(
            name, networks_client=networks_client)
        subnet = self._create_ipv6_subnet(
            network, ipv6_ra_mode="dhcpv6-stateful",
            ipv6_address_mode="dhcpv6-stateful", enable_dhcp=True)
        network_1 = self.create_topology_network(
            name, networks_client=networks_client)
        subnet_1 = self._create_ipv6_subnet(
            network_1, ipv6_ra_mode="dhcpv6-stateful",
            ipv6_address_mode="dhcpv6-stateful", enable_dhcp=True)
        rtr_name = data_utils.rand_name("ipv6-rtr")
        router = self.create_topology_router(
            rtr_name, routers_client=self.cmgr_adm.routers_client)
        interface = self.add_router_interface(
            router_client=self.cmgr_adm.routers_client,
            router_id=router['id'], subnet_id=subnet['id'])
        self.assertEqual(subnet['id'], interface['subnet_id'])
        interface = self.add_router_interface(
            router_client=self.cmgr_adm.routers_client,
            router_id=router['id'], subnet_id=subnet_1['id'])
        self.assertEqual(subnet_1['id'], interface['subnet_id'])

    @decorators.attr(type=['nsxv3', 'positive'])
    @decorators.idempotent_id('85235d0f-89b5-48a0-a3ed-d1bd21be6b94')
    def test_multiple_dhcpv6_stateless_downlink_interface_rtr(self):
        """Test creating multiple dhcpv6 subnets attached
           to router downlink interface
        """
        name = data_utils.rand_name("ipv6-net")
        networks_client = self.cmgr_adm.networks_client
        network = self.create_topology_network(
            name, networks_client=networks_client)
        mode = "dhcpv6-stateless"
        subnet = self._create_ipv6_subnet(network,
                                          ipv6_ra_mode=mode,
                                          ipv6_address_mode=mode,
                                          enable_dhcp=True)
        network_1 = self.create_topology_network(
            name, networks_client=networks_client)
        mode = "dhcpv6-stateless"
        subnet_1 = self._create_ipv6_subnet(network_1,
                                            ipv6_ra_mode=mode,
                                            ipv6_address_mode=mode,
                                            enable_dhcp=True)
        rtr_name = data_utils.rand_name("ipv6-rtr")
        router = self.create_topology_router(
            rtr_name, routers_client=self.cmgr_adm.routers_client)
        interface = self.add_router_interface(
            router_client=self.cmgr_adm.routers_client,
            router_id=router['id'], subnet_id=subnet['id'])
        self.assertEqual(subnet['id'], interface['subnet_id'])
        interface = self.add_router_interface(
            router_client=self.cmgr_adm.routers_client,
            router_id=router['id'], subnet_id=subnet_1['id'])
        self.assertEqual(subnet_1['id'], interface['subnet_id'])

    @decorators.attr(type=['nsxv3', 'positive'])
    @decorators.idempotent_id('85235d0f-89b5-48a0-a3ed-d1bd21be6b94')
    def test_multiple_dhcpv6_heterogeneous_downlink_interface_rtr(self):
        """Test creating multiple dhcpv6 subnets attached
           to router downlink interface
        """
        name = data_utils.rand_name("ipv6-net")
        networks_client = self.cmgr_adm.networks_client
        network = self.create_topology_network(
            name, networks_client=networks_client)
        subnet = self._create_ipv6_subnet(network,
                                          ipv6_ra_mode="dhcpv6-stateful",
                                          ipv6_address_mode="dhcpv6-stateful",
                                          enable_dhcp=True)
        network_1 = self.create_topology_network(
            name, networks_client=networks_client)
        mode = "dhcpv6-stateless"
        subnet_1 = self._create_ipv6_subnet(network_1,
                                            ipv6_ra_mode=mode,
                                            ipv6_address_mode=mode,
                                            enable_dhcp=True)
        rtr_name = data_utils.rand_name("ipv6-rtr")
        router = self.create_topology_router(
            rtr_name, routers_client=self.cmgr_adm.routers_client)
        interface = self.add_router_interface(
            router_client=self.cmgr_adm.routers_client,
            router_id=router['id'], subnet_id=subnet['id'])
        self.assertEqual(subnet['id'], interface['subnet_id'])
        self.assertRaises(exceptions.BadRequest,
                          self.add_router_interface,
                          router_client=self.cmgr_adm.routers_client,
                          router_id=router['id'],
                          subnet_id=subnet_1['id'])

    @decorators.attr(type=['nsxv3', 'positive'])
    @decorators.idempotent_id('8e7b1e28-e50e-458e-beb4-49ce5663c561')
    def test_dhcpv6_stateful_ipv4_ipv6_downlink_interface_rtr(self):
        """Test creating ipv4 and ipv6 slaac subnets attached
           to router downlink interface
        """
        name = data_utils.rand_name("ipv6-net")
        networks_client = self.cmgr_adm.networks_client
        network = self.create_topology_network(name,
                                               networks_client=networks_client)
        subnet_client = self.cmgr_adm.subnets_client
        subnet_name = network['name'] + 'ipv4-sub'
        subnet_ipv4 = self.create_topology_subnet(subnet_name, network,
                                                  subnets_client=subnet_client,
                                                  cidr='20.20.0.0/16')
        subnet = self._create_ipv6_subnet(network,
                                          ipv6_ra_mode="dhcpv6-stateful",
                                          ipv6_address_mode="dhcpv6-stateful")
        rtr_name = data_utils.rand_name("ipv6-rtr")
        router = self.create_topology_router(
            rtr_name, routers_client=self.cmgr_adm.routers_client)
        interface = self.add_router_interface(
            router_client=self.cmgr_adm.routers_client,
            router_id=router['id'], subnet_id=subnet_ipv4['id'])
        self.assertEqual(subnet_ipv4['id'], interface['subnet_id'])
        interface = self.add_router_interface(
            router_client=self.cmgr_adm.routers_client,
            router_id=router['id'], subnet_id=subnet['id'])
        self.assertEqual(subnet['id'], interface['subnet_id'])

    @decorators.attr(type=['nsxv3', 'positive'])
    @decorators.idempotent_id('8e7b1e28-e50e-458e-beb4-49ce5663c561')
    def test_dhcpv6_stateless_ipv4_ipv6_downlink_interface_rtr(self):
        """Test creating ipv4 and ipv6 slaac subnets attached
           to router downlink interface
        """
        name = data_utils.rand_name("ipv6-net")
        networks_client = self.cmgr_adm.networks_client
        network = self.create_topology_network(name,
                                               networks_client=networks_client)
        subnet_client = self.cmgr_adm.subnets_client
        subnet_name = network['name'] + 'ipv4-sub'
        subnet_ipv4 = self.create_topology_subnet(subnet_name, network,
                                                  subnets_client=subnet_client,
                                                  cidr='20.20.0.0/16')
        mode = "dhcpv6-stateless"
        subnet = self._create_ipv6_subnet(network,
                                          ipv6_ra_mode=mode,
                                          ipv6_address_mode=mode)
        rtr_name = data_utils.rand_name("ipv6-rtr")
        router = self.create_topology_router(
            rtr_name, routers_client=self.cmgr_adm.routers_client)
        interface = self.add_router_interface(
            router_client=self.cmgr_adm.routers_client,
            router_id=router['id'], subnet_id=subnet_ipv4['id'])
        self.assertEqual(subnet_ipv4['id'], interface['subnet_id'])
        interface = self.add_router_interface(
            router_client=self.cmgr_adm.routers_client,
            router_id=router['id'], subnet_id=subnet['id'])
        self.assertEqual(subnet['id'], interface['subnet_id'])

    @decorators.attr(type=['nsxv3', 'negative'])
    @decorators.idempotent_id('8cc317db-4bcd-4f56-821a-9040b4a9c740')
    def test_update_router_nsxv3_with_dhcpv6_static_route_via_0_0(self):
        """
        Check it should not allow to add static route on router with
        ::/0 next hop.
        """
        kwargs = {"admin_state_up": "True"}
        mode = "dhcpv6-stateless"
        topology_dict = self.create_topo_single_dhcpv6_network(
            "rtr_update", create_instance=False, set_gateway=False,
            ipv6_ra_mode=mode, ipv6_address_mode=mode, **kwargs)
        router_state = topology_dict['router_state']
        routes = [{
                  "destination": "3700::/64",
                  "nexthop": "::/0"
                  }]
        router_id = router_state['id']
        self.assertRaises(exceptions.BadRequest,
                          self.cmgr_adm.routers_client.update_router,
                          router_id, routes=routes)

    @decorators.attr(type=['nsxv3', 'negative'])
    @decorators.idempotent_id('8cc317db-4bcd-4f56-821a-9040b4a9c740')
    def test_update_router_nsxv3_with_dhcpv6_stateful_static_via_0_0(self):
        """
        Check it should not allow to add static route on router with
        ::/0 next hop.
        """
        kwargs = {"admin_state_up": "True"}
        mode = "dhcpv6-stateful"
        topology_dict = self.create_topo_single_dhcpv6_network(
            "rtr_update", create_instance=False, set_gateway=False,
            ipv6_ra_mode=mode, ipv6_address_mode=mode, **kwargs)
        router_state = topology_dict['router_state']
        routes = [{
                  "destination": "3700::/64",
                  "nexthop": "::/0"
                  }]
        router_id = router_state['id']
        self.assertRaises(exceptions.BadRequest,
                          self.cmgr_adm.routers_client.update_router,
                          router_id, routes=routes)

    @decorators.attr(type=['nsxv3', 'positive'])
    @decorators.idempotent_id('3564b971-6033-43cc-a13a-93b467bca803')
    def test_ipv6_interface_and_ipv4_interface_to_rtr(self):
        """
        Create ipv6 subnet under network1 & attach it to router
        then create an ipv4 subnet under same network1 & attach to rtr
        """
        name = data_utils.rand_name("dual-ipv6-ipv4-net")
        networks_client = self.cmgr_adm.networks_client
        network = self.create_topology_network(
            name, networks_client=networks_client)
        sec_rule_client = self.cmgr_adm.security_group_rules_client
        sec_client = self.cmgr_adm.security_groups_client
        sg = self.create_topology_security_group(
            tenant_id=network['tenant_id'],
            security_groups_client=sec_client,
            security_group_rules_client=sec_rule_client)
        subnet_ipv6 = self._create_ipv6_subnet(
            network, ipv6_ra_mode="dhcpv6-stateful",
            ipv6_address_mode="dhcpv6-stateful",
            slaac=False)
        time.sleep(constants.NSX_NETWORK_REALISE_TIMEOUT)
        rtr_name = data_utils.rand_name("dula-ipv4-ipv6-rtr")
        router = self.create_topology_router(
            rtr_name, routers_client=self.cmgr_adm.routers_client)
        interface = self.add_router_interface(
            router_client=self.cmgr_adm.routers_client,
            router_id=router['id'], subnet_id=subnet_ipv6['id'])
        self.assertEqual(subnet_ipv6['id'], interface['subnet_id'])
        subnet_client = self.cmgr_adm.subnets_client
        subnet_ipv4_name = network['name'] + 'dual-ipv4-sub'
        subnet_ipv4 = self.create_topology_subnet(
            subnet_ipv4_name, network, subnets_client=subnet_client,
            cidr='20.21.0.0/16')
        interface = self.add_router_interface(
            router_client=self.cmgr_adm.routers_client,
            router_id=router['id'], subnet_id=subnet_ipv4['id'])
        self.assertEqual(subnet_ipv4['id'], interface['subnet_id'])
        # Boot two VMs with the same network
        image_id = self.get_glance_image_id(['cirros', 'esx'])
        self.create_topology_instance(
            "vm_1", [network],
            security_groups=[{'name': sg['name']}],
            create_floating_ip=True, image_id=image_id,
            clients=self.cmgr_adm)
        vm1_server_floatingip = self.topology_servers["vm_1"][
            "floating_ips"][0]
        self.create_topology_instance(
            "vm_2", [network],
            security_groups=[{'name': sg['name']}],
            create_floating_ip=True, image_id=image_id,
            clients=self.cmgr_adm)
        vm2_server_floatingip = self.topology_servers["vm_2"][
            "floating_ips"][0]
        time.sleep(constants.NSX_NETWORK_REALISE_TIMEOUT)
        # Check IPv4 connectivity across the same network
        vm1_server = self.topology_servers["vm_1"]
        vm2_server = self.topology_servers["vm_2"]
        self.check_vm_internal_connectivity(
            network, vm1_server_floatingip, vm1_server)
        self.check_vm_internal_connectivity(
            network, vm2_server_floatingip, vm2_server)

    @decorators.attr(type=['nsxv3', 'positive'])
    @decorators.idempotent_id('3564b971-6033-43cc-a13a-93b467bca803')
    def test_ipv4_interface_and_ipv6_interface_to_rtr(self):
        """
        Create ipv4 subnet under network1 & attach it to router
        then create an ipv6 subnet under same network1 & attach to rtr
        """
        name = data_utils.rand_name("dual-ipv4-ipv6-net")
        networks_client = self.cmgr_adm.networks_client
        network = self.create_topology_network(
            name, networks_client=networks_client)
        sec_rule_client = self.cmgr_adm.security_group_rules_client
        sec_client = self.cmgr_adm.security_groups_client
        sg = self.create_topology_security_group(
            tenant_id=network['tenant_id'],
            security_groups_client=sec_client,
            security_group_rules_client=sec_rule_client)
        subnet_client = self.cmgr_adm.subnets_client
        subnet_ipv4_name = network['name'] + 'dual-ipv4-sub'
        subnet_ipv4 = self.create_topology_subnet(
            subnet_ipv4_name, network, subnets_client=subnet_client,
            cidr='20.22.0.0/16')
        rtr_name = data_utils.rand_name("dula-ipv4-ipv6-rtr")
        router = self.create_topology_router(
            rtr_name, routers_client=self.cmgr_adm.routers_client)
        interface = self.add_router_interface(
            router_client=self.cmgr_adm.routers_client,
            router_id=router['id'], subnet_id=subnet_ipv4['id'])
        self.assertEqual(subnet_ipv4['id'], interface['subnet_id'])

        subnet_ipv6 = self._create_ipv6_subnet(
            network,
            ipv6_ra_mode="dhcpv6-stateful",
            ipv6_address_mode="dhcpv6-stateful",
            slaac=False)
        time.sleep(constants.NSX_NETWORK_REALISE_TIMEOUT)
        interface = self.add_router_interface(
            router_client=self.cmgr_adm.routers_client,
            router_id=router['id'], subnet_id=subnet_ipv6['id'])
        self.assertEqual(subnet_ipv6['id'], interface['subnet_id'])
        # Boot two VMs with the same network
        image_id = self.get_glance_image_id(['cirros', 'esx'])
        self.create_topology_instance(
            "vm_1", [network],
            security_groups=[{'name': sg['name']}],
            create_floating_ip=True, image_id=image_id,
            clients=self.cmgr_adm)
        vm1_server_floatingip = self.topology_servers["vm_1"][
            "floating_ips"][0]
        self.create_topology_instance(
            "vm_2", [network],
            security_groups=[{'name': sg['name']}],
            create_floating_ip=True, image_id=image_id,
            clients=self.cmgr_adm)
        vm2_server_floatingip = self.topology_servers["vm_2"][
            "floating_ips"][0]

        time.sleep(constants.NSX_NETWORK_REALISE_TIMEOUT)
        # Check IPv4 connectivity across the same network
        vm1_server = self.topology_servers["vm_1"]
        vm2_server = self.topology_servers["vm_2"]
        self.check_vm_internal_connectivity(
            network, vm1_server_floatingip, vm1_server)
        self.check_vm_internal_connectivity(
            network, vm2_server_floatingip, vm2_server)

    @decorators.attr(type=['nsxv3', 'positive'])
    @decorators.idempotent_id('85235d0f-89b5-48a0-a3ed-d1bd21be6b94')
    def test_multiple_dhcpv6_stateful_downlink_interface_rtr_vlan(self):
        """Test creating multiple dhcpv6 subnets attached
           to router downlink interface
        """
        name = data_utils.rand_name("ipv6-net")
        networks_client = self.cmgr_adm.networks_client
        vlan_id_no = constants.VLAN
        body = {"provider:segmentation_id": vlan_id_no,
                "provider:network_type": constants.VLAN_TYPE,
                "admin_state_up": 'True'}
        network = self.create_topology_network(name,
                                               networks_client=networks_client,
                                               **body)
        subnet = self._create_ipv6_subnet(
            network, ipv6_ra_mode="dhcpv6-stateful",
            ipv6_address_mode="dhcpv6-stateful", enable_dhcp=True)
        network_1 = self.create_topology_network(
            name, networks_client=networks_client)
        subnet_1 = self._create_ipv6_subnet(
            network_1, ipv6_ra_mode="dhcpv6-stateful",
            ipv6_address_mode="dhcpv6-stateful", enable_dhcp=True)
        rtr_name = data_utils.rand_name("ipv6-rtr")
        router = self.create_topology_router(
            rtr_name, routers_client=self.cmgr_adm.routers_client)
        interface = self.add_router_interface(
            router_client=self.cmgr_adm.routers_client,
            router_id=router['id'], subnet_id=subnet['id'])
        self.assertEqual(subnet['id'], interface['subnet_id'])
        interface = self.add_router_interface(
            router_client=self.cmgr_adm.routers_client,
            router_id=router['id'], subnet_id=subnet_1['id'])
        self.assertEqual(subnet_1['id'], interface['subnet_id'])

    @decorators.attr(type=['nsxv3', 'positive'])
    @decorators.idempotent_id('85235d0f-89b5-48a0-a3ed-d1bd21be6b95')
    def test_multiple_dhcpv6_multiple_downlink_interface_rtr_vlan(self):
        """Test creating multiple dhcpv6 subnets attached
           to router downlink interface
        """
        name = data_utils.rand_name("ipv6-net")
        networks_client = self.cmgr_adm.networks_client
        vlan_id_no = constants.VLAN
        body = {"provider:segmentation_id": vlan_id_no,
                "provider:network_type": constants.VLAN_TYPE,
                "admin_state_up": 'True'}
        network = self.create_topology_network(name,
                                               networks_client=networks_client,
                                               **body)
        subnet = self._create_ipv6_subnet(
            network, ipv6_ra_mode="dhcpv6-stateful",
            ipv6_address_mode="dhcpv6-stateful", enable_dhcp=True)
        vlan_id_no = int(vlan_id_no) + 1
        body = {"provider:segmentation_id": vlan_id_no,
                "provider:network_type": constants.VLAN_TYPE,
                "admin_state_up": 'True'}
        network_1 = self.create_topology_network(
            name, networks_client=networks_client, **body)
        subnet_1 = self._create_ipv6_subnet(
            network_1, ipv6_ra_mode="dhcpv6-stateless",
            ipv6_address_mode="dhcpv6-stateless", enable_dhcp=True)
        rtr_name = data_utils.rand_name("ipv6-rtr")
        router = self.create_topology_router(
            rtr_name, routers_client=self.cmgr_adm.routers_client)
        interface = self.add_router_interface(
            router_client=self.cmgr_adm.routers_client,
            router_id=router['id'], subnet_id=subnet['id'])
        self.assertEqual(subnet['id'], interface['subnet_id'])
        interface = self.add_router_interface(
            router_client=self.cmgr_adm.routers_client,
            router_id=router['id'], subnet_id=subnet_1['id'])
        self.assertEqual(subnet_1['id'], interface['subnet_id'])

    @decorators.attr(type=['nsxv3', 'negative'])
    @decorators.idempotent_id('3564b971-6033-43cc-a13a-93b467bca803')
    def test_dual_ipv4_ipv6_interface_with_invalid_gateway_update(self):
        """
        Create ipv6 subnet under network1 & also create ipv4 subnet
        & update gateway of network with invalid value, then test traffic
        """
        name = data_utils.rand_name("dual-ipv6-ipv4-net1")
        networks_client = self.cmgr_adm.networks_client
        network = self.create_topology_network(
            name, networks_client=networks_client)
        sec_rule_client = self.cmgr_adm.security_group_rules_client
        sec_client = self.cmgr_adm.security_groups_client
        sg = self.create_topology_security_group(
            tenant_id=network['tenant_id'],
            security_groups_client=sec_client,
            security_group_rules_client=sec_rule_client)
        subnet_ipv6 = self._create_ipv6_subnet(
            network, ipv6_ra_mode="dhcpv6-stateful",
            ipv6_address_mode="dhcpv6-stateful",
            slaac=False)
        time.sleep(constants.NSX_NETWORK_REALISE_TIMEOUT)

        subnet_client = self.cmgr_adm.subnets_client
        subnet_ipv4_name = network['name'] + 'dual-ipv4-sub'
        subnet_ipv4 = self.create_topology_subnet(
            subnet_ipv4_name, network, subnets_client=subnet_client)
        rtr_name = data_utils.rand_name("dula-ipv4-ipv6-rtr")
        router = self.create_topology_router(
            rtr_name, routers_client=self.cmgr_adm.routers_client)
        interface = self.add_router_interface(
            router_client=self.cmgr_adm.routers_client,
            router_id=router['id'], subnet_id=subnet_ipv6['id'])
        self.assertEqual(subnet_ipv6['id'], interface['subnet_id'])
        interface = self.add_router_interface(
            router_client=self.cmgr_adm.routers_client,
            router_id=router['id'], subnet_id=subnet_ipv4['id'])
        self.assertEqual(subnet_ipv4['id'], interface['subnet_id'])

        # Update invalid gateway of ipv6 subnet within same range
        self.assertRaises(exceptions.BadRequest,
                          self.cmgr_adm.subnets_client.update_subnet,
                          subnet_ipv6['id'], gateway_ip="27::0")

        # Update invalid gateway of ipv6 subnet within different range
        self.assertRaises(exceptions.BadRequest,
                          self.cmgr_adm.subnets_client.update_subnet,
                          subnet_ipv6['id'], gateway_ip="27.0.0.1")

        # Update invalid gateway of ipv4 subnet within same range
        self.assertRaises(exceptions.BadRequest,
                          self.cmgr_adm.subnets_client.update_subnet,
                          subnet_ipv4['id'], gateway_ip="21::0")

        # Update invalid gateway of ipv4 subnet within different range
        self.assertRaises(exceptions.BadRequest,
                          self.cmgr_adm.subnets_client.update_subnet,
                          subnet_ipv4['id'], gateway_ip="27.0.0.1")
        time.sleep(constants.NSXP_BACKEND_SMALL_TIME_INTERVAL)
        # Boot two VMs with the same network
        image_id = self.get_glance_image_id(['cirros', 'esx'])
        self.create_topology_instance(
            "vm_1", [network],
            security_groups=[{'name': sg['name']}],
            create_floating_ip=True, image_id=image_id,
            clients=self.cmgr_adm)
        vm1_server_floatingip = self.topology_servers["vm_1"][
            "floating_ips"][0]
        self.create_topology_instance(
            "vm_2", [network],
            security_groups=[{'name': sg['name']}],
            create_floating_ip=True, image_id=image_id,
            clients=self.cmgr_adm)
        vm2_server_floatingip = self.topology_servers["vm_2"][
            "floating_ips"][0]
        time.sleep(constants.NSX_NETWORK_REALISE_TIMEOUT)
        # Check IPv4 connectivity across the same network
        vm1_server = self.topology_servers["vm_1"]
        vm2_server = self.topology_servers["vm_2"]
        self.check_vm_internal_connectivity(
            network, vm1_server_floatingip, vm1_server)
        self.check_vm_internal_connectivity(
            network, vm2_server_floatingip, vm2_server)
