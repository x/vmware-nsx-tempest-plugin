# Copyright 2020 VMware Inc
# All Rights Reserved
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

import ipaddress
import random
import re

from tempest import config
from tempest.lib import decorators
from vmware_nsx_tempest_plugin.lib import feature_manager
from vmware_nsx_tempest_plugin.services import nsxv3_client
from vmware_nsx_tempest_plugin.services import nsxv_client

from oslo_log import log as logging

CONF = config.CONF
LOG = logging.getLogger(__name__)


class StaticRoutesUniDimensionalTest(feature_manager.FeatureManager):

    """Test Uni Dimesional Case for
       Logical-security-groups
       Logical-security-group-rules

    """
    @classmethod
    def setup_clients(cls):
        super(StaticRoutesUniDimensionalTest, cls).setup_clients()
        cls.cmgr_adm = cls.get_client_manager('admin')
        cls.cmgr_alt = cls.get_client_manager('alt')
        cls.cmgr_adm = cls.get_client_manager('admin')

    @classmethod
    def resource_setup(cls):
        super(StaticRoutesUniDimensionalTest, cls).resource_setup()
        if CONF.network.backend == "nsxv3":
            cls.nsx = nsxv3_client.NSXV3Client(CONF.nsxv3.nsx_manager,
                                               CONF.nsxv3.nsx_user,
                                               CONF.nsxv3.nsx_password)
        elif CONF.network.backend == "nsxv":
            manager_ip = re.search(r"(\d{1,3}\.){3}\d{1,3}",
                                   CONF.nsxv.manager_uri).group(0)
            cls.vsm = nsxv_client.VSMClient(
                manager_ip, CONF.nsxv.user, CONF.nsxv.password)

    @decorators.attr(type='nsxv3')
    @decorators.idempotent_id('a12264a2-daab-451f-ad3b-f0713a390f37')
    def test_create_static_routes_single_nexthop_30routes(self):
        """
        This testcase is to create the static routes
        The same nexthop is used for 30 static routes
        """
        kwargs = {"admin_state_up": "True"}
        topology_dict = self._create_topo_single_network("rtr_update",
                                                         create_instance=False,
                                                         set_gateway=False,
                                                         **kwargs)
        next_hopi = topology_dict.get('subnet_state').get('allocation_pools')
        next_hop = next_hopi[0].get('end')
        router_id = topology_dict.get('router_state', {}).get('id')
        routes = []
        destinations = []
        for i in range(1, 255):
            for j in range(1, 32):
                val = "%s.0.0.0/%s" % (i, j)
                try:
                    x = ipaddress.ip_network(str(val, 'utf-8'))
                    destinations.append(str(x))
                except Exception:
                    pass
        for destination in destinations:
            route = {"destination": destination,
                     "nexthop": next_hop
                     }
            routes.append(route)
        self.routers_client.update_router(router_id,
                                          routes=random.sample(routes, 30))
        self.routers_client.update_router(router_id, routes=[])

    @decorators.attr(type='nsxv3')
    @decorators.idempotent_id('a12264a2-daab-451f-ad3b-f0713a390f37')
    def test_update_multiple_times_static_routes_single_nexthop_30routes(self):
        """
        This testcase is to create the static routes
        The same nexthop is used for 30 static routes,
        The static route is updated multple times.
        """
        kwargs = {"admin_state_up": "True"}
        topology_dict = self._create_topo_single_network("rtr_update",
                                                         create_instance=False,
                                                         set_gateway=False,
                                                         **kwargs)
        next_hopi = topology_dict.get('subnet_state').get('allocation_pools')
        next_hop = next_hopi[0].get('end')
        router_id = topology_dict.get('router_state', {}).get('id')
        routes = []
        destinations = []
        for i in range(1, 255):
            for j in range(1, 32):
                val = "%s.0.0.0/%s" % (i, j)
                try:
                    x = ipaddress.ip_network(str(val, 'utf-8'))
                    destinations.append(str(x))
                except Exception:
                    pass
        for destination in destinations:
            route = {"destination": destination,
                     "nexthop": next_hop
                     }
            routes.append(route)
        for i in range(10):
            self.routers_client.update_router(router_id,
                                              routes=random.sample(routes, 30))
        self.routers_client.update_router(router_id, routes=[])

    @decorators.attr(type='nsxv3')
    @decorators.idempotent_id('a12264a2-daab-451f-ad3b-f0713a390f37')
    def test_create_static_routes_random_nexthop_30routes(self):
        """
        This testcase is to create the static routes
        The same nexthop is used for 30 static routes
        """
        kwargs = {"admin_state_up": "True"}
        topology_dict = self._create_topo_single_network("rtr_update",
                                                         create_instance=False,
                                                         set_gateway=False,
                                                         **kwargs)
        next_hop_dicti = topology_dict.get('subnet_state')
        next_hop_dict = next_hop_dicti.get('allocation_pools')[0]
        router_id = topology_dict.get('router_state', {}).get('id')
        routes = []
        destinations = []
        for i in range(1, 255):
            for j in range(1, 32):
                val = "%s.0.0.0/%s" % (i, j)
                try:
                    x = ipaddress.ip_network(str(val, 'utf-8'))
                    destinations.append(str(x))
                except Exception:
                    pass
        for destination in destinations:
            route = {"destination": destination,
                     "nexthop": random.sample(next_hop_dict.values(), k=1)[0]
                     }
            routes.append(route)
        self.routers_client.update_router(router_id,
                                          routes=random.sample(routes, 30))
        self.routers_client.update_router(router_id, routes=[])

    @decorators.attr(type='nsxv3')
    @decorators.idempotent_id('a12264a2-daab-451f-ad3b-f0713a390f37')
    def test_update_static_routes_random_nexthop_30routes(self):
        """
        This testcase is to create the static routes
        The same nexthop is used for 30 static routes
        """
        kwargs = {"admin_state_up": "True"}
        topology_dict = self._create_topo_single_network("rtr_update",
                                                         create_instance=False,
                                                         set_gateway=False,
                                                         **kwargs)
        next_hop_dicti = topology_dict.get('subnet_state')
        next_hop_dict = next_hop_dicti.get('allocation_pools')[0]
        router_id = topology_dict.get('router_state', {}).get('id')
        routes = []
        destinations = []
        for i in range(1, 255):
            for j in range(1, 32):
                val = "%s.0.0.0/%s" % (i, j)
                try:
                    x = ipaddress.ip_network(str(val, 'utf-8'))
                    destinations.append(str(x))
                except Exception:
                    pass
        for destination in destinations:
            route = {"destination": destination,
                     "nexthop": random.sample(next_hop_dict.values(), k=1)[0]
                     }
            routes.append(route)
        for i in range(100):
            self.routers_client.update_router(router_id,
                                              routes=random.sample(routes, 30))
        self.routers_client.update_router(router_id, routes=[])
