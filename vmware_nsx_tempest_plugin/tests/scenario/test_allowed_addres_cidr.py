# Copyright 2017 VMware Inc
# All Rights Reserved
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

import os
import time

from tempest.common.utils.linux import remote_client
from tempest import config
from tempest.lib.common.utils import data_utils

from tempest.lib import decorators
from tempest.lib import exceptions

from vmware_nsx_tempest_plugin.common import constants
from vmware_nsx_tempest_plugin.lib import feature_manager
from vmware_nsx_tempest_plugin.services import nsxp_client
from vmware_nsx_tempest_plugin.services import nsxv3_client

CONF = config.CONF
LOG = constants.log.getLogger(__name__)


class TestAllowedAddresCidr(feature_manager.FeatureManager):

    """Test New Cases Scenario

    """
    @classmethod
    def setup_clients(cls):
        super(TestAllowedAddresCidr, cls).setup_clients()
        cls.cmgr_adm = cls.get_client_manager('admin')
        cls.cmgr_alt = cls.get_client_manager('alt')
        cls.cmgr_adm = cls.get_client_manager('admin')
        cls.routers_client = cls.cmgr_adm.routers_client
        cls.networks_client = cls.cmgr_adm.networks_client
        cls.subnets_client = cls.cmgr_adm.subnets_client
        cls.sec_rule_client = cls.cmgr_adm.security_group_rules_client
        cls.sec_client = cls.cmgr_adm.security_groups_client

    def setUp(self):
        super(TestAllowedAddresCidr, self).setUp()
        CONF.validation.ssh_shell_prologue = ''
        self.vip_ip_address = ''
        self.namestart = 'lbaas-ops'
        self.poke_counters = 12
        self.hm_delay = 4
        self.hm_max_retries = 3
        self.hm_timeout = 10
        self.server_names = []
        self.loadbalancer = None
        self.vip_fip = None
        self.web_service_start_delay = 2.5

    @classmethod
    def resource_setup(cls):
        super(TestAllowedAddresCidr, cls).resource_setup()
        cls.nsx = nsxv3_client.NSXV3Client(CONF.nsxv3.nsx_manager,
                                           CONF.nsxv3.nsx_user,
                                           CONF.nsxv3.nsx_password)
        cls.nsxp = nsxp_client.NSXPClient(CONF.nsxv3.nsx_manager,
                                          CONF.nsxv3.nsx_user,
                                          CONF.nsxv3.nsx_password)

    @decorators.idempotent_id('4441119c-14ca-1428-a428-09956daa46c3')
    def test_allowed_address_cidr_octavia_lb(self):
        """
        Create NAT and Firewall rules on router.
        Verify order of NAT and Firewall.
        """
        kwargs = {"enable_snat": True}
        topo_dict = self._create_topo_single_network(
            'tempest_allowed_cidr', create_instance=False,
            set_gateway=True, **kwargs)
        network_state = topo_dict['network_state']
        time.sleep(constants.NSX_NETWORK_REALISE_TIMEOUT)
        sec_rule_client = self.cmgr_adm.security_group_rules_client
        sec_client = self.cmgr_adm.security_groups_client
        kwargs = dict(tenant_id=network_state['tenant_id'],
                      security_group_rules_client=sec_rule_client,
                      security_groups_client=sec_client)
        self.sg = self._create_security_group(
            security_group_rules_client=self.cmgr_adm.
            security_group_rules_client,
            security_groups_client=self.cmgr_adm.security_groups_client)
        lbaas_rules = [dict(direction='ingress', protocol='tcp',
                            port_range_min=constants.HTTP_PORT,
                            port_range_max=constants.HTTP_PORT, ),
                       dict(direction='ingress', protocol='tcp',
                            port_range_min=443, port_range_max=443, ),
                       dict(direction='ingress', protocol='tcp',
                            port_range_min=constants.HTTP_PORT,
                            port_range_max=constants.HTTP_PORT,
                            ethertype='IPv6'),
                       dict(direction='ingress', protocol='tcp',
                            port_range_min=443, port_range_max=443,
                            ethertype='IPv6')]
        for rule in lbaas_rules:
            self.add_security_group_rule(
                self.sg,
                rule,
                ruleclient=self.cmgr_adm.security_group_rules_client,
                secclient=self.cmgr_adm.security_groups_client,
                tenant_id=network_state['tenant_id'])
        security_groups = [{'name': self.sg['name']}]
        image_id = self.get_glance_image_id(["cirros", "esx"])
        self.create_topology_instance(
            "state_vm_1", [network_state],
            create_floating_ip=True, image_id=image_id, clients=self.cmgr_adm,
            security_groups=security_groups)
        self.create_topology_instance(
            "state_vm_2", [network_state],
            create_floating_ip=True, image_id=image_id, clients=self.cmgr_adm,
            security_groups=security_groups)
        self.start_web_servers(constants.HTTP_PORT)
        lb_cist = self.create_project_octavia(
                    protocol_type="HTTP", protocol_port="80",
                    lb_algorithm="LEAST_CONNECTIONS",
                    vip_net_id=network_state['id'])
        allowed_cidrs = {'allowed_cidrs': ['3.0.0.0/24', '4.0.0.0/24']}
        self.octavia_admin_listener_client.\
            update_octavia_listener(lb_cist['listener_id'],
                                    listener_data=allowed_cidrs)['listener']
        time.sleep(constants.NSX_NETWORK_REALISE_TIMEOUT)
        self.check_project_lbaas(no_resp=True)
        out = os.popen("sudo ifconfig eth1 | grep 'inet'"
                       "| awk {'print$2'} | "
                       "cut -f2 -d':'").read().strip()
        allowed_cidrs = {'allowed_cidrs': ['%s/24' % out]}
        self.octavia_admin_listener_client.\
            update_octavia_listener(lb_cist['listener_id'],
                                    listener_data=allowed_cidrs)['listener']
        time.sleep(constants.NSX_NETWORK_REALISE_TIMEOUT)
        self.check_project_lbaas(no_resp=False)
        self.delete_octavia_lb_resources(lb_cist['lb_id'])

    @decorators.idempotent_id('2317449c-11cc-1428-a428-09956daa46c3')
    def test_allowed_address_ipv6_cidr_octavia_lb(self):
        """
        Create NAT and Firewall rules on router.
        Verify order of NAT and Firewall.
        """
        kwargs = {"enable_snat": True}
        mode = "dhcpv6-stateless"
        network_name = "test-net"
        network = self.create_topology_network(
            network_name, networks_client=self.cmgr_adm.networks_client)
        sec_rule_client = self.cmgr_adm.security_group_rules_client
        sec_client = self.cmgr_adm.security_groups_client
        kwargs = dict(tenant_id=network['tenant_id'],
                      security_group_rules_client=sec_rule_client,
                      security_groups_client=sec_client)
        self.sg = self._create_security_group(
            security_group_rules_client=self.cmgr_adm.
            security_group_rules_client,
            security_groups_client=self.cmgr_adm.security_groups_client)
        lbaas_rules = [dict(direction='ingress', protocol='tcp'),
                       dict(direction='ingress', protocol='tcp',
                            ethertype='IPv6'),
                       dict(direction='egress', protocol='tcp',
                            ethertype='IPv6'),
                       dict(direction='egress', protocol='tcp')]
        for rule in lbaas_rules:
            self.add_security_group_rule(
                self.sg,
                rule,
                ruleclient=self.cmgr_adm.security_group_rules_client,
                secclient=self.cmgr_adm.security_groups_client,
                tenant_id=network['tenant_id'])
        kwargs = {"admin_state_up": "True"}
        security_groups = [{'name': self.sg['name']}]
        topology_dict = self.create_topo_single_dhcpv6_network(
            "allowed_address_cidr", create_instance=True,
            set_gateway=True,
            ipv6_ra_mode=mode, ipv6_address_mode=mode,
            ipv6cidr="1300::/64", cidr="13.0.0.0/24",
            security_groups=security_groups, **kwargs)
        subnetipv6_state = topology_dict['subnet_v6']
        topology_dict = self.create_topo_single_dhcpv6_network(
            "allowed_address_cidr", create_instance=False,
            set_gateway=True,
            ipv6_ra_mode=mode, ipv6_address_mode=mode,
            ipv6cidr="3500::/64", cidr="35.0.0.0/24",
            security_groups=security_groups, **kwargs)
        network_state1 = topology_dict['network_state']
        topology_dict = self.create_topo_single_dhcpv6_network(
            "allowed_address_cidr",
            set_gateway=True,
            ipv6_ra_mode=mode, ipv6_address_mode=mode,
            ipv6cidr="4600::/64", cidr="46.0.0.0/24",
            security_groups=security_groups,
            create_instance=False, **kwargs)
        network_state2 = topology_dict['network_state']
        time.sleep(constants.NSX_NETWORK_REALISE_TIMEOUT)
        self.start_web_servers(constants.HTTP_PORT)
        lb_cist = self.create_project_octavia(
                    protocol_type="HTTP", protocol_port="80",
                    lb_algorithm="LEAST_CONNECTIONS",
                    vip_subnet_id=subnetipv6_state['id'],
                    create_fip=False, ipv6=True)
        security_groups = [{'name': self.sg['name']}]
        image_id = self.get_glance_image_id(["cirros", "esx"])
        server3 = self.create_topology_instance(
            "state_vm_3", [network_state1],
            create_floating_ip=True, image_id=image_id, clients=self.cmgr_adm,
            security_groups=security_groups)
        server4 = self.create_topology_instance(
            "state_vm_4", [network_state2],
            create_floating_ip=True, image_id=image_id, clients=self.cmgr_adm,
            security_groups=security_groups)
        allowed_cidrs = {'allowed_cidrs': ['3500::/64', '4600::/64']}
        self.octavia_admin_listener_client.\
            update_octavia_listener(lb_cist['listener_id'],
                                    listener_data=allowed_cidrs)['listener']
        fip1 = server3["floating_ips"][0]["floating_ip_address"]
        fip2 = server4["floating_ips"][0]["floating_ip_address"]
        ssh_client = remote_client.RemoteClient(
                fip1, 'cirros', 'gocubsgo')
        command = "curl http://[%s]" % lb_cist['loadbalancer']['vip_address']
        for i in range(4):
            data = ssh_client.exec_command(command)
            self.assertIn(data, ['state_vm_1', 'state_vm_2'])
        time.sleep(constants.NSXP_BACKEND_SMALL_TIME_INTERVAL)
        ssh_client = remote_client.RemoteClient(
                fip2, 'cirros', 'gocubsgo')
        command = "curl http://[%s]" % lb_cist['loadbalancer']['vip_address']
        for i in range(4):
            data = ssh_client.exec_command(command)
            self.assertIn(data, ['state_vm_1', 'state_vm_2'])
        allowed_cidrs = {'allowed_cidrs': ["6700::/64"]}
        self.octavia_admin_listener_client.\
            update_octavia_listener(lb_cist['listener_id'],
                                    listener_data=allowed_cidrs)['listener']
        fip1 = server3["floating_ips"][0]["floating_ip_address"]
        fip2 = server4["floating_ips"][0]["floating_ip_address"]
        ssh_client = remote_client.RemoteClient(
                fip1, 'cirros', 'gocubsgo')
        command = "curl http://[%s]" % lb_cist['loadbalancer']['vip_address']
        time.sleep(constants.NSXP_BACKEND_SMALL_TIME_INTERVAL)
        for i in range(4):
            self.assertRaises(
                exceptions.SSHExecCommandFailed,
                ssh_client.exec_command, command)
        time.sleep(constants.NSXP_BACKEND_SMALL_TIME_INTERVAL)
        ssh_client = remote_client.RemoteClient(
                fip2, 'cirros', 'gocubsgo')
        command = "curl http://[%s]" % lb_cist['loadbalancer']['vip_address']
        for i in range(4):
            self.assertRaises(
                exceptions.SSHExecCommandFailed,
                ssh_client.exec_command, command)
        allowed_cidrs = {'allowed_cidrs': []}
        self.octavia_admin_listener_client.\
            update_octavia_listener(lb_cist['listener_id'],
                                    listener_data=allowed_cidrs)['listener']
        fip1 = server3["floating_ips"][0]["floating_ip_address"]
        fip2 = server4["floating_ips"][0]["floating_ip_address"]
        ssh_client = remote_client.RemoteClient(
                fip1, 'cirros', 'gocubsgo')
        command = "curl http://[%s]" % lb_cist['loadbalancer']['vip_address']
        time.sleep(constants.NSXP_BACKEND_SMALL_TIME_INTERVAL)
        for i in range(4):
            data = ssh_client.exec_command(command)
            self.assertIn(data, ['state_vm_1', 'state_vm_2'])
        time.sleep(constants.NSXP_BACKEND_SMALL_TIME_INTERVAL)
        ssh_client = remote_client.RemoteClient(
                fip2, 'cirros', 'gocubsgo')
        command = "curl http://[%s]" % lb_cist['loadbalancer']['vip_address']
        for i in range(4):
            data = ssh_client.exec_command(command)
            self.assertIn(data, ['state_vm_1', 'state_vm_2'])
        self.delete_octavia_lb_resources(lb_cist['lb_id'])

    @decorators.idempotent_id('2317449c-14ca-1428-a444-09956daa46c3')
    def test_allowed_address_0_0_0_0_cidr_octavia_lb(self):
        """
        Create NAT and Firewall rules on router.
        Verify order of NAT and Firewall.
        """
        kwargs = {"enable_snat": True}
        topo_dict = self._create_topo_single_network(
            'tempest_allowed_cidr', create_instance=False,
            set_gateway=True, **kwargs)
        network_state = topo_dict['network_state']
        time.sleep(constants.NSX_NETWORK_REALISE_TIMEOUT)
        sec_rule_client = self.cmgr_adm.security_group_rules_client
        sec_client = self.cmgr_adm.security_groups_client
        kwargs = dict(tenant_id=network_state['tenant_id'],
                      security_group_rules_client=sec_rule_client,
                      security_groups_client=sec_client)
        self.sg = self._create_security_group(
            security_group_rules_client=self.cmgr_adm.
            security_group_rules_client,
            security_groups_client=self.cmgr_adm.security_groups_client)
        lbaas_rules = [dict(direction='ingress', protocol='tcp',
                            port_range_min=constants.HTTP_PORT,
                            port_range_max=constants.HTTP_PORT, ),
                       dict(direction='ingress', protocol='tcp',
                            port_range_min=443, port_range_max=443, ),
                       dict(direction='ingress', protocol='tcp',
                            port_range_min=constants.HTTP_PORT,
                            port_range_max=constants.HTTP_PORT,
                            ethertype='IPv6'),
                       dict(direction='ingress', protocol='tcp',
                            port_range_min=443, port_range_max=443,
                            ethertype='IPv6')]
        for rule in lbaas_rules:
            self.add_security_group_rule(
                self.sg,
                rule,
                ruleclient=self.cmgr_adm.security_group_rules_client,
                secclient=self.cmgr_adm.security_groups_client,
                tenant_id=network_state['tenant_id'])
        security_groups = [{'name': self.sg['name']}]
        image_id = self.get_glance_image_id(["cirros", "esx"])
        self.create_topology_instance(
            "state_vm_1", [network_state],
            create_floating_ip=True, image_id=image_id, clients=self.cmgr_adm,
            security_groups=security_groups)
        self.create_topology_instance(
            "state_vm_2", [network_state],
            create_floating_ip=True, image_id=image_id, clients=self.cmgr_adm,
            security_groups=security_groups)
        self.start_web_servers(constants.HTTP_PORT)
        lb_cist = self.create_project_octavia(
                    protocol_type="HTTP", protocol_port="80",
                    lb_algorithm="LEAST_CONNECTIONS",
                    vip_net_id=network_state['id'])
        allowed_cidrs = {'allowed_cidrs': ['0.0.0.0/0']}
        self.octavia_admin_listener_client.\
            update_octavia_listener(lb_cist['listener_id'],
                                    listener_data=allowed_cidrs)['listener']
        time.sleep(constants.NSX_NETWORK_REALISE_TIMEOUT)
        self.check_project_lbaas(no_resp=False)
        allowed_cidrs = {'allowed_cidrs': ['0.0.0.0/24']}
        self.octavia_admin_listener_client.\
            update_octavia_listener(lb_cist['listener_id'],
                                    listener_data=allowed_cidrs)['listener']
        time.sleep(constants.NSX_NETWORK_REALISE_TIMEOUT)
        self.check_project_lbaas(no_resp=True)
        out = os.popen("sudo ifconfig eth1 | grep 'inet'"
                       "| awk {'print$2'} | "
                       "cut -f2 -d':'").read().strip()
        allowed_cidrs = {'allowed_cidrs': ['%s/24' % out]}
        self.octavia_admin_listener_client.\
            update_octavia_listener(lb_cist['listener_id'],
                                    listener_data=allowed_cidrs)['listener']
        time.sleep(constants.NSX_NETWORK_REALISE_TIMEOUT)
        self.check_project_lbaas(no_resp=False)
        self.delete_octavia_lb_resources(lb_cist['lb_id'])

    @decorators.idempotent_id('2317449c-14ca-1428-a428-00006daa46c3')
    def test_allowed_address_cidr_barbican_octavia_lb(self):
        """
        Create NAT and Firewall rules on router.
        Verify order of NAT and Firewall.
        """
        kwargs = {"enable_snat": True}
        topo_dict = self._create_topo_single_network(
            'tempest_allowed_cidr', create_instance=False,
            set_gateway=True, **kwargs)
        network_state = topo_dict['network_state']
        time.sleep(constants.NSX_NETWORK_REALISE_TIMEOUT)
        sec_rule_client = self.cmgr_adm.security_group_rules_client
        sec_client = self.cmgr_adm.security_groups_client
        kwargs = dict(tenant_id=network_state['tenant_id'],
                      security_group_rules_client=sec_rule_client,
                      security_groups_client=sec_client)
        self.sg = self._create_security_group(
            security_group_rules_client=self.cmgr_adm.
            security_group_rules_client,
            security_groups_client=self.cmgr_adm.security_groups_client)
        lbaas_rules = [dict(direction='ingress', protocol='tcp'),
                       dict(direction='egress', protocol='tcp'),
                       dict(direction='ingress', protocol='tcp',
                            ethertype='IPv6'),
                       dict(direction='egress', protocol='tcp',
                            ethertype='IPv6')]
        for rule in lbaas_rules:
            self.add_security_group_rule(
                self.sg,
                rule,
                ruleclient=self.cmgr_adm.security_group_rules_client,
                secclient=self.cmgr_adm.security_groups_client,
                tenant_id=network_state['tenant_id'])
        security_groups = [{'name': self.sg['name']}]
        image_id = self.get_glance_image_id(["cirros", "esx"])
        self.create_topology_instance(
            "state_vm_1", [network_state],
            create_floating_ip=True, image_id=image_id, clients=self.cmgr_adm,
            security_groups=security_groups)
        self.create_topology_instance(
            "state_vm_2", [network_state],
            create_floating_ip=True, image_id=image_id, clients=self.cmgr_adm,
            security_groups=security_groups)
        self.start_web_servers(constants.HTTP_PORT)
        barbican_secrets = self.create_barbican_secret_conatainer(
            constants.CERT_FILE, constants.KEY_FILE)
        barbican_container = barbican_secrets['secret_container']
        lb_cist = self.create_project_octavia(
                                    protocol_type="TERMINATED_HTTPS",
                                    protocol_port="443",
                                    lb_algorithm="LEAST_CONNECTIONS",
                                    vip_net_id=network_state['id'],
                                    hm_type='HTTP',
                                    member_count=2,
                                    weight=5,
                                    pool_protocol='HTTP',
                                    pool_port='80',
                                    barbican_container=barbican_container,
                                    count=0, barbican=True,
                                    delay=self.hm_delay,
                                    max_retries=self.hm_max_retries,
                                    timeout=self.hm_timeout)
        allowed_cidrs = {'allowed_cidrs': ['0.0.0.0/0']}
        self.octavia_admin_listener_client.\
            update_octavia_listener(lb_cist['listener_id'],
                                    listener_data=allowed_cidrs)['listener']
        time.sleep(constants.NSX_NETWORK_REALISE_TIMEOUT)
        self.check_project_lbaas(no_resp=False, HTTPS=True)
        allowed_cidrs = {'allowed_cidrs': ['0.0.0.0/24']}
        self.octavia_admin_listener_client.\
            update_octavia_listener(lb_cist['listener_id'],
                                    listener_data=allowed_cidrs)['listener']
        time.sleep(constants.NSX_NETWORK_REALISE_TIMEOUT)
        self.check_project_lbaas(no_resp=True, HTTPS=True)
        out = os.popen("sudo ifconfig eth1 | grep 'inet'"
                       "| awk {'print$2'} | "
                       "cut -f2 -d':'").read().strip()
        allowed_cidrs = {'allowed_cidrs': ['%s/24' % out]}
        self.octavia_admin_listener_client.\
            update_octavia_listener(lb_cist['listener_id'],
                                    listener_data=allowed_cidrs)['listener']
        time.sleep(constants.NSX_NETWORK_REALISE_TIMEOUT)
        self.check_project_lbaas(no_resp=False, HTTPS=True)
        allowed_cidrs = {'allowed_cidrs': []}
        self.octavia_admin_listener_client.\
            update_octavia_listener(lb_cist['listener_id'],
                                    listener_data=allowed_cidrs)['listener']
        time.sleep(constants.NSX_NETWORK_REALISE_TIMEOUT)
        self.check_project_lbaas(no_resp=False, HTTPS=True)
        self.delete_octavia_lb_resources(lb_cist['lb_id'])

    @decorators.idempotent_id('2317449c-14ca-1428-a428-99996daa46c3')
    def test_allowed_address_ipv6_cidr_barbican_octavia_lb(self):
        """
        Create NAT and Firewall rules on router.
        Verify order of NAT and Firewall.
        """
        kwargs = {"enable_snat": True}
        mode = "dhcpv6-stateless"
        network_name = data_utils.rand_name(name='tempest-net')
        network_state1 = self.create_topology_network(
            network_name, networks_client=self.cmgr_adm.networks_client)
        time.sleep(constants.NSX_NETWORK_REALISE_TIMEOUT)
        sec_rule_client = self.cmgr_adm.security_group_rules_client
        sec_client = self.cmgr_adm.security_groups_client
        kwargs = dict(tenant_id=network_state1['tenant_id'],
                      security_group_rules_client=sec_rule_client,
                      security_groups_client=sec_client)
        self.sg = self._create_security_group(
            security_group_rules_client=self.cmgr_adm.
            security_group_rules_client,
            security_groups_client=self.cmgr_adm.security_groups_client)
        lbaas_rules = [dict(direction='ingress', protocol='tcp'),
                       dict(direction='ingress', protocol='tcp',
                            ethertype='IPv6'),
                       dict(direction='egress', protocol='tcp',
                            ethertype='IPv6'),
                       dict(direction='egress', protocol='tcp')]
        for rule in lbaas_rules:
            self.add_security_group_rule(
                self.sg,
                rule,
                ruleclient=self.cmgr_adm.security_group_rules_client,
                secclient=self.cmgr_adm.security_groups_client,
                tenant_id=network_state1['tenant_id'])
        kwargs = {"admin_state_up": "True"}
        security_groups = [{'name': self.sg['name']}]
        topology_dict = self.create_topo_single_dhcpv6_network(
            "allowed_address_cidr", create_instance=True,
            set_gateway=True,
            ipv6_ra_mode=mode, ipv6_address_mode=mode,
            ipv6cidr="1300::/64", cidr="13.0.0.0/24",
            security_groups=security_groups, **kwargs)
        subnetipv6_state = topology_dict['subnet_v6']
        topology_dict = self.create_topo_single_dhcpv6_network(
            "allowed_address_cidr", create_instance=False,
            set_gateway=True,
            ipv6_ra_mode=mode, ipv6_address_mode=mode,
            ipv6cidr="3500::/64", cidr="35.0.0.0/24",
            security_groups=security_groups, **kwargs)
        network_state1 = topology_dict['network_state']
        topology_dict = self.create_topo_single_dhcpv6_network(
            "allowed_address_cidr",
            set_gateway=True,
            ipv6_ra_mode=mode, ipv6_address_mode=mode,
            ipv6cidr="4600::/64", cidr="46.0.0.0/24",
            security_groups=security_groups,
            create_instance=False, **kwargs)
        network_state2 = topology_dict['network_state']
        time.sleep(constants.NSX_NETWORK_REALISE_TIMEOUT)
        self.start_web_servers(constants.HTTP_PORT)
        barbican_secrets = self.create_barbican_secret_conatainer(
            constants.CERT_FILE, constants.KEY_FILE)
        barbican_container = barbican_secrets['secret_container']
        lb_cist = self.create_project_octavia(
                    protocol_type="TERMINATED_HTTPS",
                    protocol_port="443",
                    lb_algorithm="LEAST_CONNECTIONS",
                    vip_subnet_id=subnetipv6_state['id'],
                    hm_type='HTTP',
                    member_count=2,
                    weight=5,
                    pool_protocol='HTTP',
                    pool_port='80',
                    barbican_container=barbican_container,
                    count=0, barbican=True,
                    delay=self.hm_delay,
                    max_retries=self.hm_max_retries,
                    timeout=self.hm_timeout,
                    create_fip=False, ipv6=True)
        security_groups = [{'name': self.sg['name']}]
        image_id = self.get_glance_image_id(["cirros", "esx"])
        server3 = self.create_topology_instance(
            "state_vm_3", [network_state1],
            create_floating_ip=True, image_id=image_id, clients=self.cmgr_adm,
            security_groups=security_groups)
        server4 = self.create_topology_instance(
            "state_vm_4", [network_state2],
            create_floating_ip=True, image_id=image_id, clients=self.cmgr_adm,
            security_groups=security_groups)
        allowed_cidrs = {'allowed_cidrs': ['3500::/64', '4600::/64']}
        self.octavia_admin_listener_client.\
            update_octavia_listener(lb_cist['listener_id'],
                                    listener_data=allowed_cidrs)['listener']
        fip1 = server3["floating_ips"][0]["floating_ip_address"]
        fip2 = server4["floating_ips"][0]["floating_ip_address"]
        ssh_client = remote_client.RemoteClient(
                fip1, 'cirros', 'gocubsgo')
        command = "curl -k https://[%s]" % \
            lb_cist['loadbalancer']['vip_address']
        for i in range(4):
            data = ssh_client.exec_command(command)
            self.assertIn(data, ['state_vm_1', 'state_vm_2'])
        time.sleep(constants.NSXP_BACKEND_SMALL_TIME_INTERVAL)
        ssh_client = remote_client.RemoteClient(
                fip2, 'cirros', 'gocubsgo')
        command = "curl -k https://[%s]" % \
            lb_cist['loadbalancer']['vip_address']
        for i in range(4):
            data = ssh_client.exec_command(command)
            self.assertIn(data, ['state_vm_1', 'state_vm_2'])
        allowed_cidrs = {'allowed_cidrs': ["6700::/64"]}
        self.octavia_admin_listener_client.\
            update_octavia_listener(lb_cist['listener_id'],
                                    listener_data=allowed_cidrs)['listener']
        fip1 = server3["floating_ips"][0]["floating_ip_address"]
        fip2 = server4["floating_ips"][0]["floating_ip_address"]
        ssh_client = remote_client.RemoteClient(
                fip1, 'cirros', 'gocubsgo')
        command = "curl -k https://[%s]" % \
            lb_cist['loadbalancer']['vip_address']
        time.sleep(constants.NSXP_BACKEND_SMALL_TIME_INTERVAL)
        for i in range(4):
            self.assertRaises(
                exceptions.SSHExecCommandFailed,
                ssh_client.exec_command, command)
        time.sleep(constants.NSXP_BACKEND_SMALL_TIME_INTERVAL)
        ssh_client = remote_client.RemoteClient(
                fip2, 'cirros', 'gocubsgo')
        command = "curl -k https://[%s]" % \
            lb_cist['loadbalancer']['vip_address']
        for i in range(4):
            self.assertRaises(
                exceptions.SSHExecCommandFailed,
                ssh_client.exec_command, command)
        allowed_cidrs = {'allowed_cidrs': []}
        self.octavia_admin_listener_client.\
            update_octavia_listener(lb_cist['listener_id'],
                                    listener_data=allowed_cidrs)['listener']
        fip1 = server3["floating_ips"][0]["floating_ip_address"]
        fip2 = server4["floating_ips"][0]["floating_ip_address"]
        ssh_client = remote_client.RemoteClient(
                fip1, 'cirros', 'gocubsgo')
        command = "curl -k https://[%s]" % \
            lb_cist['loadbalancer']['vip_address']
        time.sleep(constants.NSXP_BACKEND_SMALL_TIME_INTERVAL)
        for i in range(4):
            data = ssh_client.exec_command(command)
            self.assertIn(data, ['state_vm_1', 'state_vm_2'])
        time.sleep(constants.NSXP_BACKEND_SMALL_TIME_INTERVAL)
        ssh_client = remote_client.RemoteClient(
                fip2, 'cirros', 'gocubsgo')
        command = "curl -k https://[%s]" % \
            lb_cist['loadbalancer']['vip_address']
        for i in range(4):
            data = ssh_client.exec_command(command)
            self.assertIn(data, ['state_vm_1', 'state_vm_2'])
        self.delete_octavia_lb_resources(lb_cist['lb_id'])
