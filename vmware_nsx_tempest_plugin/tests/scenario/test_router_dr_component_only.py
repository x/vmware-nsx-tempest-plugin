# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
import time

from oslo_log import log as logging
from tempest import config
from tempest.lib.common.utils import data_utils
from tempest.lib.common.utils import test_utils

from tempest.lib import decorators

from vmware_nsx_tempest_plugin.common import constants
from vmware_nsx_tempest_plugin.lib import feature_manager
from vmware_nsx_tempest_plugin.services import nsxv3_client

CONF = config.CONF

LOG = logging.getLogger(__name__)


class TestTier1DRComponentDeployment(feature_manager.FeatureManager):
    """Test TestTier1DRComponentDeployment

    Adding test cases to test deploy tier1
    on sepcific edge_cluster.
    """
    @classmethod
    def setup_clients(cls):
        super(TestTier1DRComponentDeployment, cls).setup_clients()
        cls.cmgr_adm = cls.get_client_manager('admin')
        cls.cmgr_alt = cls.get_client_manager('alt')
        cls.cmgr_adm = cls.get_client_manager('admin')
        cls.routers_client = cls.cmgr_adm.routers_client
        cls.networks_client = cls.cmgr_adm.networks_client
        cls.subnets_client = cls.cmgr_adm.subnets_client
        cls.sec_rule_client = cls.cmgr_adm.security_group_rules_client
        cls.sec_client = cls.cmgr_adm.security_groups_client

    def setUp(self):
        super(TestTier1DRComponentDeployment, self).setUp()
        self.nsx = nsxv3_client.NSXV3Client(CONF.nsxv3.nsx_manager,
                                            CONF.nsxv3.nsx_user,
                                            CONF.nsxv3.nsx_password)
        CONF.validation.ssh_shell_prologue = ''
        self.vip_ip_address = ''
        self.namestart = 'lbaas-ops'
        self.poke_counters = 12
        self.hm_delay = 4
        self.hm_max_retries = 3
        self.hm_timeout = 10
        self.server_names = []
        self.loadbalancer = None
        self.vip_fip = None
        self.web_service_start_delay = 2.5

    @classmethod
    def skip_checks(cls):
        """Class level skip checks.

        Class level check. Skip all the MDproxy tests, if native_dhcp_metadata
        is not True under nsxv3 section of the config
        """
        super(TestTier1DRComponentDeployment, cls).skip_checks()

    def create_topo_single_network(self, namestart, create_instance=True,
                                   set_gateway=True, instance_count=None,
                                   **kwargs):
        """
        Create Topo where 1 logical switches which is
        connected via tier-1 router.
        """
        rtr_name = data_utils.rand_name(name='tempest-router')
        network_name = data_utils.rand_name(name='tempest-net')
        subnet_name = data_utils.rand_name(name='tempest-subnet')
        router_state = self.create_topology_router(rtr_name,
                                                   set_gateway=set_gateway,
                                                   **kwargs)
        network_state = self.create_topology_network(network_name)
        subnet_state = self.create_topology_subnet(subnet_name, network_state,
                                                   router_id=router_state["id"]
                                                   )
        time.sleep(constants.NSX_NETWORK_REALISE_TIMEOUT)
        if create_instance:
            image_id = self.get_glance_image_id(["cirros", "esx"])
            self.create_topology_instance(
                "state_vm_1", [network_state],
                create_floating_ip=True, image_id=image_id)
            self.create_topology_instance(
                "state_vm_2", [network_state],
                create_floating_ip=True, image_id=image_id)
        topology_dict = dict(router_state=router_state,
                             network_state=network_state,
                             subnet_state=subnet_state)
        return topology_dict

    @decorators.idempotent_id('1206238b-91cc-0987-b217-09955dbb58d4')
    def test_only_dr_componet_of_router_should_present(self):
        """
        Check only dr component of router should be present
        on edge
        """
        kwargs = {"enable_snat": False}
        network_name = data_utils.rand_name(name='tempest-net')
        subnet_name = data_utils.rand_name(name='tempest-subnet')
        router_state = self.create_topology_router(
            set_gateway=True,
            routers_client=self.cmgr_adm.routers_client, **kwargs)
        network_state = self.create_topology_network(
            network_name, networks_client=self.cmgr_adm.networks_client)
        self.create_topology_subnet(
            subnet_name, network_state, router_id=router_state["id"],
            routers_client=self.cmgr_adm.routers_client,
            subnets_client=self.cmgr_adm.subnets_client)
        time.sleep(constants.NSXP_BACKEND_SMALL_TIME_INTERVAL)
        result = self.check_router_components_on_edge(router_state)
        self.assertEqual(True, result[0]['dr_present'])
        self.assertEqual(False, result[1]['sr_present'])

    @decorators.idempotent_id('1206238b-02dd-1098-c228-09955dbb58d4')
    def test_tier1_sr_component_should_present(self):
        """
        Check sr and dr component of router should be present
        on edge, check N-S and E-W traffic
        """
        kwargs = {"enable_snat": False}
        network_name = data_utils.rand_name(name='tempest-net')
        subnet_name = data_utils.rand_name(name='tempest-subnet')
        router_state = self.create_topology_router(
            set_gateway=True,
            routers_client=self.cmgr_adm.routers_client, **kwargs)
        network_state = self.create_topology_network(
            network_name, networks_client=self.cmgr_adm.networks_client)
        self.create_topology_subnet(
            subnet_name, network_state, router_id=router_state["id"],
            routers_client=self.cmgr_adm.routers_client,
            subnets_client=self.cmgr_adm.subnets_client)
        time.sleep(constants.NSXP_BACKEND_SMALL_TIME_INTERVAL)
        result = self.check_router_components_on_edge(router_state)
        self.assertEqual(True, result[0]['dr_present'])
        self.assertEqual(False, result[1]['sr_present'])
        public_network_info = {"external_gateway_info": dict(
            network_id=CONF.network.public_network_id,
            enable_snat=True)}
        self.cmgr_adm.routers_client.update_router(router_state['id'],
                                                   **public_network_info)
        time.sleep(constants.NSXP_BACKEND_SMALL_TIME_INTERVAL)
        result = self.check_router_components_on_edge(router_state)
        self.assertEqual(True, result[0]['dr_present'])
        self.assertEqual(True, result[1]['sr_present'])
        security_group = self._create_security_group(
            security_group_rules_client=self.cmgr_adm.
            security_group_rules_client,
            security_groups_client=self.cmgr_adm.security_groups_client)
        image_id = self.get_glance_image_id(["cirros", "esx"])
        security_groups = [{'name': security_group['name']}]
        self.create_topology_instance(
            "state_vm_1", [network_state],
            create_floating_ip=True, image_id=image_id, clients=self.cmgr_adm,
            security_groups=security_groups)
        self.create_topology_instance(
            "state_vm_2", [network_state],
            create_floating_ip=True, image_id=image_id, clients=self.cmgr_adm,
            security_groups=security_groups)
        self.check_cross_network_connectivity(
            network_state,
            self.servers_details.get("state_vm_1").floating_ips[0],
            self.servers_details.get("state_vm_1").server, should_connect=True)
        self.check_cross_network_connectivity(
            network_state,
            self.servers_details.get("state_vm_2").floating_ips[0],
            self.servers_details.get("state_vm_2").server, should_connect=True)

    @decorators.idempotent_id('1206238b-02dd-1098-c228-10066ecc69e5')
    def test_tier1_sr_should_create_when_service_is_enabled(self):
        """
        Check sr and dr component of router should be present
        on edge when any service is enable
        """
        kwargs = {"enable_snat": False}
        router_state_1 = self.create_topology_router(set_gateway=True,
                                                     routers_client=self.
                                                     cmgr_adm.routers_client,
                                                     **kwargs)
        network_lbaas_1 = self.create_topology_network(
            "network_lbaas", networks_client=self.cmgr_adm.networks_client)
        sec_rule_client = self.cmgr_adm.security_group_rules_client
        sec_client = self.cmgr_adm.security_groups_client
        kwargs = dict(tenant_id=network_lbaas_1['tenant_id'],
                      security_group_rules_client=sec_rule_client,
                      security_groups_client=sec_client)
        self.sg = self.create_topology_security_group(**kwargs)
        lbaas_rules = [dict(direction='ingress', protocol='tcp',
                            port_range_min=constants.HTTP_PORT,
                            port_range_max=constants.HTTP_PORT, ),
                       dict(direction='ingress', protocol='tcp',
                            port_range_min=443, port_range_max=443, )]
        for rule in lbaas_rules:
            self.add_security_group_rule(
                self.sg,
                rule,
                ruleclient=sec_rule_client,
                secclient=sec_client,
                tenant_id=network_lbaas_1['tenant_id'])
        subnet_lbaas = self.create_topology_subnet(
            "subnet_lbaas", network_lbaas_1,
            subnets_client=self.cmgr_adm.subnets_client,
            routers_client=self.cmgr_adm.routers_client,
            router_id=router_state_1["id"])
        time.sleep(constants.NSX_NETWORK_REALISE_TIMEOUT)
        no_of_servers = 2
        image_id = self.get_glance_image_id(["cirros", "esx"])
        for instance in range(0, no_of_servers):
            self.create_topology_instance(
                "server_lbaas_%s" % instance, [network_lbaas_1],
                security_groups=[{'name': self.sg['name']}],
                image_id=image_id, create_floating_ip=False,
                clients=self.cmgr_adm)
        network = self.cmgr_adm.networks_client.show_network(
            CONF.network.public_network_id)['network']
        result = self.check_router_components_on_edge(router_state_1)
        self.assertEqual(True, result[0]['dr_present'])
        self.assertEqual(False, result[1]['sr_present'])
        for i in range(0, 2):
            lb_resource = self.create_project_lbaas(
                protocol_type="HTTP", protocol_port="80",
                vip_subnet_id=subnet_lbaas['id'],
                lb_algorithm="ROUND_ROBIN", hm_type='PING',
                create_fip=False, clean_up=True,
                external_subnet=network['subnets'][0])
            time.sleep(constants.NSXP_BACKEND_SMALL_TIME_INTERVAL)
            result = self.check_router_components_on_edge(router_state_1)
            self.assertEqual(True, result[0]['dr_present'])
            self.assertEqual(True, result[1]['sr_present'])
            self.delete_loadbalancer_resources(lb_resource['lb_id'],
                                               admin=True)
            time.sleep(constants.NSXP_BACKEND_SMALL_TIME_INTERVAL)
            result = self.check_router_components_on_edge(router_state_1)
            self.assertEqual(True, result[0]['dr_present'])
            self.assertEqual(False, result[1]['sr_present'])

    @decorators.idempotent_id('1206238b-02dd-1098-c228-10066ecc69e5')
    def test_tier1_sr_should_create_when_service_is_enabled_octavia(self):
        """
        Check sr and dr component of router should be present
        on edge when any service is enable
        """
        kwargs = {"enable_snat": False}
        router_state_1 = self.create_topology_router(set_gateway=True,
                                                     routers_client=self.
                                                     cmgr_adm.routers_client,
                                                     **kwargs)
        network_lbaas_1 = self.create_topology_network(
            "network_lbaas", networks_client=self.cmgr_adm.networks_client)
        sec_rule_client = self.cmgr_adm.security_group_rules_client
        sec_client = self.cmgr_adm.security_groups_client
        kwargs = dict(tenant_id=network_lbaas_1['tenant_id'],
                      security_group_rules_client=sec_rule_client,
                      security_groups_client=sec_client)
        self.sg = self.create_topology_security_group(**kwargs)
        lbaas_rules = [dict(direction='ingress', protocol='tcp',
                            port_range_min=constants.HTTP_PORT,
                            port_range_max=constants.HTTP_PORT, ),
                       dict(direction='ingress', protocol='tcp',
                            port_range_min=443, port_range_max=443, )]
        for rule in lbaas_rules:
            self.add_security_group_rule(
                self.sg,
                rule,
                ruleclient=sec_rule_client,
                secclient=sec_client,
                tenant_id=network_lbaas_1['tenant_id'])
        self.create_topology_subnet(
            "subnet_lbaas", network_lbaas_1,
            subnets_client=self.cmgr_adm.subnets_client,
            routers_client=self.cmgr_adm.routers_client,
            router_id=router_state_1["id"])
        time.sleep(constants.NSX_NETWORK_REALISE_TIMEOUT)
        no_of_servers = 2
        image_id = self.get_glance_image_id(["cirros", "esx"])
        for instance in range(0, no_of_servers):
            self.create_topology_instance(
                "server_lbaas_%s" % instance, [network_lbaas_1],
                security_groups=[{'name': self.sg['name']}],
                image_id=image_id, create_floating_ip=False,
                clients=self.cmgr_adm)
        result = self.check_router_components_on_edge(router_state_1)
        self.assertEqual(True, result[0]['dr_present'])
        self.assertEqual(False, result[1]['sr_present'])
        for i in range(0, 2):
            lb_dict = self.create_project_octavia(
                    protocol_type="HTTP", protocol_port="80",
                    lb_algorithm="LEAST_CONNECTIONS",
                    vip_net_id=network_lbaas_1['id'],
                    fip_disassociate=False, create_fip=False)
            time.sleep(constants.NSXP_BACKEND_SMALL_TIME_INTERVAL)
            result = self.check_router_components_on_edge(router_state_1)
            self.assertEqual(True, result[0]['dr_present'])
            self.assertEqual(True, result[1]['sr_present'])
            self.delete_octavia_lb_resources(lb_dict['lb_id'])
            time.sleep(constants.NSXP_BACKEND_SMALL_TIME_INTERVAL)
            result = self.check_router_components_on_edge(router_state_1)
            self.assertEqual(True, result[0]['dr_present'])
            self.assertEqual(False, result[1]['sr_present'])

    @decorators.idempotent_id('2317349c-13ee-2198-c228-10066ecc69e5')
    def test_tier1_sr_component_stail_entry_should_not_present_octavia(self):
        """
        Check sr component stail entry should not present on edge after
        multiple times enabling and disabling service on router.
        """
        kwargs = {"enable_snat": False}
        router_state_1 = self.create_topology_router(set_gateway=True,
                                                     routers_client=self.
                                                     cmgr_adm.routers_client,
                                                     **kwargs)
        network_lbaas_1 = self.create_topology_network(
            "network_lbaas", networks_client=self.cmgr_adm.networks_client)
        sec_rule_client = self.cmgr_adm.security_group_rules_client
        sec_client = self.cmgr_adm.security_groups_client
        kwargs = dict(tenant_id=network_lbaas_1['tenant_id'],
                      security_group_rules_client=sec_rule_client,
                      security_groups_client=sec_client)
        self.sg = self.create_topology_security_group(**kwargs)
        lbaas_rules = [dict(direction='ingress', protocol='tcp',
                            port_range_min=constants.HTTP_PORT,
                            port_range_max=constants.HTTP_PORT, ),
                       dict(direction='ingress', protocol='tcp',
                            port_range_min=443, port_range_max=443, )]
        for rule in lbaas_rules:
            self.add_security_group_rule(
                self.sg,
                rule,
                ruleclient=sec_rule_client,
                secclient=sec_client,
                tenant_id=network_lbaas_1['tenant_id'])
        self.create_topology_subnet(
            "subnet_lbaas", network_lbaas_1,
            subnets_client=self.cmgr_adm.subnets_client,
            routers_client=self.cmgr_adm.routers_client,
            router_id=router_state_1["id"])
        time.sleep(constants.NSX_NETWORK_REALISE_TIMEOUT)
        no_of_servers = 2
        image_id = self.get_glance_image_id(["cirros", "esx"])
        for instance in range(0, no_of_servers):
            self.create_topology_instance(
                "server_lbaas_%s" % instance, [network_lbaas_1],
                security_groups=[{'name': self.sg['name']}],
                image_id=image_id, create_floating_ip=False,
                clients=self.cmgr_adm)
        result = self.check_router_components_on_edge(router_state_1)
        self.assertEqual(True, result[0]['dr_present'])
        self.assertEqual(False, result[1]['sr_present'])
        for i in range(0, 20):
            lb_dict = self.create_project_octavia(
                    protocol_type="HTTP", protocol_port="80",
                    lb_algorithm="LEAST_CONNECTIONS",
                    vip_net_id=network_lbaas_1['id'],
                    fip_disassociate=False, create_fip=False)
            time.sleep(constants.NSXP_BACKEND_SMALL_TIME_INTERVAL)
            result = self.check_router_components_on_edge(router_state_1)
            self.assertEqual(True, result[0]['dr_present'])
            self.assertEqual(True, result[1]['sr_present'])
            self.delete_octavia_lb_resources(lb_dict['lb_id'])
            time.sleep(constants.NSXP_BACKEND_SMALL_TIME_INTERVAL)
            result = self.check_router_components_on_edge(router_state_1)
            self.assertEqual(True, result[0]['dr_present'])
            self.assertEqual(False, result[1]['sr_present'])

    @decorators.idempotent_id('2317349c-13ee-2198-c228-10066ecc69e5')
    def test_tier1_sr_component_stail_entry_should_not_present(self):
        """
        Check sr component stail entry should not present on edge after
        multiple times enabling and disabling service on router.
        """
        kwargs = {"enable_snat": False}
        network_name = data_utils.rand_name(name='tempest-net')
        subnet_name = data_utils.rand_name(name='tempest-subnet')
        router_state = self.create_topology_router(
            set_gateway=True,
            routers_client=self.cmgr_adm.routers_client, **kwargs)
        network_state = self.create_topology_network(
            network_name, networks_client=self.cmgr_adm.networks_client)
        self.create_topology_subnet(
            subnet_name, network_state, router_id=router_state["id"],
            routers_client=self.cmgr_adm.routers_client,
            subnets_client=self.cmgr_adm.subnets_client)
        time.sleep(constants.NSXP_BACKEND_SMALL_TIME_INTERVAL)
        result = self.check_router_components_on_edge(router_state)
        self.assertEqual(True, result[0]['dr_present'])
        self.assertEqual(False, result[1]['sr_present'])
        for i in range(1, 20):
            public_network_info = {"external_gateway_info": dict(
                network_id=CONF.network.public_network_id,
                enable_snat=True)}
            self.cmgr_adm.routers_client.update_router(router_state['id'],
                                                       **public_network_info)
            time.sleep(constants.NSXP_BACKEND_SMALL_TIME_INTERVAL)
            result = self.check_router_components_on_edge(router_state)
            self.assertEqual(True, result[0]['dr_present'])
            self.assertEqual(True, result[1]['sr_present'])
            public_network_info = {"external_gateway_info": dict(
                network_id=CONF.network.public_network_id,
                enable_snat=False)}
            self.cmgr_adm.routers_client.update_router(router_state['id'],
                                                       **public_network_info)
            time.sleep(constants.NSXP_BACKEND_SMALL_TIME_INTERVAL)
            result = self.check_router_components_on_edge(router_state)
            self.assertEqual(True, result[0]['dr_present'])
            self.assertEqual(False, result[1]['sr_present'])

    @decorators.idempotent_id('2317349c-13ee-3209-d339-10066ecc69e5')
    def test_sr_dr_componet_of_router_status_with_multiple_services(self):
        """
        Check sr-dr component of router status
        """
        kwargs = {"enable_snat": False}
        network_name = data_utils.rand_name(name='tempest-net')
        subnet_name = data_utils.rand_name(name='tempest-subnet')
        router_state = self.create_topology_router(
            set_gateway=True,
            routers_client=self.cmgr_adm.routers_client, **kwargs)
        network_state = self.create_topology_network(
            network_name, networks_client=self.cmgr_adm.networks_client)
        subnet_lbaas = self.create_topology_subnet(
            subnet_name, network_state, router_id=router_state["id"],
            routers_client=self.cmgr_adm.routers_client,
            subnets_client=self.cmgr_adm.subnets_client)
        time.sleep(constants.NSXP_BACKEND_SMALL_TIME_INTERVAL)
        result = self.check_router_components_on_edge(router_state)
        self.assertEqual(True, result[0]['dr_present'])
        self.assertEqual(False, result[1]['sr_present'])
        public_network_info = {"external_gateway_info": dict(
            network_id=CONF.network.public_network_id,
            enable_snat=True)}
        self.cmgr_adm.routers_client.update_router(router_state['id'],
                                                   **public_network_info)
        time.sleep(constants.NSXP_BACKEND_SMALL_TIME_INTERVAL)
        result = self.check_router_components_on_edge(router_state)
        self.assertEqual(True, result[0]['dr_present'])
        self.assertEqual(True, result[1]['sr_present'])
        sec_rule_client = self.cmgr_adm.security_group_rules_client
        sec_client = self.cmgr_adm.security_groups_client
        kwargs = dict(tenant_id=network_state['tenant_id'],
                      security_group_rules_client=sec_rule_client,
                      security_groups_client=sec_client)
        self.sg = self.create_topology_security_group(**kwargs)
        lbaas_rules = [dict(direction='ingress', protocol='tcp',
                            port_range_min=constants.HTTP_PORT,
                            port_range_max=constants.HTTP_PORT, ),
                       dict(direction='ingress', protocol='tcp',
                            port_range_min=443, port_range_max=443, )]
        for rule in lbaas_rules:
            self.add_security_group_rule(
                self.sg,
                rule,
                ruleclient=sec_rule_client,
                secclient=sec_client,
                tenant_id=network_state['tenant_id'])
        no_of_servers = 2
        image_id = self.get_glance_image_id(["cirros", "esx"])
        for instance in range(0, no_of_servers):
            self.create_topology_instance(
                "server_lbaas_%s" % instance, [network_state],
                security_groups=[{'name': self.sg['name']}],
                image_id=image_id, create_floating_ip=False,
                clients=self.cmgr_adm)
        network = self.cmgr_adm.networks_client.show_network(
            CONF.network.public_network_id)['network']
        lb_resource = self.create_project_lbaas(
            protocol_type="HTTP", protocol_port="80",
            vip_subnet_id=subnet_lbaas['id'],
            lb_algorithm="ROUND_ROBIN", hm_type='PING',
            create_fip=False, clean_up=True,
            external_subnet=network['subnets'][0])
        time.sleep(constants.NSXP_BACKEND_SMALL_TIME_INTERVAL)
        result = self.check_router_components_on_edge(router_state)
        self.assertEqual(True, result[0]['dr_present'])
        self.assertEqual(True, result[1]['sr_present'])
        self.delete_loadbalancer_resources(lb_resource['lb_id'], admin=True)
        time.sleep(constants.NSXP_BACKEND_SMALL_TIME_INTERVAL)
        result = self.check_router_components_on_edge(router_state)
        self.assertEqual(True, result[0]['dr_present'])
        self.assertEqual(True, result[1]['sr_present'])
        public_network_info = {"external_gateway_info": dict(
            network_id=CONF.network.public_network_id,
            enable_snat=False)}
        self.cmgr_adm.routers_client.update_router(router_state['id'],
                                                   **public_network_info)
        time.sleep(constants.NSXP_BACKEND_SMALL_TIME_INTERVAL)
        result = self.check_router_components_on_edge(router_state)
        self.assertEqual(True, result[0]['dr_present'])
        self.assertEqual(False, result[1]['sr_present'])

    @decorators.idempotent_id('2317349c-13ee-3209-d339-10066ecc69e5')
    def test_sr_dr_componet_of_router_with_multiple_services_octavia(self):
        """
        Check sr-dr component of router status
        """
        kwargs = {"enable_snat": False}
        network_name = data_utils.rand_name(name='tempest-net')
        subnet_name = data_utils.rand_name(name='tempest-subnet')
        router_state = self.create_topology_router(
            set_gateway=True,
            routers_client=self.cmgr_adm.routers_client, **kwargs)
        network_state = self.create_topology_network(
            network_name, networks_client=self.cmgr_adm.networks_client)
        subnet_state = self.create_topology_subnet(
            subnet_name, network_state,
            subnets_client=self.cmgr_adm.subnets_client)
        interface = self.cmgr_adm.routers_client.add_router_interface(
            router_state['id'], subnet_id=subnet_state["id"])
        self.addCleanup(test_utils.call_and_ignore_notfound_exc,
                        self.cmgr_adm.routers_client.remove_router_interface,
                        router_state['id'], subnet_id=subnet_state["id"])
        time.sleep(constants.NSXP_BACKEND_SMALL_TIME_INTERVAL)
        result = self.check_router_components_on_edge(router_state)
        self.assertEqual(True, result[0]['dr_present'])
        self.assertEqual(False, result[1]['sr_present'])
        ext_network = self.cmgr_adm.networks_client.show_network(
            CONF.network.public_network_id)['network']
        ext_subnet = self.cmgr_adm.subnets_client.show_subnet(
            ext_network['subnets'][0])['subnet']
        fw_rules = self.create_firewall_rule(
            name='test_rule', protocol='icmp',
            action="allow", destination_ip_address=ext_subnet["cidr"])
        rules = []
        # Check firewall rule
        rules.append(fw_rules['firewall_rule']['id'])
        policy_name = data_utils.rand_name('fw-policy-')
        # Create firewall policy
        fw_policy = self.create_firewall_policy(
            name=policy_name, firewall_rules=rules,
            project_id=router_state['project_id'])
        show_policy = self.show_firewall_policy(
            fw_policy['firewall_policy']['id'])
        # Check firewall policy
        self.assertEqual(
            show_policy.get('firewall_policy')['name'],
            policy_name)
        self.assertEqual(show_policy.get('firewall_policy')
                         ['firewall_rules'], rules)
        policy_id = fw_policy['firewall_policy']['id']
        group_name = data_utils.rand_name('fw-group-')
        # Create firewall group
        fw_group = self.create_firewall_group(
            name=group_name,
            ingress_firewall_policy_id=policy_id,
            egress_firewall_policy_id=policy_id,
            ports=[interface['port_id']],
            project_id=router_state['project_id'])
        self._wait_firewall_ready(fw_group["firewall_group"]["id"])
        time.sleep(constants.NSXP_BACKEND_SMALL_TIME_INTERVAL)
        result = self.check_router_components_on_edge(router_state)
        self.assertEqual(True, result[0]['dr_present'])
        self.assertEqual(True, result[1]['sr_present'])
        public_network_info = {"external_gateway_info": dict(
            network_id=CONF.network.public_network_id,
            enable_snat=True)}
        self.cmgr_adm.routers_client.update_router(router_state['id'],
                                                   **public_network_info)
        time.sleep(constants.NSXP_BACKEND_SMALL_TIME_INTERVAL)
        result = self.check_router_components_on_edge(router_state)
        self.assertEqual(True, result[0]['dr_present'])
        self.assertEqual(True, result[1]['sr_present'])
        sec_rule_client = self.cmgr_adm.security_group_rules_client
        sec_client = self.cmgr_adm.security_groups_client
        kwargs = dict(tenant_id=network_state['tenant_id'],
                      security_group_rules_client=sec_rule_client,
                      security_groups_client=sec_client)
        self.sg = self.create_topology_security_group(**kwargs)
        lbaas_rules = [dict(direction='ingress', protocol='tcp',
                            port_range_min=constants.HTTP_PORT,
                            port_range_max=constants.HTTP_PORT, ),
                       dict(direction='ingress', protocol='tcp',
                            port_range_min=443, port_range_max=443, )]
        for rule in lbaas_rules:
            self.add_security_group_rule(
                self.sg,
                rule,
                ruleclient=sec_rule_client,
                secclient=sec_client,
                tenant_id=network_state['tenant_id'])
        no_of_servers = 2
        image_id = self.get_glance_image_id(["cirros", "esx"])
        for instance in range(0, no_of_servers):
            self.create_topology_instance(
                "server_lbaas_%s" % instance, [network_state],
                security_groups=[{'name': self.sg['name']}],
                image_id=image_id, create_floating_ip=False,
                clients=self.cmgr_adm)
        lb_dict = self.create_project_octavia(
                    protocol_type="HTTP", protocol_port="80",
                    lb_algorithm="LEAST_CONNECTIONS",
                    vip_net_id=network_state['id'],
                    fip_disassociate=False, create_fip=False)
        time.sleep(constants.NSXP_BACKEND_SMALL_TIME_INTERVAL)
        result = self.check_router_components_on_edge(router_state)
        self.assertEqual(True, result[0]['dr_present'])
        self.assertEqual(True, result[1]['sr_present'])
        self.delete_octavia_lb_resources(lb_dict['lb_id'])
        time.sleep(constants.NSXP_BACKEND_SMALL_TIME_INTERVAL)
        result = self.check_router_components_on_edge(router_state)
        self.assertEqual(True, result[0]['dr_present'])
        self.assertEqual(True, result[1]['sr_present'])
        public_network_info = {"external_gateway_info": dict(
            network_id=CONF.network.public_network_id,
            enable_snat=False)}
        self.cmgr_adm.routers_client.update_router(router_state['id'],
                                                   **public_network_info)
        time.sleep(constants.NSXP_BACKEND_SMALL_TIME_INTERVAL)
        result = self.check_router_components_on_edge(router_state)
        self.assertEqual(True, result[0]['dr_present'])
        self.assertEqual(True, result[1]['sr_present'])
        self.fwaas_v2_client.update_firewall_v2_group(
            fw_group["firewall_group"]["id"], ports=[])
        self.fwaas_v2_client.delete_firewall_v2_group(
            fw_group["firewall_group"]["id"])
        self.fwaas_v2_client.delete_firewall_v2_policy(
            fw_policy['firewall_policy']['id'])
        self.fwaas_v2_client.delete_firewall_v2_rule(
            fw_rules['firewall_rule']['id'])
        time.sleep(constants.NSXP_BACKEND_SMALL_TIME_INTERVAL)
        result = self.check_router_components_on_edge(router_state)
        self.assertEqual(True, result[0]['dr_present'])
        self.assertEqual(False, result[1]['sr_present'])

    @decorators.idempotent_id('2317349c-24ff-4310-e440-21176ecc69e5')
    def test_sr_component_status_after_deleting_multiple_services_once(self):
        """
        Check only sr component should not present afetr
        deleting multiple services at once
        """
        kwargs = {"enable_snat": False}
        network_name = data_utils.rand_name(name='tempest-net')
        subnet_name = data_utils.rand_name(name='tempest-subnet')
        router_state = self.create_topology_router(
            set_gateway=True,
            routers_client=self.cmgr_adm.routers_client, **kwargs)
        network_state = self.create_topology_network(
            network_name, networks_client=self.cmgr_adm.networks_client)
        subnet_lbaas = self.create_topology_subnet(
            subnet_name, network_state, router_id=router_state["id"],
            routers_client=self.cmgr_adm.routers_client,
            subnets_client=self.cmgr_adm.subnets_client)
        time.sleep(constants.NSXP_BACKEND_SMALL_TIME_INTERVAL)
        result = self.check_router_components_on_edge(router_state)
        self.assertEqual(True, result[0]['dr_present'])
        self.assertEqual(False, result[1]['sr_present'])
        public_network_info = {"external_gateway_info": dict(
            network_id=CONF.network.public_network_id,
            enable_snat=True)}
        self.cmgr_adm.routers_client.update_router(router_state['id'],
                                                   **public_network_info)
        sec_rule_client = self.cmgr_adm.security_group_rules_client
        sec_client = self.cmgr_adm.security_groups_client
        kwargs = dict(tenant_id=network_state['tenant_id'],
                      security_group_rules_client=sec_rule_client,
                      security_groups_client=sec_client)
        self.sg = self.create_topology_security_group(**kwargs)
        lbaas_rules = [dict(direction='ingress', protocol='tcp',
                            port_range_min=constants.HTTP_PORT,
                            port_range_max=constants.HTTP_PORT, ),
                       dict(direction='ingress', protocol='tcp',
                            port_range_min=443, port_range_max=443, )]
        for rule in lbaas_rules:
            self.add_security_group_rule(
                self.sg,
                rule,
                ruleclient=sec_rule_client,
                secclient=sec_client,
                tenant_id=network_state['tenant_id'])
        no_of_servers = 2
        image_id = self.get_glance_image_id(["cirros", "esx"])
        for instance in range(0, no_of_servers):
            self.create_topology_instance(
                "server_lbaas_%s" % instance, [network_state],
                security_groups=[{'name': self.sg['name']}],
                image_id=image_id, create_floating_ip=False,
                clients=self.cmgr_adm)
        network = self.cmgr_adm.networks_client.show_network(
            CONF.network.public_network_id)['network']
        lb_resource = self.create_project_lbaas(
            protocol_type="HTTP", protocol_port="80",
            vip_subnet_id=subnet_lbaas['id'],
            lb_algorithm="ROUND_ROBIN", hm_type='PING',
            create_fip=False, clean_up=True,
            external_subnet=network['subnets'][0])
        self.delete_loadbalancer_resources(lb_resource['lb_id'], admin=True)
        public_network_info = {"external_gateway_info": dict(
            network_id=CONF.network.public_network_id,
            enable_snat=False)}
        self.cmgr_adm.routers_client.update_router(router_state['id'],
                                                   **public_network_info)
        time.sleep(constants.NSXP_BACKEND_SMALL_TIME_INTERVAL)
        result = self.check_router_components_on_edge(router_state)
        self.assertEqual(True, result[0]['dr_present'])
        self.assertEqual(False, result[1]['sr_present'])

    @decorators.idempotent_id('2317349c-13ee-3209-d339-10066ecc69e5')
    def test_sr_component_status_del_mul_services_once_octavia(self):
        """
        Check sr-dr component of router status
        """
        kwargs = {"enable_snat": False}
        network_name = data_utils.rand_name(name='tempest-net')
        subnet_name = data_utils.rand_name(name='tempest-subnet')
        router_state = self.create_topology_router(
            set_gateway=True,
            routers_client=self.cmgr_adm.routers_client, **kwargs)
        network_state = self.create_topology_network(
            network_name, networks_client=self.cmgr_adm.networks_client)
        subnet_state = self.create_topology_subnet(
            subnet_name, network_state,
            subnets_client=self.cmgr_adm.subnets_client)
        interface = self.cmgr_adm.routers_client.add_router_interface(
            router_state['id'], subnet_id=subnet_state["id"])
        self.addCleanup(test_utils.call_and_ignore_notfound_exc,
                        self.cmgr_adm.routers_client.remove_router_interface,
                        router_state['id'], subnet_id=subnet_state["id"])
        time.sleep(constants.NSXP_BACKEND_SMALL_TIME_INTERVAL)
        result = self.check_router_components_on_edge(router_state)
        self.assertEqual(True, result[0]['dr_present'])
        self.assertEqual(False, result[1]['sr_present'])
        ext_network = self.cmgr_adm.networks_client.show_network(
            CONF.network.public_network_id)['network']
        ext_subnet = self.cmgr_adm.subnets_client.show_subnet(
            ext_network['subnets'][0])['subnet']
        fw_rules = self.create_firewall_rule(
            name='test_rule', protocol='icmp',
            action="allow", destination_ip_address=ext_subnet["cidr"])
        rules = []
        # Check firewall rule
        rules.append(fw_rules['firewall_rule']['id'])
        policy_name = data_utils.rand_name('fw-policy-')
        # Create firewall policy
        fw_policy = self.create_firewall_policy(
            name=policy_name, firewall_rules=rules,
            project_id=router_state['project_id'])
        show_policy = self.show_firewall_policy(
            fw_policy['firewall_policy']['id'])
        # Check firewall policy
        self.assertEqual(
            show_policy.get('firewall_policy')['name'],
            policy_name)
        self.assertEqual(show_policy.get('firewall_policy')
                         ['firewall_rules'], rules)
        policy_id = fw_policy['firewall_policy']['id']
        group_name = data_utils.rand_name('fw-group-')
        # Create firewall group
        fw_group = self.create_firewall_group(
            name=group_name,
            ingress_firewall_policy_id=policy_id,
            egress_firewall_policy_id=policy_id,
            ports=[interface['port_id']],
            project_id=router_state['project_id'])
        self._wait_firewall_ready(fw_group["firewall_group"]["id"])
        public_network_info = {"external_gateway_info": dict(
            network_id=CONF.network.public_network_id,
            enable_snat=True)}
        self.cmgr_adm.routers_client.update_router(router_state['id'],
                                                   **public_network_info)
        sec_rule_client = self.cmgr_adm.security_group_rules_client
        sec_client = self.cmgr_adm.security_groups_client
        kwargs = dict(tenant_id=network_state['tenant_id'],
                      security_group_rules_client=sec_rule_client,
                      security_groups_client=sec_client)
        self.sg = self.create_topology_security_group(**kwargs)
        lbaas_rules = [dict(direction='ingress', protocol='tcp',
                            port_range_min=constants.HTTP_PORT,
                            port_range_max=constants.HTTP_PORT, ),
                       dict(direction='ingress', protocol='tcp',
                            port_range_min=443, port_range_max=443, )]
        for rule in lbaas_rules:
            self.add_security_group_rule(
                self.sg,
                rule,
                ruleclient=sec_rule_client,
                secclient=sec_client,
                tenant_id=network_state['tenant_id'])
        no_of_servers = 2
        image_id = self.get_glance_image_id(["cirros", "esx"])
        for instance in range(0, no_of_servers):
            self.create_topology_instance(
                "server_lbaas_%s" % instance, [network_state],
                security_groups=[{'name': self.sg['name']}],
                image_id=image_id, create_floating_ip=False,
                clients=self.cmgr_adm)
        lb_dict = self.create_project_octavia(
                    protocol_type="HTTP", protocol_port="80",
                    lb_algorithm="LEAST_CONNECTIONS",
                    vip_net_id=network_state['id'],
                    fip_disassociate=False, create_fip=False)
        time.sleep(constants.NSXP_BACKEND_SMALL_TIME_INTERVAL)
        result = self.check_router_components_on_edge(router_state)
        self.assertEqual(True, result[0]['dr_present'])
        self.assertEqual(True, result[1]['sr_present'])
        self.delete_octavia_lb_resources(lb_dict['lb_id'])
        public_network_info = {"external_gateway_info": dict(
            network_id=CONF.network.public_network_id,
            enable_snat=False)}
        self.cmgr_adm.routers_client.update_router(router_state['id'],
                                                   **public_network_info)
        self.fwaas_v2_client.update_firewall_v2_group(
            fw_group["firewall_group"]["id"], ports=[])
        self.fwaas_v2_client.delete_firewall_v2_group(
            fw_group["firewall_group"]["id"])
        self.fwaas_v2_client.delete_firewall_v2_policy(
            fw_policy['firewall_policy']['id'])
        self.fwaas_v2_client.delete_firewall_v2_rule(
            fw_rules['firewall_rule']['id'])
        time.sleep(constants.NSXP_BACKEND_SMALL_TIME_INTERVAL)
        result = self.check_router_components_on_edge(router_state)
        self.assertEqual(True, result[0]['dr_present'])
        self.assertEqual(False, result[1]['sr_present'])
