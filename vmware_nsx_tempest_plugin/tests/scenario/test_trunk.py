# Copyright 2017 VMware Inc
# All Rights Reserved
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

import re
import time


from tempest import config
from tempest.lib.common.utils import data_utils

from tempest.lib import decorators

from vmware_nsx_tempest_plugin.common import constants
from vmware_nsx_tempest_plugin.lib import feature_manager
from vmware_nsx_tempest_plugin.services import nsxv3_client
from vmware_nsx_tempest_plugin.services import nsxv_client
from vmware_nsx_tempest_plugin.services import trunk_client

CONF = config.CONF


class TestTrunkService(feature_manager.FeatureManager):

    """Test New Cases Scenario

    """
    @classmethod
    def setup_clients(cls):
        super(TestTrunkService, cls).setup_clients()
        cls.cmgr_adm = cls.get_client_manager('admin')
        cls.cmgr_alt = cls.get_client_manager('alt')
        cls.cmgr_adm = cls.get_client_manager('admin')
        cls.routers_client = cls.cmgr_adm.routers_client
        cls.networks_client = cls.cmgr_adm.networks_client
        cls.subnets_client = cls.cmgr_adm.subnets_client
        cls.sec_rule_client = cls.cmgr_adm.security_group_rules_client
        cls.sec_client = cls.cmgr_adm.security_groups_client
        cls.trunk_client = trunk_client.get_client(cls.cmgr_adm)

    @classmethod
    def resource_setup(cls):
        super(TestTrunkService, cls).resource_setup()
        if CONF.network.backend == "nsxv3" \
           or CONF.network.backend == "nsxp":
            cls.nsx = nsxv3_client.NSXV3Client(CONF.nsxv3.nsx_manager,
                                               CONF.nsxv3.nsx_user,
                                               CONF.nsxv3.nsx_password)
        elif CONF.network.backend == "nsxv":
            manager_ip = re.search(r"(\d{1,3}\.){3}\d{1,3}",
                                   CONF.nsxv.manager_uri).group(0)
            cls.vsm = nsxv_client.VSMClient(
                manager_ip, CONF.nsxv.user, CONF.nsxv.password)

        cls.namestart = 'lbaas-ops'
        cls.poke_counters = 12
        cls.hm_delay = 4
        cls.hm_max_retries = 3
        cls.hm_timeout = 10
        cls.server_names = []
        cls.loadbalancer = None
        cls.vip_fip = None
        cls.web_service_start_delay = 2.5

    @classmethod
    def create_trunks(cls, **kwargs):
        return cls.trunk_client.create_trunk(**kwargs)

    @classmethod
    def delete_trunks(cls, trunkportid):
        return cls.trunk_client.delete_trunk(trunkportid)

    @classmethod
    def add_subports(cls, trunkportid, **kwargs):
        return cls.trunk_client.add_subport(trunkportid, **kwargs)

    @classmethod
    def remove_subport(cls, trunkportid, **kwargs):
        return cls.trunk_client.remove_subport(trunkportid, **kwargs)

    def create_trunk_parent_port(self, provider, project_id,
                                 parent_port, trunkportname):
        kwargs = {}
        kwargs['trunk'] = dict(port_id=parent_port,
                               admin_state_up='true',
                               name=trunkportname)
        trunk_id = self.create_trunks(**kwargs)
        return trunk_id

    def delete_trunk_config(self, trunkportid):
        self.delete_trunks(trunkportid)

    def add_subport_to_parent(self, vlan_id, provider, project_id,
                              child_port, childportname,
                              trunkportid):
        kwargs = {}
        subport_dict = dict(segmentation_id=vlan_id,
                            port_id=child_port,
                            segmentation_type='vlan',
                            name=childportname)
        subport_detail = []
        subport_detail.append(subport_dict)
        kwargs['sub_ports'] = subport_detail
        child_port = self.add_subports(trunkportid, **kwargs)
        return child_port

    def remove_subports(self, provider, project_id, child_port, trunkportid):
        kwargs = {}
        subport_dict = dict(port_id=child_port)
        subport_detail = []
        subport_detail.append(subport_dict)
        kwargs['sub_ports'] = subport_detail
        delete_child_port = self.remove_subport(trunkportid, **kwargs)
        return delete_child_port

    def _assign_ip_address(self, ssh_src1, interface_name, ip_address):
        int_face = interface_name.split('.')[0]
        vlan_id = interface_name.split('.')[1]
        ssh_src1.exec_command("sudo ip link add link  %s name  %s type vlan\
                              id %s" % (int_face, interface_name, vlan_id))

        ssh_src1.exec_command("sudo ifconfig %s %s/24 \
                              up" % (interface_name, ip_address))

    def create_trunk_network_topo(self,
                                  create_instance=True,
                                  set_gateway=True):
        rtr_name = data_utils.rand_name(name='tempest-router')
        network_name1 = data_utils.rand_name(name='tempest-Parent')
        network_name2 = data_utils.rand_name(name='tempest-child1')
        network_name3 = data_utils.rand_name(name='tempest-child2')
        subnet_name1 = data_utils.rand_name(name='tempest-subnetParent')
        subnet_name2 = data_utils.rand_name(name='tempest-subneChild1')
        subnet_name3 = data_utils.rand_name(name='tempest-subneChild2')
        nw_client = self.cmgr_adm.networks_client
        rtr_client = self.cmgr_adm.routers_client
        router = self.create_topology_router(rtr_name,
                                             set_gateway=set_gateway,
                                             routers_client=rtr_client)
        networkParent = self.create_topology_network(network_name1,
                                                     networks_client=nw_client)
        networkChild1 = self.create_topology_network(network_name2,
                                                     networks_client=nw_client)
        networkChild2 = self.create_topology_network(network_name3,
                                                     networks_client=nw_client)
        self.create_topology_subnet(subnet_name1,
                                    networkParent,
                                    router_id=router["id"],
                                    cidr='10.198.1.0/24')
        self.create_topology_subnet(subnet_name2,
                                    networkChild1,
                                    router_id=router["id"],
                                    cidr='10.198.2.0/24')
        self.create_topology_subnet(subnet_name3,
                                    networkChild2,
                                    router_id=router["id"],
                                    cidr='10.198.3.0/24')
        port_client = self.cmgr_adm.ports_client

        kwargs = dict(tenant_id=networkParent['tenant_id'],
                      security_group_rules_client=self.sec_rule_client,
                      security_groups_client=self.sec_client)
        self.sg = self.create_topology_security_group(**kwargs)
        create_port_body = {}
        create_port_body['security_groups'] = [self.sg['id']]
        self.parent1_port = self.create_topology_port(network=networkParent,
                                                      ports_client=port_client,
                                                      **create_port_body)
        self.parent2_port = self.create_topology_port(network=networkParent,
                                                      ports_client=port_client,
                                                      **create_port_body)
        kwargs = {'mac_address': self.parent1_port['port']['mac_address'],
                  'security_groups': [self.sg['id']]}
        self.child1_p1 = self.create_topology_port(network=networkChild1,
                                                   ports_client=port_client,
                                                   **kwargs)
        self.child2_p1 = self.create_topology_port(network=networkChild2,
                                                   ports_client=port_client,
                                                   **kwargs)
        kwargs = {'mac_address': self.parent2_port['port']['mac_address'],
                  'security_groups': [self.sg['id']]}
        self.child1_p2 = self.create_topology_port(network=networkChild1,
                                                   ports_client=port_client,
                                                   **kwargs)
        self.child2_p2 = self.create_topology_port(network=networkChild2,
                                                   ports_client=port_client,
                                                   **kwargs)
        project_id = self.parent1_port['port']['tenant_id']
        networks = dict(project_id=project_id,
                        parent1_port=self.parent1_port,
                        parent2_port=self.parent2_port,
                        child1_p1=self.child1_p1,
                        child2_p1=self.child2_p1,
                        child1_p2=self.child1_p2,
                        child2_p2=self.child2_p2,
                        sg=self.sg)
        return networks

    def _test_trunk_child_subport_add(self, topology):
        project_id = topology['project_id']
        parent1_port = topology['parent1_port']
        parent2_port = topology['parent2_port']
        child1_p1 = topology['child1_p1']
        child2_p1 = topology['child2_p1']
        child1_p2 = topology['child1_p2']
        child2_p2 = topology['child2_p2']
        sg_name_dict = dict(name=topology['sg']['name'])
        parent1_id = parent1_port['port']['id']
        parent2_id = parent2_port['port']['id']
        trunk_parent1 = self.create_trunk_parent_port(provider=True,
                                                      project_id=project_id,
                                                      parent_port=parent1_id,
                                                      trunkportname='p11')
        print("parent1 Trunk port id:::::" + str(trunk_parent1))
        child1_id = child1_p1['port']['id']
        child2_id = child2_p1['port']['id']
        child1_trunk_p1 = self.add_subport_to_parent(provider=True,
                                                     vlan_id=101,
                                                     project_id=project_id,
                                                     child_port=child1_id,
                                                     childportname='child11',
                                                     trunkportid=trunk_parent1)
        print("Child trunk port is " + str(child1_trunk_p1))
        child2_trunk_p1 = self.add_subport_to_parent(provider=True,
                                                     vlan_id=102,
                                                     project_id=project_id,
                                                     child_port=child2_id,
                                                     childportname='child22',
                                                     trunkportid=trunk_parent1)
        print("Child trunk port is " + str(child2_trunk_p1))
        trunk_parent2 = self.create_trunk_parent_port(provider=True,
                                                      project_id=project_id,
                                                      parent_port=parent2_id,
                                                      trunkportname='p22')
        print("parent2 Trunk port id:::::" + str(trunk_parent2))
        child1_id = child1_p2['port']['id']
        child2_id = child2_p2['port']['id']
        child1_trunk_p2 = self.add_subport_to_parent(provider=True,
                                                     vlan_id=101,
                                                     project_id=project_id,
                                                     child_port=child1_id,
                                                     childportname='ch11_p2',
                                                     trunkportid=trunk_parent2)
        print("Child trunk port is " + str(child1_trunk_p2))
        child2_trunk_p2 = self.add_subport_to_parent(provider=True,
                                                     vlan_id=102,
                                                     project_id=project_id,
                                                     child_port=child2_id,
                                                     childportname='ch22_p2',
                                                     trunkportid=trunk_parent2)
        print("Child trunk port is " + str(child2_trunk_p2))
        child1_ip_vm1 = child1_p1['port']['fixed_ips'][0]['ip_address']
        child2_ip_vm1 = child2_p1['port']['fixed_ips'][0]['ip_address']
        child1_ip_vm2 = child1_p2['port']['fixed_ips'][0]['ip_address']
        child2_ip_vm2 = child2_p2['port']['fixed_ips'][0]['ip_address']
        server_name1_default = data_utils.rand_name('server1-default')
        server_name2_default = data_utils.rand_name('server2-default')
        image_id = self.get_glance_image_id([CONF.trunk.image_name])
        flavor_name = self.get_flavor_id([CONF.trunk.flavor_id])
        keypair = self.create_keypair(self.cmgr_adm.keypairs_client)
        server1 = self.create_topology_instance(server_name1_default,
                                                create_floating_ip=True,
                                                image_id=image_id,
                                                port=parent1_port['port'],
                                                security_groups=[sg_name_dict],
                                                clients=self.cmgr_adm,
                                                keypair=keypair,
                                                flavor=flavor_name)
        server2 = self.create_topology_instance(server_name2_default,
                                                create_floating_ip=True,
                                                image_id=image_id,
                                                port=parent2_port['port'],
                                                security_groups=[sg_name_dict],
                                                clients=self.cmgr_adm,
                                                keypair=keypair,
                                                flavor=flavor_name)
        ip_address = server1['floating_ips'][0]['floating_ip_address']
        ssh_src1 = self._get_remote_client(ip_address, username='ubuntu',
                                           use_password=False,
                                           private_key=keypair['private_key'])
        if 'dns_assignment' in parent2_port['port'].keys():
            remote_ip = \
                parent2_port['port']['dns_assignment'][0]['ip_address']
        else:
            remote_ip = \
                parent2_port['port']['fixed_ips'][0]['ip_address']
        # Verify connectivity between vms
        self._assign_ip_address(ssh_src1, 'eth0.101', child1_ip_vm1)
        self._assign_ip_address(ssh_src1, 'eth0.102', child2_ip_vm1)
        ip_address = server2['floating_ips'][0]['floating_ip_address']
        ssh_src2 = self._get_remote_client(ip_address, username='ubuntu',
                                           use_password=False,
                                           private_key=keypair['private_key'])
        # Verify connectivity between vms
        self._assign_ip_address(ssh_src2, 'eth0.101', child1_ip_vm2)
        self._assign_ip_address(ssh_src2, 'eth0.102', child2_ip_vm2)
        self.check_remote_connectivity(ssh_src1, remote_ip,
                                       should_succeed=True)
        self.check_remote_connectivity(ssh_src1, child2_ip_vm2,
                                       should_succeed=True)
        # Remove child subport
        print('Removinfg child subport')
        self.remove_subports(provider=True,
                             project_id=project_id,
                             child_port=child1_p1['port']['id'],
                             trunkportid=trunk_parent1)
        self.remove_subports(provider=True,
                             project_id=project_id,
                             child_port=child2_p1['port']['id'],
                             trunkportid=trunk_parent1)
        self.remove_subports(provider=True,
                             project_id=project_id,
                             child_port=child1_p2['port']['id'],
                             trunkportid=trunk_parent2)
        self.remove_subports(provider=True,
                             project_id=project_id,
                             child_port=child2_p2['port']['id'],
                             trunkportid=trunk_parent2)
        self.delete_trunk_config(trunk_parent1)
        self.delete_trunk_config(trunk_parent2)

    def _test_trunk_child_subport_add_delete(self, topology):
        project_id = topology['project_id']
        parent1_port = topology['parent1_port']
        parent2_port = topology['parent2_port']
        child1_p1 = topology['child1_p1']
        child2_p1 = topology['child2_p1']
        child1_p2 = topology['child1_p2']
        child2_p2 = topology['child2_p2']
        sg_name_dict = dict(name=topology['sg']['name'])
        parent1_id = parent1_port['port']['id']
        parent2_id = parent2_port['port']['id']
        trunk_parent1 = self.create_trunk_parent_port(provider=True,
                                                      project_id=project_id,
                                                      parent_port=parent1_id,
                                                      trunkportname='p11')
        print("parent1 Trunk port id:::::" + str(trunk_parent1))
        child1_id = child1_p1['port']['id']
        child2_id = child2_p1['port']['id']
        child1_trunk_p1 = self.add_subport_to_parent(provider=True,
                                                     vlan_id=101,
                                                     project_id=project_id,
                                                     child_port=child1_id,
                                                     childportname='child11',
                                                     trunkportid=trunk_parent1)
        print("Child trunk port is " + str(child1_trunk_p1))
        child2_trunk_p1 = self.add_subport_to_parent(provider=True,
                                                     vlan_id=102,
                                                     project_id=project_id,
                                                     child_port=child2_id,
                                                     childportname='child22',
                                                     trunkportid=trunk_parent1)
        print("Child trunk port is " + str(child2_trunk_p1))
        trunk_parent2 = self.create_trunk_parent_port(provider=True,
                                                      project_id=project_id,
                                                      parent_port=parent2_id,
                                                      trunkportname='p22')
        print("parent2 Trunk port id:::::" + str(trunk_parent2))
        child1_id = child1_p2['port']['id']
        child2_id = child2_p2['port']['id']
        child1_trunk_p2 = self.add_subport_to_parent(provider=True,
                                                     vlan_id=101,
                                                     project_id=project_id,
                                                     child_port=child1_id,
                                                     childportname='ch11_p2',
                                                     trunkportid=trunk_parent2)
        print("Child trunk port is " + str(child1_trunk_p2))
        child2_trunk_p2 = self.add_subport_to_parent(provider=True,
                                                     vlan_id=102,
                                                     project_id=project_id,
                                                     child_port=child2_id,
                                                     childportname='ch22_p2',
                                                     trunkportid=trunk_parent2)
        print("Child trunk port is " + str(child2_trunk_p2))
        child1_ip_vm1 = child1_p1['port']['fixed_ips'][0]['ip_address']
        child2_ip_vm1 = child2_p1['port']['fixed_ips'][0]['ip_address']
        child1_ip_vm2 = child1_p2['port']['fixed_ips'][0]['ip_address']
        child2_ip_vm2 = child2_p2['port']['fixed_ips'][0]['ip_address']
        server_name1_default = data_utils.rand_name('server1-default')
        server_name2_default = data_utils.rand_name('server2-default')
        image_id = self.get_glance_image_id([CONF.trunk.image_name])
        flavor_name = self.get_flavor_id([CONF.trunk.flavor_id])
        keypair = self.create_keypair(self.cmgr_adm.keypairs_client)
        server1 = self.create_topology_instance(server_name1_default,
                                                create_floating_ip=True,
                                                image_id=image_id,
                                                port=parent1_port['port'],
                                                security_groups=[sg_name_dict],
                                                clients=self.cmgr_adm,
                                                keypair=keypair,
                                                flavor=flavor_name)
        server2 = self.create_topology_instance(server_name2_default,
                                                create_floating_ip=True,
                                                image_id=image_id,
                                                port=parent2_port['port'],
                                                security_groups=[sg_name_dict],
                                                clients=self.cmgr_adm,
                                                keypair=keypair,
                                                flavor=flavor_name)
        ip_address = server1['floating_ips'][0]['floating_ip_address']
        ssh_src1 = self._get_remote_client(ip_address, username='ubuntu',
                                           use_password=False,
                                           private_key=keypair['private_key'])
        if 'dns_assignment' in parent2_port['port'].keys():
            remote_ip = \
                parent2_port['port']['dns_assignment'][0]['ip_address']
        else:
            remote_ip = \
                parent2_port['port']['fixed_ips'][0]['ip_address']
        # Verify connectivity between vms
        self._assign_ip_address(ssh_src1, 'eth0.101', child1_ip_vm1)
        self._assign_ip_address(ssh_src1, 'eth0.102', child2_ip_vm1)
        ip_address = server2['floating_ips'][0]['floating_ip_address']
        ssh_src2 = self._get_remote_client(ip_address, username='ubuntu',
                                           use_password=False,
                                           private_key=keypair['private_key'])
        # Verify connectivity between vms
        self._assign_ip_address(ssh_src2, 'eth0.101', child1_ip_vm2)
        self._assign_ip_address(ssh_src2, 'eth0.102', child2_ip_vm2)
        self.check_remote_connectivity(ssh_src1, remote_ip,
                                       should_succeed=True)
        self.check_remote_connectivity(ssh_src1, child2_ip_vm2,
                                       should_succeed=True)
        # Remove subport & check connectivity should fail #
        child2_trunk_p2 = self.remove_subports(provider=True,
                                               project_id=project_id,
                                               child_port=child2_id,
                                               trunkportid=trunk_parent2)
        self.check_remote_connectivity(ssh_src1, child2_ip_vm2,
                                       should_succeed=False)
        child2_trunk_p2 = self.add_subport_to_parent(provider=True,
                                                     vlan_id=102,
                                                     project_id=project_id,
                                                     child_port=child2_id,
                                                     childportname='ch22_p2',
                                                     trunkportid=trunk_parent2)
        ip_address = server2['floating_ips'][0]['floating_ip_address']
        ssh_src2 = self._get_remote_client(ip_address, username='ubuntu',
                                           use_password=False,
                                           private_key=keypair['private_key'])
        # Verify connectivity between vms
        self.check_remote_connectivity(ssh_src1, child2_ip_vm2,
                                       should_succeed=True)
        # Remove child subport
        print('Removinfg child subport')
        self.remove_subports(provider=True,
                             project_id=project_id,
                             child_port=child1_p1['port']['id'],
                             trunkportid=trunk_parent1)
        self.remove_subports(provider=True,
                             project_id=project_id,
                             child_port=child2_p1['port']['id'],
                             trunkportid=trunk_parent1)
        self.remove_subports(provider=True,
                             project_id=project_id,
                             child_port=child1_p2['port']['id'],
                             trunkportid=trunk_parent2)
        self.remove_subports(provider=True,
                             project_id=project_id,
                             child_port=child2_p2['port']['id'],
                             trunkportid=trunk_parent2)
        self.delete_trunk_config(trunk_parent1)
        self.delete_trunk_config(trunk_parent2)

    def _test_trunk_instance_destroy_deploy(self, topology):
        project_id = topology['project_id']
        parent1_port = topology['parent1_port']
        parent2_port = topology['parent2_port']
        child1_p1 = topology['child1_p1']
        child2_p1 = topology['child2_p1']
        child1_p2 = topology['child1_p2']
        child2_p2 = topology['child2_p2']
        sg_name_dict = dict(name=topology['sg']['name'])
        parent1_id = parent1_port['port']['id']
        parent2_id = parent2_port['port']['id']
        trunk_parent1 = self.create_trunk_parent_port(provider=True,
                                                      project_id=project_id,
                                                      parent_port=parent1_id,
                                                      trunkportname='p11')
        print("parent1 Trunk port id:::::" + str(trunk_parent1))
        child1_id = child1_p1['port']['id']
        child2_id = child2_p1['port']['id']
        child1_trunk_p1 = self.add_subport_to_parent(provider=True,
                                                     vlan_id=101,
                                                     project_id=project_id,
                                                     child_port=child1_id,
                                                     childportname='child11',
                                                     trunkportid=trunk_parent1)
        print("Child trunk port is " + str(child1_trunk_p1))
        child2_trunk_p1 = self.add_subport_to_parent(provider=True,
                                                     vlan_id=102,
                                                     project_id=project_id,
                                                     child_port=child2_id,
                                                     childportname='child22',
                                                     trunkportid=trunk_parent1)
        print("Child trunk port is " + str(child2_trunk_p1))
        trunk_parent2 = self.create_trunk_parent_port(provider=True,
                                                      project_id=project_id,
                                                      parent_port=parent2_id,
                                                      trunkportname='p22')
        print("parent2 Trunk port id:::::" + str(trunk_parent2))
        child1_id = child1_p2['port']['id']
        child2_id = child2_p2['port']['id']
        child1_trunk_p2 = self.add_subport_to_parent(provider=True,
                                                     vlan_id=101,
                                                     project_id=project_id,
                                                     child_port=child1_id,
                                                     childportname='ch11_p2',
                                                     trunkportid=trunk_parent2)
        print("Child trunk port is " + str(child1_trunk_p2))
        child2_trunk_p2 = self.add_subport_to_parent(provider=True,
                                                     vlan_id=102,
                                                     project_id=project_id,
                                                     child_port=child2_id,
                                                     childportname='ch22_p2',
                                                     trunkportid=trunk_parent2)
        print("Child trunk port is " + str(child2_trunk_p2))
        child1_ip_vm1 = child1_p1['port']['fixed_ips'][0]['ip_address']
        child2_ip_vm1 = child2_p1['port']['fixed_ips'][0]['ip_address']
        child1_ip_vm2 = child1_p2['port']['fixed_ips'][0]['ip_address']
        child2_ip_vm2 = child2_p2['port']['fixed_ips'][0]['ip_address']
        server_name1_default = data_utils.rand_name('server1-default')
        server_name2_default = data_utils.rand_name('server2-default')
        image_id = self.get_glance_image_id([CONF.trunk.image_name])
        flavor_name = self.get_flavor_id([CONF.trunk.flavor_id])
        keypair = self.create_keypair(self.cmgr_adm.keypairs_client)
        server1 = self.create_topology_instance(server_name1_default,
                                                create_floating_ip=True,
                                                image_id=image_id,
                                                port=parent1_port['port'],
                                                security_groups=[sg_name_dict],
                                                clients=self.cmgr_adm,
                                                keypair=keypair,
                                                flavor=flavor_name)
        server2 = self.create_topology_instance(server_name2_default,
                                                create_floating_ip=True,
                                                image_id=image_id,
                                                port=parent2_port['port'],
                                                security_groups=[sg_name_dict],
                                                clients=self.cmgr_adm,
                                                keypair=keypair,
                                                flavor=flavor_name)
        ip_address = server1['floating_ips'][0]['floating_ip_address']
        ssh_src1 = self._get_remote_client(ip_address, username='ubuntu',
                                           use_password=False,
                                           private_key=keypair['private_key'])
        if 'dns_assignment' in parent2_port['port'].keys():
            remote_ip = \
                parent2_port['port']['dns_assignment'][0]['ip_address']
        else:
            remote_ip = \
                parent2_port['port']['fixed_ips'][0]['ip_address']
        # Verify connectivity between vms
        self._assign_ip_address(ssh_src1, 'eth0.101', child1_ip_vm1)
        self._assign_ip_address(ssh_src1, 'eth0.102', child2_ip_vm1)
        ip_address = server2['floating_ips'][0]['floating_ip_address']
        ssh_src2 = self._get_remote_client(ip_address, username='ubuntu',
                                           use_password=False,
                                           private_key=keypair['private_key'])
        self._assign_ip_address(ssh_src2, 'eth0.101', child1_ip_vm2)
        self._assign_ip_address(ssh_src2, 'eth0.102', child2_ip_vm2)
        self.check_remote_connectivity(ssh_src1, remote_ip,
                                       should_succeed=True)
        self.os_admin.servers_client.delete_server(server2['id'])
        self.check_remote_connectivity(ssh_src1, remote_ip,
                                       should_succeed=False)
        time.sleep(constants.NSX_BACKEND_SMALL_TIME_INTERVAL)
        server2 = self.create_topology_instance(server_name2_default,
                                                create_floating_ip=False,
                                                image_id=image_id,
                                                port=parent2_port['port'],
                                                security_groups=[sg_name_dict],
                                                clients=self.cmgr_adm,
                                                keypair=keypair,
                                                flavor=flavor_name)
        self.check_remote_connectivity(ssh_src1, remote_ip,
                                       should_succeed=True)
        # Remove child subport
        print('Removinfg child subport')
        self.remove_subports(provider=True,
                             project_id=project_id,
                             child_port=child1_p1['port']['id'],
                             trunkportid=trunk_parent1)
        self.remove_subports(provider=True,
                             project_id=project_id,
                             child_port=child2_p1['port']['id'],
                             trunkportid=trunk_parent1)
        self.remove_subports(provider=True,
                             project_id=project_id,
                             child_port=child1_p2['port']['id'],
                             trunkportid=trunk_parent2)
        self.remove_subports(provider=True,
                             project_id=project_id,
                             child_port=child2_p2['port']['id'],
                             trunkportid=trunk_parent2)
        self.delete_trunk_config(trunk_parent1)
        self.delete_trunk_config(trunk_parent2)

    def _test_trunk_between_two_vms(self, topology):
        project_id = topology['project_id']
        parent1_port = topology['parent1_port']
        parent2_port = topology['parent2_port']
        child1_p1 = topology['child1_p1']
        child2_p1 = topology['child2_p1']
        child1_p2 = topology['child1_p2']
        child2_p2 = topology['child2_p2']
        sg_name_dict = dict(name=topology['sg']['name'])
        parent1_id = parent1_port['port']['id']
        parent2_id = parent2_port['port']['id']
        trunk_parent1 = self.create_trunk_parent_port(provider=True,
                                                      project_id=project_id,
                                                      parent_port=parent1_id,
                                                      trunkportname='p11')
        print("parent1 Trunk port id:::::" + str(trunk_parent1))
        child1_id = child1_p1['port']['id']
        child2_id = child2_p1['port']['id']
        child1_trunk_p1 = self.add_subport_to_parent(provider=True,
                                                     vlan_id=101,
                                                     project_id=project_id,
                                                     child_port=child1_id,
                                                     childportname='child11',
                                                     trunkportid=trunk_parent1)
        print("Child trunk port is " + str(child1_trunk_p1))
        child2_trunk_p1 = self.add_subport_to_parent(provider=True,
                                                     vlan_id=102,
                                                     project_id=project_id,
                                                     child_port=child2_id,
                                                     childportname='child22',
                                                     trunkportid=trunk_parent1)
        print("Child trunk port is " + str(child2_trunk_p1))
        trunk_parent2 = self.create_trunk_parent_port(provider=True,
                                                      project_id=project_id,
                                                      parent_port=parent2_id,
                                                      trunkportname='p22')
        print("parent2 Trunk port id:::::" + str(trunk_parent2))
        child1_id = child1_p2['port']['id']
        child2_id = child2_p2['port']['id']
        child1_trunk_p2 = self.add_subport_to_parent(provider=True,
                                                     vlan_id=101,
                                                     project_id=project_id,
                                                     child_port=child1_id,
                                                     childportname='ch11_p2',
                                                     trunkportid=trunk_parent2)
        print("Child trunk port is " + str(child1_trunk_p2))
        child2_trunk_p2 = self.add_subport_to_parent(provider=True,
                                                     vlan_id=102,
                                                     project_id=project_id,
                                                     child_port=child2_id,
                                                     childportname='ch22_p2',
                                                     trunkportid=trunk_parent2)
        print("Child trunk port is " + str(child2_trunk_p2))
        child1_ip_vm1 = child1_p1['port']['fixed_ips'][0]['ip_address']
        child2_ip_vm1 = child2_p1['port']['fixed_ips'][0]['ip_address']
        server_name1_default = data_utils.rand_name('server1-default')
        server_name2_default = data_utils.rand_name('server2-default')
        image_id = self.get_glance_image_id([CONF.trunk.image_name])
        flavor_name = self.get_flavor_id([CONF.trunk.flavor_id])
        keypair = self.create_keypair(self.cmgr_adm.keypairs_client)
        server1 = self.create_topology_instance(server_name1_default,
                                                create_floating_ip=True,
                                                image_id=image_id,
                                                port=parent1_port['port'],
                                                security_groups=[sg_name_dict],
                                                clients=self.cmgr_adm,
                                                keypair=keypair,
                                                flavor=flavor_name)
        server2 = self.create_topology_instance(server_name2_default,
                                                create_floating_ip=True,
                                                image_id=image_id,
                                                port=parent2_port['port'],
                                                security_groups=[sg_name_dict],
                                                clients=self.cmgr_adm,
                                                keypair=keypair,
                                                flavor=flavor_name)
        ip_address = server1['floating_ips'][0]['floating_ip_address']
        ssh_src1 = self._get_remote_client(ip_address, username='ubuntu',
                                           use_password=False,
                                           private_key=keypair['private_key'])
        if 'dns_assignment' in parent2_port['port'].keys():
            remote_ip = \
                parent2_port['port']['dns_assignment'][0]['ip_address']
        else:
            remote_ip = \
                parent2_port['port']['fixed_ips'][0]['ip_address']
        # Verify connectivity between vms
        self._assign_ip_address(ssh_src1, 'eth0.101', child1_ip_vm1)
        self._assign_ip_address(ssh_src1, 'eth0.102', child2_ip_vm1)
        ip_address = server2['floating_ips'][0]['floating_ip_address']
        ssh_src2 = self._get_remote_client(ip_address, username='ubuntu',
                                           use_password=False,
                                           private_key=keypair['private_key'])
        # Verify connectivity between vms
        child1_ip_vm2 = child1_p2['port']['fixed_ips'][0]['ip_address']
        child2_ip_vm2 = child2_p2['port']['fixed_ips'][0]['ip_address']
        self._assign_ip_address(ssh_src2, 'eth0.101', child1_ip_vm2)
        self._assign_ip_address(ssh_src2, 'eth0.102', child2_ip_vm2)
        self.check_remote_connectivity(ssh_src1, remote_ip,
                                       should_succeed=True)
        self.check_remote_connectivity(ssh_src1, child1_ip_vm2,
                                       should_succeed=True)
        self.check_remote_connectivity(ssh_src1, child2_ip_vm2,
                                       should_succeed=True)
        # Remove child subport
        print('Removinfg child subport')
        self.remove_subports(provider=True,
                             project_id=project_id,
                             child_port=child1_p1['port']['id'],
                             trunkportid=trunk_parent1)
        self.remove_subports(provider=True,
                             project_id=project_id,
                             child_port=child2_p1['port']['id'],
                             trunkportid=trunk_parent1)
        self.remove_subports(provider=True,
                             project_id=project_id,
                             child_port=child1_p2['port']['id'],
                             trunkportid=trunk_parent2)
        self.remove_subports(provider=True,
                             project_id=project_id,
                             child_port=child2_p2['port']['id'],
                             trunkportid=trunk_parent2)
        self.delete_trunk_config(trunk_parent1)
        self.delete_trunk_config(trunk_parent2)

    def _test_trunk_instance_vm_vlan_diff_subnet(self, topology):
        project_id = topology['project_id']
        parent1_port = topology['parent1_port']
        parent2_port = topology['parent2_port']
        child1_p1 = topology['child1_p1']
        child2_p1 = topology['child2_p1']
        child1_p2 = topology['child1_p2']
        child2_p2 = topology['child2_p2']
        sg_name_dict = dict(name=topology['sg']['name'])
        parent1_id = parent1_port['port']['id']
        parent2_id = parent2_port['port']['id']
        trunk_parent1 = self.create_trunk_parent_port(provider=True,
                                                      project_id=project_id,
                                                      parent_port=parent1_id,
                                                      trunkportname='p11')
        print("parent1 Trunk port id:::::" + str(trunk_parent1))
        child1_id = child1_p1['port']['id']
        child2_id = child2_p1['port']['id']
        child1_trunk_p1 = self.add_subport_to_parent(provider=True,
                                                     vlan_id=101,
                                                     project_id=project_id,
                                                     child_port=child1_id,
                                                     childportname='child11',
                                                     trunkportid=trunk_parent1)
        print("Child trunk port is " + str(child1_trunk_p1))
        child2_trunk_p1 = self.add_subport_to_parent(provider=True,
                                                     vlan_id=102,
                                                     project_id=project_id,
                                                     child_port=child2_id,
                                                     childportname='child22',
                                                     trunkportid=trunk_parent1)
        print("Child trunk port is " + str(child2_trunk_p1))
        trunk_parent2 = self.create_trunk_parent_port(provider=True,
                                                      project_id=project_id,
                                                      parent_port=parent2_id,
                                                      trunkportname='p22')
        print("parent2 Trunk port id:::::" + str(trunk_parent2))
        child1_id = child1_p2['port']['id']
        child2_id = child2_p2['port']['id']
        child1_trunk_p2 = self.add_subport_to_parent(provider=True,
                                                     vlan_id=101,
                                                     project_id=project_id,
                                                     child_port=child1_id,
                                                     childportname='ch11_p2',
                                                     trunkportid=trunk_parent2)
        print("Child trunk port is " + str(child1_trunk_p2))
        child2_trunk_p2 = self.add_subport_to_parent(provider=True,
                                                     vlan_id=102,
                                                     project_id=project_id,
                                                     child_port=child2_id,
                                                     childportname='ch22_p2',
                                                     trunkportid=trunk_parent2)
        print("Child trunk port is " + str(child2_trunk_p2))
        child1_ip_vm1 = child1_p1['port']['fixed_ips'][0]['ip_address']
        child2_ip_vm1 = child2_p1['port']['fixed_ips'][0]['ip_address']
        server_name1_default = data_utils.rand_name('server1-default')
        server_name2_default = data_utils.rand_name('server2-default')
        image_id = self.get_glance_image_id([CONF.trunk.image_name])
        flavor_name = self.get_flavor_id([CONF.trunk.flavor_id])
        keypair = self.create_keypair(self.cmgr_adm.keypairs_client)
        server1 = self.create_topology_instance(server_name1_default,
                                                create_floating_ip=True,
                                                image_id=image_id,
                                                port=parent1_port['port'],
                                                security_groups=[sg_name_dict],
                                                clients=self.cmgr_adm,
                                                keypair=keypair,
                                                flavor=flavor_name)
        server2 = self.create_topology_instance(server_name2_default,
                                                create_floating_ip=True,
                                                image_id=image_id,
                                                port=parent2_port['port'],
                                                security_groups=[sg_name_dict],
                                                clients=self.cmgr_adm,
                                                keypair=keypair,
                                                flavor=flavor_name)
        ip_address = server1['floating_ips'][0]['floating_ip_address']
        ssh_src1 = self._get_remote_client(ip_address, username='ubuntu',
                                           use_password=False,
                                           private_key=keypair['private_key'])
        if 'dns_assignment' in parent2_port['port'].keys():
            remote_ip = \
                parent2_port['port']['dns_assignment'][0]['ip_address']
        else:
            remote_ip = \
                parent2_port['port']['fixed_ips'][0]['ip_address']
        # Verify connectivity between vms
        self._assign_ip_address(ssh_src1, 'eth0.201', child1_ip_vm1)
        self._assign_ip_address(ssh_src1, 'eth0.202', child2_ip_vm1)
        ip_address = server2['floating_ips'][0]['floating_ip_address']
        ssh_src2 = self._get_remote_client(ip_address, username='ubuntu',
                                           use_password=False,
                                           private_key=keypair['private_key'])
        # Verify connectivity between vms
        child1_ip_vm2 = child1_p2['port']['fixed_ips'][0]['ip_address']
        child2_ip_vm2 = child2_p2['port']['fixed_ips'][0]['ip_address']
        self._assign_ip_address(ssh_src2, 'eth0.101', child1_ip_vm2)
        self._assign_ip_address(ssh_src2, 'eth0.102', child2_ip_vm2)
        self.check_remote_connectivity(ssh_src1, remote_ip,
                                       should_succeed=True)
        self.check_remote_connectivity(ssh_src1, child1_ip_vm2,
                                       should_succeed=False)
        self.check_remote_connectivity(ssh_src1, child2_ip_vm2,
                                       should_succeed=False)
        # Remove child subport
        print('Removinfg child subport')
        self.remove_subports(provider=True,
                             project_id=project_id,
                             child_port=child1_p1['port']['id'],
                             trunkportid=trunk_parent1)
        self.remove_subports(provider=True,
                             project_id=project_id,
                             child_port=child2_p1['port']['id'],
                             trunkportid=trunk_parent1)
        self.remove_subports(provider=True,
                             project_id=project_id,
                             child_port=child1_p2['port']['id'],
                             trunkportid=trunk_parent2)
        self.remove_subports(provider=True,
                             project_id=project_id,
                             child_port=child2_p2['port']['id'],
                             trunkportid=trunk_parent2)
        self.delete_trunk_config(trunk_parent1)
        self.delete_trunk_config(trunk_parent2)

    @decorators.idempotent_id('9d4192e9-b1b7-48c9-af04-67a82637c359')
    def test_trunk_between_two_vms(self):
        self.network_topo = self.create_trunk_network_topo(False, True)
        self._test_trunk_between_two_vms(self.network_topo)

    @decorators.idempotent_id('8d4192e9-b1b7-48c9-af04-68a82637c359')
    def test_trunk_child_subport_add(self):
        self.network_topo = self.create_trunk_network_topo(False, True)
        self._test_trunk_child_subport_add(self.network_topo)

    @decorators.idempotent_id('8d4192e9-b1b7-48c9-af04-68a82637c359')
    def test_trunk_child_subport_add_delete(self):
        self.network_topo = self.create_trunk_network_topo(False, True)
        self._test_trunk_child_subport_add_delete(self.network_topo)

    @decorators.idempotent_id('7e4192e9-b1b7-48c9-af04-68a82637c359')
    def test_trunk_destroy_deploy_with_existing_parent_port(self):
        self.network_topo = self.create_trunk_network_topo(False, True)
        self._test_trunk_instance_destroy_deploy(self.network_topo)

    @decorators.idempotent_id('7e4192e9-b1b7-48c9-af04-79a82637c359')
    def test_trunk_and_vm_sub_interface_vlan_diff(self):
        self.network_topo = self.create_trunk_network_topo(False, True)
        self._test_trunk_instance_vm_vlan_diff_subnet(self.network_topo)
