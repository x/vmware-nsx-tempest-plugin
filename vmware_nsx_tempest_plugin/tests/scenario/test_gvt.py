# Copyright 2021 VMware Inc
# All Rights Reserved
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
from tempest import config
from tempest.lib.common.utils import data_utils

from tempest.lib import decorators
from tempest.lib import exceptions

from tempest.lib.common.utils import test_utils

from vmware_nsx_tempest_plugin.common import constants
from vmware_nsx_tempest_plugin.lib import feature_manager
from vmware_nsx_tempest_plugin.services import nsxv3_client

CONF = config.CONF
DSCP_MARK = 12
DSCP_MARK_UPDATED = 16
BW_VALUE_KBPS = 1024
BW_VALUE_MBPS = 1
UPDATED_BW_VALUE_KBPS = 2048
UPDATED_BW_VALUE_MBPS = 2
MAX_BURST_KBPS = 1024000
MAX_BURST_MBPS = 1


class GuestVlanTaggingTests(feature_manager.FeatureManager):

    """Test SR Anti Affinity

    """
    @classmethod
    def setup_clients(cls):
        super(GuestVlanTaggingTests, cls).setup_clients()
        cls.cmgr_adm = cls.get_client_manager('admin')
        cls.cmgr_alt = cls.get_client_manager('alt')
        cls.cmgr_adm = cls.get_client_manager('admin')
        cls.routers_client = cls.cmgr_adm.routers_client
        cls.networks_client = cls.cmgr_adm.networks_client
        cls.subnets_client = cls.cmgr_adm.subnets_client
        cls.sec_rule_client = cls.cmgr_adm.security_group_rules_client
        cls.sec_client = cls.cmgr_adm.security_groups_client
        cls.quotas_client = cls.cmgr_adm.quotas_client

    @classmethod
    def resource_setup(cls):
        cls.nsx = nsxv3_client.NSXV3Client(CONF.nsxv3.nsx_manager,
                                           CONF.nsxv3.nsx_user,
                                           CONF.nsxv3.nsx_password)
        out = cls.nsx.get_transport_zones()
        vlan_flag = 0
        vxlan_flag = 0
        for tz in out:
            if "transport_type" in tz.keys() and \
                   (vlan_flag == 0 or vxlan_flag == 0):
                if vxlan_flag == 0 and tz['transport_type'] == "OVERLAY"\
                        and tz['_create_user'] == 'admin':
                    cls.overlay_id = tz['id']
                    vxlan_flag = 1
                if vlan_flag == 0 and tz['transport_type'] == "VLAN"\
                        and tz['_create_user'] == 'admin':
                    cls.vlan_id = tz['id']
                    vlan_flag = 1

    def provider_networks_gvt_topoloy(self, net_type,
                                      admin_state_up=True,
                                      tz_id=None,
                                      vlan_id_unique=None,
                                      networks_client=None,
                                      transparent_vlan=True):
        if networks_client is None:
            networks_client = self.cmgr_adm.networks_client
        if net_type == constants.VXLAN_TYPE:
            name = "provider_network_vxlan"
            if transparent_vlan:
                body = {"provider:physical_network": tz_id,
                        "provider:network_type": net_type,
                        "admin_state_up": admin_state_up,
                        "vlan_transparent": True}
            else:
                body = {"provider:physical_network": tz_id,
                        "provider:network_type": net_type,
                        "admin_state_up": admin_state_up,
                        "vlan_transparent": False}
        elif net_type == constants.VLAN_TYPE:
            name = "provider_network_vlan"
            if vlan_id_unique is not None:
                vlan_id_no = vlan_id_unique
            else:
                vlan_id_no = 0
            if tz_id is None:
                if transparent_vlan:
                    body = {"provider:segmentation_id": vlan_id_no,
                            "provider:network_type": net_type,
                            "admin_state_up": admin_state_up,
                            "vlan_transparent": True}
                else:
                    body = {"provider:segmentation_id": vlan_id_no,
                            "provider:network_type": net_type,
                            "admin_state_up": admin_state_up,
                            "vlan_transparent": False}
            else:
                if transparent_vlan:
                    body = {"provider:segmentation_id": vlan_id_no,
                            "provider:network_type": net_type,
                            "provider:physical_network": tz_id,
                            "admin_state_up": admin_state_up,
                            "vlan_transparent": True}
                else:
                    body = {"provider:segmentation_id": vlan_id_no,
                            "provider:network_type": net_type,
                            "provider:physical_network": tz_id,
                            "admin_state_up": admin_state_up,
                            "vlan_transparent": False}
        network = self.create_topology_network(name,
                                               networks_client=networks_client,
                                               **body)
        return network

    def _create_qos_policy(self):
        """Create a qos policy with bandwidth limit rule
           and dscp rule
        """
        name = data_utils.rand_name('test-qos-policy-')
        policy = self.create_qos_policy(name,
                                        description='bw_dscp_rule',
                                        shared=False)
        # add bw rule
        self.create_bandwidth_limit_rule(
            policy_id=policy['id'], max_kbps=BW_VALUE_KBPS,
            max_burst_kbps=MAX_BURST_KBPS)
        # add dscp rule
        self.create_dscp_marking_rule(
            policy_id=policy['id'], dscp_mark=DSCP_MARK)
        return policy

    @decorators.attr(type='nsxv3')
    @decorators.idempotent_id('86d814ed-fdce-4062-a633-2f0a7af5671d')
    def test_read_vxlan_enabled_network(self):
        provider_network = self.provider_networks_gvt_topoloy(
            constants.VXLAN_TYPE, tz_id=self.overlay_id)
        subnet_client = self.cmgr_adm.subnets_client
        subnet_name = provider_network['name'] + '_subnet'
        self.create_topology_subnet(subnet_name, provider_network,
                                    enable_dhcp=False,
                                    subnets_client=subnet_client)
        network = self.cmgr_adm.networks_client.show_network(
            provider_network['id'])
        self.assertIsNotNone(network)

    @decorators.attr(type='nsxv3')
    @decorators.idempotent_id('75c793ed-efdf-4062-a633-2f0a7af5671d')
    def test_update_with_qos_vlan_enabled_network_with_enabled_gvt(self):
        provider_network = self.provider_networks_gvt_topoloy(
            constants.VLAN_TYPE,
            tz_id=self.vlan_id)
        subnet_client = self.cmgr_adm.subnets_client
        subnet_name = provider_network['name'] + '_subnet'
        self.create_topology_subnet(subnet_name, provider_network,
                                    enable_dhcp=False,
                                    subnets_client=subnet_client)
        network = self.cmgr_adm.networks_client.show_network(
            provider_network['id'])['network']
        self.assertIsNotNone(network)
        qos_policy = self._create_qos_policy()
        network = self.cmgr_adm.networks_client.update_network(
                network['id'], qos_policy_id=qos_policy['id'])['network']
        self.addCleanup(test_utils.call_and_ignore_notfound_exc,
                        self.cmgr_adm.networks_client.update_network,
                        network['id'], qos_policy_id=None)
        self.assertEqual(network['qos_policy_id'],
                         qos_policy['id'])

    @decorators.attr(type='nsxv3')
    @decorators.idempotent_id('75c793ed-fdce-5173-a633-2f0a7af5671d')
    def test_update_with_qos_vxlan_enabled_network_with_enabled_gvt(self):
        provider_network = self.provider_networks_gvt_topoloy(
            constants.VXLAN_TYPE, tz_id=self.overlay_id)
        subnet_client = self.cmgr_adm.subnets_client
        subnet_name = provider_network['name'] + '_subnet'
        self.create_topology_subnet(subnet_name, provider_network,
                                    enable_dhcp=False,
                                    subnets_client=subnet_client)
        network = self.cmgr_adm.networks_client.show_network(
            provider_network['id'])['network']
        self.assertIsNotNone(network)
        qos_policy = self._create_qos_policy()
        network = self.cmgr_adm.networks_client.update_network(
                network['id'], qos_policy_id=qos_policy['id'])['network']
        self.addCleanup(test_utils.call_and_ignore_notfound_exc,
                        self.cmgr_adm.networks_client.update_network,
                        network['id'], qos_policy_id=None)
        self.assertEqual(network['qos_policy_id'],
                         qos_policy['id'])

    @decorators.attr(type='nsxv3')
    @decorators.idempotent_id('75c793ed-fdce-4062-b744-2f0a7af5671d')
    def test_update_with_port_security_vlan_enabled_network_with_gvt(self):
        provider_network = self.provider_networks_gvt_topoloy(
            constants.VLAN_TYPE,
            tz_id=self.vlan_id)
        subnet_client = self.cmgr_adm.subnets_client
        subnet_name = provider_network['name'] + '_subnet'
        self.create_topology_subnet(subnet_name, provider_network,
                                    enable_dhcp=False,
                                    subnets_client=subnet_client)
        network = self.cmgr_adm.networks_client.show_network(
            provider_network['id'])['network']
        self.assertIsNotNone(network)
        network = self.cmgr_adm.networks_client.update_network(
                network['id'], port_security_enabled=False)['network']
        self.assertEqual(network['port_security_enabled'],
                         False)

    @decorators.attr(type='nsxv3')
    @decorators.idempotent_id('75c793ed-fdce-4062-a633-3e1b8af5671d')
    def test_update_with_port_security_vxlan_enabled_network_with_gvt(self):
        provider_network = self.provider_networks_gvt_topoloy(
            constants.VXLAN_TYPE, tz_id=self.overlay_id)
        subnet_client = self.cmgr_adm.subnets_client
        subnet_name = provider_network['name'] + '_subnet'
        self.create_topology_subnet(subnet_name, provider_network,
                                    enable_dhcp=False,
                                    subnets_client=subnet_client)
        network = self.cmgr_adm.networks_client.show_network(
            provider_network['id'])['network']
        self.assertIsNotNone(network)
        network = self.cmgr_adm.networks_client.update_network(
                network['id'], port_security_enabled=False)['network']
        self.assertEqual(network['port_security_enabled'],
                         False)

    @decorators.attr(type='nsxv3')
    @decorators.idempotent_id('75c793ed-fdce-4062-a633-2f0a7af6723d')
    def test_update_subnet_vlan_enabled_network_with_enabled_gvt(self):
        provider_network = self.provider_networks_gvt_topoloy(
            constants.VLAN_TYPE,
            tz_id=self.vlan_id)
        subnet_client = self.cmgr_adm.subnets_client
        subnet_name = provider_network['name'] + '_subnet'
        subnet = self.create_topology_subnet(subnet_name, provider_network,
                                             enable_dhcp=False,
                                             subnets_client=subnet_client)
        network = self.cmgr_adm.networks_client.show_network(
            provider_network['id'])['network']
        self.assertIsNotNone(network)
        subnet = self.cmgr_adm.subnets_client.update_subnet(
            subnet['id'], name="test-subnet-new")['subnet']
        self.assertEqual(subnet['name'],
                         "test-subnet-new")

    @decorators.attr(type='nsxv3')
    @decorators.idempotent_id('75c703ae-fdce-4062-a633-2f0a7af5671d')
    def test_update_subnet_vxlan_enabled_network_with_enabled_gvt(self):
        provider_network = self.provider_networks_gvt_topoloy(
            constants.VXLAN_TYPE, tz_id=self.overlay_id)
        subnet_client = self.cmgr_adm.subnets_client
        subnet_name = provider_network['name'] + '_subnet'
        subnet = self.create_topology_subnet(subnet_name, provider_network,
                                             enable_dhcp=False,
                                             subnets_client=subnet_client)
        network = self.cmgr_adm.networks_client.show_network(
            provider_network['id'])['network']
        self.assertIsNotNone(network)
        subnet = self.cmgr_adm.subnets_client.update_subnet(
            subnet['id'], name="test-subnet-new")['subnet']
        self.assertEqual(subnet['name'],
                         "test-subnet-new")

    @decorators.attr(type='nsxv3')
    @decorators.idempotent_id('75c793ed-ebbb-5341-a633-2f0a7af5671d')
    def test_read_vlan_enabled_network_with_enabled_gvt(self):
        provider_network = self.provider_networks_gvt_topoloy(
            constants.VLAN_TYPE,
            tz_id=self.vlan_id)
        subnet_client = self.cmgr_adm.subnets_client
        subnet_name = provider_network['name'] + '_subnet'
        self.create_topology_subnet(subnet_name, provider_network,
                                    enable_dhcp=False,
                                    subnets_client=subnet_client)
        network = self.cmgr_adm.networks_client.show_network(
            provider_network['id'])
        self.assertIsNotNone(network)

    @decorators.attr(type='nsxv3')
    @decorators.idempotent_id('75c793ed-ffcc-4040-a633-2f0a7af5671d')
    def test_delete_vlan_enabled_network_with_enabled_gvt_manual(self):
        provider_network = self.provider_networks_gvt_topoloy(
            constants.VLAN_TYPE,
            tz_id=self.vlan_id)
        subnet_client = self.cmgr_adm.subnets_client
        subnet_name = provider_network['name'] + '_subnet'
        self.create_topology_subnet(subnet_name, provider_network,
                                    enable_dhcp=False,
                                    subnets_client=subnet_client)
        network = self.cmgr_adm.networks_client.show_network(
            provider_network['id'])
        self.assertIsNotNone(network)
        self.cmgr_adm.networks_client.delete_network(
            provider_network['id'])
        self.assertRaises(exceptions.NotFound,
                          self.cmgr_adm.networks_client.show_network,
                          provider_network['id'])

    @decorators.attr(type='nsxv3')
    @decorators.idempotent_id('75c793ed-fdce-4062-aa22-2f0a7af5671d')
    def test_delete_vxlan_enabled_network_manual(self):
        provider_network = self.provider_networks_gvt_topoloy(
            constants.VXLAN_TYPE, tz_id=self.overlay_id)
        subnet_client = self.cmgr_adm.subnets_client
        subnet_name = provider_network['name'] + '_subnet'
        self.create_topology_subnet(subnet_name, provider_network,
                                    enable_dhcp=False,
                                    subnets_client=subnet_client)
        network = self.cmgr_adm.networks_client.show_network(
            provider_network['id'])
        self.assertIsNotNone(network)
        self.cmgr_adm.networks_client.delete_network(
            provider_network['id'])
        self.assertRaises(exceptions.NotFound,
                          self.cmgr_adm.networks_client.show_network,
                          provider_network['id'])

    @decorators.attr(type='nsxv3')
    @decorators.idempotent_id('75c793ed-fdce-4062-a633-2f2f1ba5671d')
    def test_create_vlan_backed_gvt_with_incorrect_physical_network_id(self):
        self.assertRaises(exceptions.BadRequest,
                          self.provider_networks_gvt_topoloy,
                          constants.VLAN_TYPE,
                          tz_id="yajsjasj8281912")

    @decorators.attr(type='nsxv3')
    @decorators.idempotent_id('75c793ed-fdce-0022-a633-2f0a7af5671d')
    def test_create_vlan_backed_gvt_with_incorrect_network_type_manual(self):
        networks_client = self.cmgr_adm.networks_client
        name = "provider_network_vlan"
        vlan_id_no = 0
        body = {"provider:segmentation_id": vlan_id_no,
                "provider:network_type": "vlan1",
                "provider:physical_network": self.vlan_id,
                "admin_state_up": True,
                "vlan_transparent": True}
        self.assertRaises(exceptions.BadRequest,
                          self.create_topology_network,
                          name, networks_client=networks_client,
                          **body)

    @decorators.attr(type='nsxv3')
    @decorators.idempotent_id('75c793ed-dddd-4062-a633-2f0a7af5671d')
    def test_create_vlan_enabled_network_vlan_40to1000with_disabled_dhcp(self):
        kwargs = {'subnet': -1, 'network': -1, 'floatingip': -1,
                  'router': -1, 'port': -1}
        self.network_quotas_client.update_quotas(
            tenant_id=self.network_quotas_client.tenant_id, **kwargs)
        for i in range(40, 1000):
            provider_network = self.provider_networks_gvt_topoloy(
                constants.VLAN_TYPE,
                tz_id=self.vlan_id,
                vlan_id_unique=i,
                transparent_vlan=False)
            subnet_client = self.cmgr_adm.subnets_client
            subnet_name = provider_network['name'] + '_subnet'
            self.create_topology_subnet(subnet_name, provider_network,
                                        enable_dhcp=False,
                                        subnets_client=subnet_client)
            network = self.cmgr_adm.networks_client.show_network(
                 provider_network['id'])
            self.assertIsNotNone(network)

    @decorators.attr(type='nsxv3')
    @decorators.idempotent_id('75c712aa-fdce-4062-a633-2f0a7af5671d')
    def test_create_vlan_enabled_network_vlan40to1000and_update_subnet(self):
        kwargs = {'subnet': -1, 'network': -1, 'floatingip': -1,
                  'router': -1, 'port': -1}
        self.network_quotas_client.update_quotas(
            tenant_id=self.network_quotas_client.tenant_id, **kwargs)
        for i in range(40, 1000):
            provider_network = self.provider_networks_gvt_topoloy(
                constants.VLAN_TYPE,
                tz_id=self.vlan_id,
                vlan_id_unique=i,
                transparent_vlan=False)
            subnet_client = self.cmgr_adm.subnets_client
            subnet_name = provider_network['name'] + '_subnet'
            subnet = self.create_topology_subnet(subnet_name, provider_network,
                                                 enable_dhcp=False,
                                                 subnets_client=subnet_client)
            subnet = self.cmgr_adm.subnets_client.update_subnet(
                subnet['id'], name="test-subnet-new")['subnet']
            self.assertEqual(subnet['name'],
                             "test-subnet-new")

    @decorators.attr(type='nsxv3')
    @decorators.idempotent_id('75c653ed-fdce-4062-a633-2f0a7af5671d')
    def test_create_vlan_enabled_network_vlan40to1000and_updateqos(self):
        kwargs = {'subnet': -1, 'network': -1, 'floatingip': -1,
                  'router': -1, 'port': -1}
        self.network_quotas_client.update_quotas(
            tenant_id=self.network_quotas_client.tenant_id, **kwargs)
        qos_policy = self._create_qos_policy()
        for i in range(40, 1000):
            provider_network = self.provider_networks_gvt_topoloy(
                constants.VLAN_TYPE,
                tz_id=self.vlan_id,
                vlan_id_unique=i,
                transparent_vlan=False)
            subnet_client = self.cmgr_adm.subnets_client
            subnet_name = provider_network['name'] + '_subnet'
            self.create_topology_subnet(subnet_name, provider_network,
                                        enable_dhcp=False,
                                        subnets_client=subnet_client)
            network = self.cmgr_adm.networks_client.update_network(
                      provider_network['id'],
                      qos_policy_id=qos_policy['id'])['network']
            self.addCleanup(test_utils.call_and_ignore_notfound_exc,
                            self.cmgr_adm.networks_client.update_network,
                            network['id'], qos_policy_id=None)
            self.assertEqual(network['qos_policy_id'],
                             qos_policy['id'])

    @decorators.attr(type='nsxv3')
    @decorators.idempotent_id('23a803ed-fdce-4062-a633-2f0a7af5671d')
    def test_update_vlan_enabled_network_40to1000with_port_sec_dis(self):
        kwargs = {'subnet': -1, 'network': -1, 'floatingip': -1,
                  'router': -1, 'port': -1}
        self.network_quotas_client.update_quotas(
            tenant_id=self.network_quotas_client.tenant_id, **kwargs)
        for i in range(40, 1000):
            provider_network = self.provider_networks_gvt_topoloy(
                constants.VLAN_TYPE,
                tz_id=self.vlan_id,
                vlan_id_unique=i,
                transparent_vlan=False)
            subnet_client = self.cmgr_adm.subnets_client
            subnet_name = provider_network['name'] + '_subnet'
            self.create_topology_subnet(subnet_name, provider_network,
                                        enable_dhcp=False,
                                        subnets_client=subnet_client)
            network = self.cmgr_adm.networks_client.update_network(
                provider_network['id'], port_security_enabled=False)['network']
            self.assertEqual(network['port_security_enabled'],
                             False)
