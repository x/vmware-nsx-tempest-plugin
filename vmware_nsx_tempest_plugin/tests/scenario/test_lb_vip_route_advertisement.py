# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
import time

from oslo_log import log as logging
from tempest import config
from tempest.lib.common.utils import data_utils
from tempest.lib.common.utils import test_utils
from tempest.lib import decorators

from vmware_nsx_tempest_plugin.common import constants
from vmware_nsx_tempest_plugin.lib import feature_manager
from vmware_nsx_tempest_plugin.services import nsxv3_client

CONF = config.CONF

LOG = logging.getLogger(__name__)


class TestLBVipRoute(feature_manager.FeatureManager):
    """Test TestLBVipRoute

    Adding test cases to test deploy tier1
    on sepcific edge_cluster.
    """

    def setUp(self):
        super(TestLBVipRoute, self).setUp()
        self.cmgr_adm = self.get_client_manager('admin')
        self.cmgr_alt = self.get_client_manager('alt')
        self.cmgr_adm = self.get_client_manager('admin')
        self.nsx = nsxv3_client.NSXV3Client(CONF.nsxv3.nsx_manager,
                                            CONF.nsxv3.nsx_user,
                                            CONF.nsxv3.nsx_password)
        CONF.validation.ssh_shell_prologue = ''
        self.vip_ip_address = ''
        self.namestart = 'lbaas-ops'
        self.poke_counters = 12
        self.hm_delay = 4
        self.hm_max_retries = 3
        self.hm_timeout = 10
        self.server_names = []
        self.loadbalancer = None
        self.vip_fip = None
        self.web_service_start_delay = 2.5

    @classmethod
    def skip_checks(cls):
        """Class level skip checks.
        Class level check. Skip all the MDproxy tests, if native_dhcp_metadata
        is not True under nsxv3 section of the config
        """
        super(TestLBVipRoute, cls).skip_checks()

    def _create_network_topo(self, subnetpool_id, prefixlen=26):
        net_client = self.cmgr_adm.networks_client
        body = {'name': 'provider-network'}
        network = net_client.create_network(**body)
        self.addCleanup(test_utils.call_and_ignore_notfound_exc,
                        net_client.delete_network,
                        network['network']['id'])
        body = {"network_id": network['network']['id'],
                "ip_version": 4, "subnetpool_id": subnetpool_id,
                "prefixlen": 28}
        subnet_client = self.cmgr_adm.subnets_client
        subnet = subnet_client.create_subnet(**body)
        self.addCleanup(test_utils.call_and_ignore_notfound_exc,
                        subnet_client.delete_subnet,
                        subnet['subnet']['id'])
        network_topo = dict(network=network, subnet=subnet)
        return network_topo

    @decorators.idempotent_id('2317349c-02dd-0016-c228-98844caa46c3')
    def test_lb_vip_route_with_tenant_net(self):
        """
        Check lb vip route should not present on tier1
        if floating ip is not created.
        """
        kwargs = {"enable_snat": True}
        router_state_1 = self.create_topology_router(set_gateway=True,
                                                     routers_client=self.
                                                     cmgr_adm.routers_client,
                                                     **kwargs)
        network_lbaas_1 = self.create_topology_network(
            "network_lbaas", networks_client=self.cmgr_adm.networks_client)
        sec_rule_client = self.cmgr_adm.security_group_rules_client
        sec_client = self.cmgr_adm.security_groups_client
        kwargs = dict(tenant_id=network_lbaas_1['tenant_id'],
                      security_group_rules_client=sec_rule_client,
                      security_groups_client=sec_client)
        self.sg = self.create_topology_security_group(**kwargs)
        lbaas_rules = [dict(direction='ingress', protocol='tcp',
                            port_range_min=constants.HTTP_PORT,
                            port_range_max=constants.HTTP_PORT, ),
                       dict(direction='ingress', protocol='tcp',
                            port_range_min=443, port_range_max=443, )]
        for rule in lbaas_rules:
            self.add_security_group_rule(self.sg, rule,
                                         ruleclient=sec_rule_client,
                                         secclient=sec_client)
        subnet_lbaas = self.create_topology_subnet(
            "subnet_lbaas", network_lbaas_1, router_id=router_state_1["id"],
            routers_client=self.cmgr_adm.routers_client,
            subnets_client=self.cmgr_adm.subnets_client)
        no_of_servers = 2
        image_id = self.get_glance_image_id(["cirros", "esx"])
        for instance in range(0, no_of_servers):
            self.create_topology_instance(
                "server_lbaas_%s" % instance, [network_lbaas_1],
                security_groups=[{'name': self.sg['name']}],
                image_id=image_id, create_floating_ip=False,
                clients=self.cmgr_adm)
        loadbalancer = self.load_balancers_admin_client.\
            create_load_balancer(name='test-lb',
                                 vip_subnet_id=subnet_lbaas['id']
                                 )['loadbalancer']
        self.load_balancers_admin_client.\
            wait_for_load_balancer_status(loadbalancer['id'])
        listener = self.listeners_admin_client.create_listener(
            loadbalancer_id=loadbalancer['id'], protocol='HTTP',
            protocol_port='80', name='test_listener')['listener']
        self.load_balancers_admin_client.\
            wait_for_load_balancer_status(loadbalancer['id'])
        pool = self.pools_admin_client.create_pool(
            listener_id=listener['id'],
            lb_algorithm='ROUND_ROBIN', protocol='HTTP')['pool']
        self.load_balancers_admin_client.\
            wait_for_load_balancer_status(loadbalancer['id'])
        self.health_monitors_admin_client.create_health_monitor(
                pool_id=pool['id'], type='PING',
                delay='5', max_retries='3',
                timeout='5')
        self.load_balancers_admin_client.\
            wait_for_load_balancer_status(loadbalancer['id'])
        self.members_admin_client.create_member(
            pool['id'], subnet_id=subnet_lbaas['id'],
            address="127.0.0.1",
            protocol_port=80)['member']
        self.load_balancers_admin_client.\
            wait_for_load_balancer_status(loadbalancer['id'])
        lb_resource = {}
        lb_resource['vip_ip'] = loadbalancer['vip_address']
        lb_resource['lb_id'] = loadbalancer['id']
        time.sleep(30)
        nsx_router_nat_rules = self.nsx.get_logical_router_nat_rule_ips(
            router_state_1['name'], router_state_1['id'])
        route_present = False
        for advertised_net in nsx_router_nat_rules['advertisedNetworks']:
            if len(advertised_net['networks']) > 0:
                if lb_resource['vip_ip'] in advertised_net[
                        'networks'][0]['network'] and\
                        advertised_net['networks'][
                        0]['advertiseRouteType'] == 'T1_LB_VIP' and\
                        advertised_net['networks'][0]['advertiseAllow']:
                    route_present = True
        self.assertEqual(False, route_present, 'Lb vip route is advertised')
        vip_fip = self.create_floatingip(
            loadbalancer,
            client=self.cmgr_adm.floating_ips_client,
            port_id=loadbalancer['vip_port_id'])
        self.vip_ip_address = vip_fip['floating_ip_address']
        time.sleep(30)
        nsx_router_nat_rules = self.nsx.get_logical_router_nat_rule_ips(
            router_state_1['name'], router_state_1['id'])
        route_present = False
        for advertised_net in nsx_router_nat_rules['advertisedNetworks']:
            if len(advertised_net['networks']) > 0:
                if self.vip_ip_address in advertised_net[
                        'networks'][0]['network'] and\
                        advertised_net['networks'][
                        0]['advertiseRouteType'] == 'T1_LB_VIP' and\
                        advertised_net['networks'][0]['advertiseAllow']:
                    route_present = True
        self.assertEqual(True, route_present, 'Lb vip route is not advertised')
        kwargs = dict(port_id=None)
        self.cmgr_adm.floating_ips_client.\
            update_floatingip(vip_fip['id'],
                              **kwargs)['floatingip']
        time.sleep(30)
        nsx_router_nat_rules = self.nsx.get_logical_router_nat_rule_ips(
            router_state_1['name'], router_state_1['id'])
        route_present = False
        for advertised_net in nsx_router_nat_rules['advertisedNetworks']:
            if len(advertised_net['networks']) > 0:
                if lb_resource['vip_ip'] in advertised_net[
                        'networks'][0]['network'] and\
                        advertised_net['networks'][
                        0]['advertiseRouteType'] == 'T1_LB_VIP' and\
                        advertised_net['networks'][0]['advertiseAllow']:
                    route_present = True
        self.assertEqual(False, route_present, 'Lb vip route is advertised')
        self.delete_loadbalancer_resources(lb_resource['lb_id'], admin=True)
        time.sleep(30)
        nsx_router_nat_rules = self.nsx.get_logical_router_nat_rule_ips(
            router_state_1['name'], router_state_1['id'])
        route_present = False
        for advertised_net in nsx_router_nat_rules['advertisedNetworks']:
            if len(advertised_net['networks']) > 0:
                if lb_resource['vip_ip'] in advertised_net[
                        'networks'][0]['network'] and\
                        advertised_net['networks'][
                        0]['advertiseRouteType'] == 'T1_LB_VIP':
                    route_present = True
        self.assertEqual(False, route_present, 'Lb vip route is present')

    @decorators.idempotent_id('2317349c-02cc-1127-d339-09955dbb47d4')
    def test_lb_vip_route_with_external_net(self):
        """
        Check lb vip vip route should be present with external net
        """
        kwargs = {"enable_snat": True}
        router_state_1 = self.create_topology_router(set_gateway=True,
                                                     routers_client=self.
                                                     cmgr_adm.routers_client,
                                                     **kwargs)
        network_lbaas_1 = self.create_topology_network(
            "network_lbaas", networks_client=self.cmgr_adm.networks_client)
        sec_rule_client = self.cmgr_adm.security_group_rules_client
        sec_client = self.cmgr_adm.security_groups_client
        kwargs = dict(tenant_id=network_lbaas_1['tenant_id'],
                      security_group_rules_client=sec_rule_client,
                      security_groups_client=sec_client)
        self.sg = self.create_topology_security_group(**kwargs)
        lbaas_rules = [dict(direction='ingress', protocol='tcp',
                            port_range_min=constants.HTTP_PORT,
                            port_range_max=constants.HTTP_PORT, ),
                       dict(direction='ingress', protocol='tcp',
                            port_range_min=443, port_range_max=443, )]
        for rule in lbaas_rules:
            self.add_security_group_rule(
                self.sg,
                rule,
                ruleclient=sec_rule_client,
                secclient=sec_client,
                tenant_id=network_lbaas_1['tenant_id'])
        subnet_lbaas = self.create_topology_subnet(
            "subnet_lbaas", network_lbaas_1,
            subnets_client=self.cmgr_adm.subnets_client,
            routers_client=self.cmgr_adm.routers_client,
            router_id=router_state_1["id"])
        no_of_servers = 2
        image_id = self.get_glance_image_id(["cirros", "esx"])
        for instance in range(0, no_of_servers):
            self.create_topology_instance(
                "server_lbaas_%s" % instance, [network_lbaas_1],
                security_groups=[{'name': self.sg['name']}],
                image_id=image_id, create_floating_ip=False,
                clients=self.cmgr_adm)
        network = self.cmgr_adm.networks_client.show_network(
            CONF.network.public_network_id)['network']
        lb_resource = self.create_project_lbaas(
            protocol_type="HTTP", protocol_port="80",
            vip_subnet_id=subnet_lbaas['id'],
            lb_algorithm="ROUND_ROBIN", hm_type='PING',
            create_fip=False, clean_up=True,
            external_subnet=network['subnets'][0])
        time.sleep(30)
        nsx_router_nat_rules = self.nsx.get_logical_router_nat_rule_ips(
            router_state_1['name'], router_state_1['id'])
        route_present = False
        for advertised_net in nsx_router_nat_rules['advertisedNetworks']:
            if len(advertised_net['networks']) > 0:
                if self.loadbalancer['vip_address'] in\
                        advertised_net['networks'][0]['network'] and\
                        advertised_net['networks'][
                        0]['advertiseRouteType'] == 'T1_LB_VIP' and\
                        advertised_net['networks'][0]['advertiseAllow']:
                    route_present = True
        self.assertEqual(True, route_present, 'Lb vip route is not advertised')
        self.delete_loadbalancer_resources(lb_resource['lb_id'], admin=True)
        time.sleep(30)
        nsx_router_nat_rules = self.nsx.get_logical_router_nat_rule_ips(
            router_state_1['name'], router_state_1['id'])
        route_present = False
        for advertised_net in nsx_router_nat_rules['advertisedNetworks']:
            if len(advertised_net['networks']) > 0:
                if self.loadbalancer['vip_address'] in\
                        advertised_net['networks'][0]['network'] and\
                        advertised_net['networks'][
                        0]['advertiseRouteType'] == 'T1_LB_VIP':
                    route_present = True
        self.assertEqual(False, route_present, 'Lb vip route is present')

    @decorators.idempotent_id('2317349c-02cc-1127-d339-09955dbb47d4')
    def test_lb_vip_route_with_overlapping_subnet_tenant_network(self):
        """
        Check lb vip vip route should be present with external net
        """
        subnetpool_name = data_utils.rand_name('subnetpools')
        body = self._create_subnet_pool(self.cmgr_adm, subnetpool_name)
        subnetpool_id = body["subnetpool"]["id"]
        network_topo = self._create_network_topo(subnetpool_id, prefixlen=28)
        subnet_client = self.cmgr_adm.subnets_client
        body = {"network_id": network_topo['network']['network']['id'],
                "ip_version": 4, "subnetpool_id": subnetpool_id,
                "prefixlen": 28, "enable_dhcp": 'false'}
        subnet = subnet_client.create_subnet(**body)
        self.addCleanup(test_utils.call_and_ignore_notfound_exc,
                        subnet_client.delete_subnet,
                        subnet['subnet']['id'])
        kwargs = {"enable_snat": True}
        router_state_1 = self.create_topology_router(set_gateway=True,
                                                     routers_client=self.
                                                     cmgr_adm.routers_client,
                                                     **kwargs)
        subnet_lbaas = network_topo['subnet']['subnet']
        self.cmgr_adm.routers_client.add_router_interface(
            router_state_1['id'], subnet_id=subnet_lbaas["id"])
        self.addCleanup(
            test_utils.call_and_ignore_notfound_exc,
            self.cmgr_adm.routers_client.remove_router_interface,
            router_state_1['id'],
            subnet_id=subnet_lbaas["id"])
        loadbalancer = self.load_balancers_admin_client.\
            create_load_balancer(name='test-lb',
                                 vip_subnet_id=subnet_lbaas['id']
                                 )['loadbalancer']
        self.load_balancers_admin_client.\
            wait_for_load_balancer_status(loadbalancer['id'])
        listener = self.listeners_admin_client.create_listener(
            loadbalancer_id=loadbalancer['id'], protocol='HTTP',
            protocol_port='80', name='test_listener')['listener']
        self.load_balancers_admin_client.\
            wait_for_load_balancer_status(loadbalancer['id'])
        pool = self.pools_admin_client.create_pool(
            listener_id=listener['id'],
            lb_algorithm='ROUND_ROBIN', protocol='HTTP')['pool']
        self.load_balancers_admin_client.\
            wait_for_load_balancer_status(loadbalancer['id'])
        self.health_monitors_admin_client.create_health_monitor(
            pool_id=pool['id'], type='PING',
            delay='5', max_retries='3',
            timeout='5')
        self.load_balancers_admin_client.\
            wait_for_load_balancer_status(loadbalancer['id'])
        self.members_admin_client.create_member(
            pool['id'], subnet_id=subnet_lbaas['id'],
            address="127.0.0.1",
            protocol_port=80)['member']
        self.load_balancers_admin_client.\
            wait_for_load_balancer_status(loadbalancer['id'])
        time.sleep(30)
        nsx_router_nat_rules = self.nsx.get_logical_router_nat_rule_ips(
            router_state_1['name'], router_state_1['id'])
        route_present = False
        lb_resource = {}
        lb_resource['vip_ip'] = loadbalancer['vip_address']
        lb_resource['lb_id'] = loadbalancer['id']
        for advertised_net in nsx_router_nat_rules['advertisedNetworks']:
            if len(advertised_net['networks']) > 0:
                if lb_resource['vip_ip'] in advertised_net[
                        'networks'][0]['network'] and\
                        advertised_net['networks'][
                        0]['advertiseRouteType'] == 'T1_LB_VIP' and\
                        advertised_net['networks'][0]['advertiseAllow']:
                    route_present = True
        self.assertEqual(False, route_present, 'Lb vip route is advertised')
        vip_fip = self.create_floatingip(
            loadbalancer,
            client=self.cmgr_adm.floating_ips_client,
            port_id=loadbalancer['vip_port_id'])
        self.vip_ip_address = vip_fip['floating_ip_address']
        time.sleep(30)
        nsx_router_nat_rules = self.nsx.get_logical_router_nat_rule_ips(
            router_state_1['name'], router_state_1['id'])
        route_present = False
        for advertised_net in nsx_router_nat_rules['advertisedNetworks']:
            if len(advertised_net['networks']) > 0:
                if self.vip_ip_address in advertised_net[
                        'networks'][0]['network'] and\
                        advertised_net['networks'][
                        0]['advertiseRouteType'] == 'T1_LB_VIP' and\
                        advertised_net['networks'][0]['advertiseAllow']:
                    route_present = True
        self.assertEqual(True, route_present, 'Lb vip route is not advertised')
        kwargs = dict(port_id=None)
        self.cmgr_adm.floating_ips_client.\
            update_floatingip(vip_fip['id'],
                              **kwargs)['floatingip']
        time.sleep(30)
        nsx_router_nat_rules = self.nsx.get_logical_router_nat_rule_ips(
            router_state_1['name'], router_state_1['id'])
        route_present = False
        for advertised_net in nsx_router_nat_rules['advertisedNetworks']:
            if len(advertised_net['networks']) > 0:
                if lb_resource['vip_ip'] in advertised_net[
                        'networks'][0]['network'] and\
                        advertised_net['networks'][
                        0]['advertiseRouteType'] == 'T1_LB_VIP' and\
                        advertised_net['networks'][0]['advertiseAllow']:
                    route_present = True
        self.assertEqual(False, route_present, 'Lb vip route is advertised')
        self.delete_loadbalancer_resources(lb_resource['lb_id'], admin=True)
        time.sleep(30)
        nsx_router_nat_rules = self.nsx.get_logical_router_nat_rule_ips(
            router_state_1['name'], router_state_1['id'])
        route_present = False
        for advertised_net in nsx_router_nat_rules['advertisedNetworks']:
            if len(advertised_net['networks']) > 0:
                if lb_resource['vip_ip'] in advertised_net[
                        'networks'][0]['network'] and\
                        advertised_net['networks'][
                        0]['advertiseRouteType'] == 'T1_LB_VIP':
                    route_present = True
        self.assertEqual(False, route_present, 'Lb vip route is present')
