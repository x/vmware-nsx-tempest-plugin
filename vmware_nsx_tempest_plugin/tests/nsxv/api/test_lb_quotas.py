# Copyright 2019 VMware Inc
# All Rights Reserved
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
from tempest import config
from tempest.lib import decorators
from tempest import test

from vmware_nsx_tempest_plugin.common import constants
from vmware_nsx_tempest_plugin.lib import feature_manager

LOG = constants.log.getLogger(__name__)
CONF = config.CONF


class OctaviaQuota(feature_manager.FeatureManager):

    """Base class to support LBaaS ROUND-ROBIN test.

    It provides the methods to create loadbalancer network, and
    start web servers.

    Default lb_algorithm is ROUND_ROBIND.
    """
    @classmethod
    def setup_clients(cls):
        super(OctaviaQuota, cls).setup_clients()
        cls.cmgr_adm = cls.get_client_manager('admin')
        cls.cmgr_alt = cls.get_client_manager('alt')
        cls.cmgr_adm = cls.get_client_manager('admin')

    @classmethod
    def skip_checks(cls):
        super(OctaviaQuota, cls).skip_checks()
        cfg = CONF.network
        if not test.is_extension_enabled('lbaasv2', 'network'):
            msg = 'lbaasv2 extension is not enabled.'
            raise cls.skipException(msg)
        if not (cfg.project_networks_reachable or cfg.public_network_id):
            msg = ('Either project_networks_reachable must be "true", or '
                   'public_network_id must be defined.')
            raise cls.skipException(msg)

    @classmethod
    def setup_credentials(cls):
        # Ask framework to not create network resources for these tests.
        cls.set_network_resources()
        super(OctaviaQuota, cls).setup_credentials()

    @decorators.idempotent_id('c3ac8546-6867-4b7a-8544-3843a11b1a34')
    def test_list_default_quota(self):
        quotas = self.octavia_admin_quota_client.\
            list_default_quota()['quota']
        for quota in quotas:
            msg = quota + '\'s quota is not reset to -1'
            self.assertTrue(quotas[quota] == -1, msg)

    @decorators.idempotent_id('c4ac8546-6867-4b7a-8544-3843a11b1a34')
    def test_show_quota_and_set_quota_and_show_quota_to_verify(self):
        project_id = CONF.auth.admin_tenant_id
        quotas = self.octavia_admin_quota_client.\
            list_project_quota(project_id)['quota']
        # Update lb quota for project with increasing quota by 5
        kwargs = {}
        kwargs['quota'] = {
            "loadbalancer": quotas['load_balancer'] + 5,
            "listener": quotas['listener'] + 5,
            "member": quotas['member'] + 5,
            "pool": quotas['pool'] + 5,
            "healthmonitor": quotas['health_monitor'] + 5,
                         }
        self.octavia_admin_quota_client.\
            set_project_quota(project_id, **kwargs)['quota']
        updated_quota = self.octavia_admin_quota_client.\
            list_project_quota(project_id)['quota']
        self.assertTrue(updated_quota['load_balancer'] ==
                        (quotas['load_balancer'] + 5))
        self.assertTrue(updated_quota['listener'] ==
                        (quotas['listener'] + 5))

    @decorators.idempotent_id('c5ac8546-6867-4b7a-8544-3843a11b1a34')
    def test_delete_quota_and_show_quota_to_verify(self):
        project_id = CONF.auth.admin_tenant_id
        self.octavia_admin_quota_client.\
            delete_project_quota(project_id)
        updated_quota = self.octavia_admin_quota_client.\
            list_project_quota(project_id)['quota']
        for quota in updated_quota:
            msg = quota + '\'s quota is not reset to -1'
            self.assertTrue(updated_quota[quota] == -1, msg)
