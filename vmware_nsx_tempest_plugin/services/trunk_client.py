# Copyright 2016 VMware, Inc.
# All Rights Reserved
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

from oslo_serialization import jsonutils

from tempest.lib.services.network import base

from vmware_nsx_tempest_plugin.services import network_client_base \
    as base_client


class BaseTrunkClient(base.BaseNetworkClient):
    """Why base client for trunk_client:

    https://bugs.launchpad.net/neutron/+bug/1606659
    tag-add is a CREATE operation; then expected resp_code is 201
    however it is using http PUT operation to accomplish it.
    """

    def update_resource(self, uri, post_data, resp_code=None):
        """allow different response code."""
        if resp_code:
            req_uri = self.uri_prefix + uri
            req_post_data = jsonutils.dumps(post_data)
            resp, body = self.put(req_uri, req_post_data)
            body = jsonutils.loads(body)
            self.expected_success(resp_code, resp.status)
            return base.rest_client.ResponseBody(
                resp, body)
        else:
            return super(BaseTrunkClient, self).update_resource(
                uri, post_data)


class TrunkClient(BaseTrunkClient):
    create_trunk_path = '/trunks'
    delete_trunk_path = '/trunks/%s'
    add_sub_port = '/trunks/%s/add_subports'
    remove_sub_port = '/trunks/%s/remove_subports'

    def create_trunk(self, **kwargs):
        """Create a trunk parent port.
        """
        post_data = kwargs
        response_data = self.create_resource(self.create_trunk_path, post_data)
        return response_data['trunk']['id']

    def delete_trunk(self, trunkportid):
        """Create a trunk parent port.
        """
        delete_trunk_path = self.delete_trunk_path % trunkportid
        response_data = self.delete_resource(delete_trunk_path)
        return response_data

    def add_subport(self, trunkportid, **kwargs):
        post_data = kwargs
        self.url = self.add_sub_port % trunkportid
        response_data = self.update_resource(self.url, post_data)
        return response_data

    def remove_subport(self, trunkportid, **kwargs):
        post_data = kwargs
        self.url = self.remove_sub_port % trunkportid
        response_data = self.update_resource(self.url, post_data)
        return response_data


def get_client(client_mgr,
               set_property=False, with_name="trunk_client"):
    """create trunk_client from networks_client.

    Create network trunk_client from manager or networks_client.
    client = trunk_client.get_client(manager)
    """
    manager = getattr(client_mgr, 'manager', client_mgr)
    net_client = getattr(manager, 'networks_client')
    try:
        _params = base_client.default_params_with_timeout_values.copy()
    except Exception:
        _params = {}
    client = TrunkClient(net_client.auth_provider,
                         net_client.service,
                         net_client.region,
                         net_client.endpoint_type,
                         **_params)
    if set_property:
        setattr(manager, with_name, client)
    return client
